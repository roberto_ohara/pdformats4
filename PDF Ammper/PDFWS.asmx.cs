﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ThoughtWorks.QRCode.Codec;
using System.Drawing.Imaging;
using System.Drawing;

using NPOI.SS.UserModel;
using Syncfusion.ExcelChartToImageConverter;
using Syncfusion.XlsIO;
using IChart = Syncfusion.XlsIO.IChart;

namespace PDF_Ammper
{
    /// <summary>
    /// Descripción breve de PDFWS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class PDFWS : System.Web.Services.WebService
    {
        public static string StringConvertedToBase64;

        [WebMethod]
        public string PDF(string Login, string password, string Grafica, string TipoCambio, string factura, string logotipo, string DataResumen, string QRCFDI, string QRSAT, string QRCERTSAT,string Extras)
        {
            string Resultado = string.Empty;

            if (Login != "" && password != "")
            {
                if (Login == "Ammper" && password == "44mmp3r")
                {
                    CopyPasteResources();


                    if (QRCFDI != "" && QRSAT != "" && QRCERTSAT != "")
                    {
                        if (Grafica != "" && factura != "" && DataResumen != "" && TipoCambio != "")
                        {

                            string tipoPDF = Grafica.Substring(0, Grafica.IndexOf("|"));
                            string tempo = Grafica.Substring(Grafica.IndexOf("|") + 1);
                            //for counts son todos los datos para grafico
                            string forcount = tempo.Substring(tempo.IndexOf("|") + 1);

                            string graficafirst;
                            if (tipoPDF == "4")
                            {
                                graficafirst = forcount.Substring(0, forcount.IndexOf("|"));
                            }
                            else
                            {
                                graficafirst = forcount;
                            }

                            string[] cuentagrafica = System.Text.RegularExpressions.Regex.Split(graficafirst, "-");
                            int contador = 0;
                            foreach (var elements in cuentagrafica)
                            {
                                contador++;

                            }
                            if (contador < 32 && contador > 27)
                            {

                                string facturatemporal = factura.Substring(0, factura.IndexOf("|"));
                                string[] validador = System.Text.RegularExpressions.Regex.Split(facturatemporal, ",");
                                string temporalfrase = factura.Substring(factura.IndexOf("|") + 1);
                                contador = 0;
                                foreach (var elements in validador)
                                {
                                    contador++;
                                }
                                if (contador == 3)
                                {
                                    contador = 0;
                                    for (int m = 0; m < temporalfrase.Length; m++)
                                    {
                                        if (temporalfrase.Substring(m, 1) == "|")
                                        {
                                            contador++;

                                        }
                                    }
                                    if (contador == 6)
                                    {
                                        contador = 0;
                                        for (int m = 0; m < DataResumen.Length; m++)
                                        {
                                            //if()
                                            if (DataResumen.Substring(m, 1) == "|")
                                            {
                                                contador++;

                                            }
                                        }
                                        if (contador == 3)
                                        {
                                            try
                                            {
                                                string Etc = QRCFDI.Substring(QRCFDI.Length - 8, 8);
                                                int tipoTrue = Convert.ToInt16(tipoPDF);
                                                contador = 0;
                                                string TemporalMedidor = DataResumen.Substring(0, DataResumen.IndexOf("|"));
                                                //dato  numero de medidor


                                                //datos a contar en dolares.
                                                if (TemporalMedidor != "")
                                                {
                                                    switch (tipoTrue)
                                                    {
                                                        case 1:


                                                            string temporaldata = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor = temporaldata.Substring(0, temporaldata.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor = temporaldata.Substring(temporaldata.IndexOf("|") + 1);
                                                            string pesos = temporalsinmedidor.Substring(0, temporalsinmedidor.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds = temporalsinmedidor.Substring(temporalsinmedidor.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor.Length; m++)
                                                            {
                                                                if (datosMedidor.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 7)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos.Length; m++)
                                                                {
                                                                    if (pesos.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds.Length; m++)
                                                                {
                                                                    if (donalds.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT,TipoCambio,Extras);
                                                                    PDF1(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        case 2:


                                                            string temporaldata2 = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor2 = temporaldata2.Substring(0, temporaldata2.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor2 = temporaldata2.Substring(temporaldata2.IndexOf("|") + 1);
                                                            string pesos2 = temporalsinmedidor2.Substring(0, temporalsinmedidor2.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds2 = temporalsinmedidor2.Substring(temporalsinmedidor2.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor2.Length; m++)
                                                            {
                                                                if (datosMedidor2.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 8)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos2.Length; m++)
                                                                {
                                                                    if (pesos2.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds2.Length; m++)
                                                                {
                                                                    if (donalds2.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT,TipoCambio,Extras);
                                                                    PDF2(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        case 3:
                                                            string datadevalidador = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datademedidor = datadevalidador.Substring(0, datadevalidador.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string quitadatamedidor = datadevalidador.Substring(datadevalidador.IndexOf("|") + 1);
                                                            string pejecoins = quitadatamedidor.Substring(0, quitadatamedidor.IndexOf("|"));
                                                            for (int m = 0; m < datademedidor.Length; m++)
                                                            {
                                                                if (datademedidor.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 2)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pejecoins.Length; m++)
                                                                {
                                                                    if (pejecoins.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                if (contador == 4)
                                                                {
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT,TipoCambio,Extras);
                                                                    PDF3(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";

                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";



                                                                }

                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";


                                                            }

                                                            break;
                                                        case 4:
                                                            string temporaldata4 = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor4 = temporaldata4.Substring(0, temporaldata4.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor4 = temporaldata4.Substring(temporaldata4.IndexOf("|") + 1);
                                                            string pesos4 = temporalsinmedidor4.Substring(0, temporalsinmedidor4.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds4 = temporalsinmedidor4.Substring(temporalsinmedidor4.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor4.Length; m++)
                                                            {
                                                                if (datosMedidor4.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 2)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos4.Length; m++)
                                                                {
                                                                    if (pesos4.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds4.Length; m++)
                                                                {
                                                                    if (donalds4.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT,TipoCambio,Extras);
                                                                    PDF4(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        default:
                                                            Resultado = "STATUS: ERROR XAXM07 \n ";

                                                            Resultado += "MENSAJE:  El formato PDF seleccionado no ha sido autorizado.";
                                                            break;

                                                    }
                                                }
                                                else
                                                {
                                                    Resultado = "STATUS: ERROR XAXM11 \n ";

                                                    Resultado += "MENSAJE:  Faltan datos en DataResumen, falta el No. de Medidor.";
                                                }

                                            }
                                            catch
                                            {
                                                Resultado = "STATUS: ERROR XAXM06 \n  ";

                                                Resultado += "MENSAJE:  Algun Dato de DataResumen y/o factura esta mal formateado.";
                                            }
                                        }
                                        else
                                        {
                                            Resultado = "STATUS: ERROR XAXM10 \n ";

                                            Resultado += "MENSAJE: Faltan datos en DatosResumen,deben ser separados por [|].";

                                        }
                                    }
                                    else
                                    {
                                        Resultado = "STATUS: ERROR XAXM09 \n ";

                                        Resultado += "MENSAJE: Faltan datos en factura,deben ser separados por [|].";


                                    }
                                }
                                else
                                {
                                    Resultado = "STATUS: ERROR XAXM08 \n ";

                                    Resultado += "MENSAJE: Faltan datos en factura, deben ser 3 elementos separados por [,] antes del slogan.";

                                }
                            }

                            else if (contador == 15 || contador == 16)
                            {

                                string facturatemporal = factura.Substring(0, factura.IndexOf("|"));
                                string[] validador = System.Text.RegularExpressions.Regex.Split(facturatemporal, ",");
                                string temporalfrase = factura.Substring(factura.IndexOf("|") + 1);
                                contador = 0;
                                foreach (var elements in validador)
                                {
                                    contador++;
                                }
                                if (contador == 3)
                                {
                                    contador = 0;
                                    for (int m = 0; m < temporalfrase.Length; m++)
                                    {
                                        if (temporalfrase.Substring(m, 1) == "|")
                                        {
                                            contador++;

                                        }
                                    }
                                    if (contador == 6)
                                    {
                                        contador = 0;
                                        for (int m = 0; m < DataResumen.Length; m++)
                                        {
                                            //if()
                                            if (DataResumen.Substring(m, 1) == "|")
                                            {
                                                contador++;

                                            }
                                        }
                                        if (contador == 3)
                                        {
                                            try
                                            {
                                                string Etc = QRCFDI.Substring(QRCFDI.Length - 8, 8);
                                                int tipoTrue = Convert.ToInt16(tipoPDF);
                                                contador = 0;
                                                string TemporalMedidor = DataResumen.Substring(0, DataResumen.IndexOf("|"));
                                                //dato  numero de medidor


                                                //datos a contar en dolares.
                                                if (TemporalMedidor != "")
                                                {
                                                    switch (tipoTrue)
                                                    {
                                                        case 1:


                                                            string temporaldata = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor = temporaldata.Substring(0, temporaldata.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor = temporaldata.Substring(temporaldata.IndexOf("|") + 1);
                                                            string pesos = temporalsinmedidor.Substring(0, temporalsinmedidor.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds = temporalsinmedidor.Substring(temporalsinmedidor.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor.Length; m++)
                                                            {
                                                                if (datosMedidor.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 7)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos.Length; m++)
                                                                {
                                                                    if (pesos.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds.Length; m++)
                                                                {
                                                                    if (donalds.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT,TipoCambio,Extras);
                                                                    PDF1(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        case 2:


                                                            string temporaldata2 = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor2 = temporaldata2.Substring(0, temporaldata2.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor2 = temporaldata2.Substring(temporaldata2.IndexOf("|") + 1);
                                                            string pesos2 = temporalsinmedidor2.Substring(0, temporalsinmedidor2.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds2 = temporalsinmedidor2.Substring(temporalsinmedidor2.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor2.Length; m++)
                                                            {
                                                                if (datosMedidor2.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 8)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos2.Length; m++)
                                                                {
                                                                    if (pesos2.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds2.Length; m++)
                                                                {
                                                                    if (donalds2.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT,TipoCambio,Extras);
                                                                    PDF2(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        case 3:
                                                            string datadevalidador = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datademedidor = datadevalidador.Substring(0, datadevalidador.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string quitadatamedidor = datadevalidador.Substring(datadevalidador.IndexOf("|") + 1);
                                                            string pejecoins = quitadatamedidor.Substring(0, quitadatamedidor.IndexOf("|"));
                                                            for (int m = 0; m < datademedidor.Length; m++)
                                                            {
                                                                if (datademedidor.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 2)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pejecoins.Length; m++)
                                                                {
                                                                    if (pejecoins.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                if (contador == 4)
                                                                {
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT, TipoCambio, Extras);
                                                                    PDF3(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";

                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";



                                                                }

                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";


                                                            }

                                                            break;
                                                        case 4:
                                                            string temporaldata4 = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor4 = temporaldata4.Substring(0, temporaldata4.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor4 = temporaldata4.Substring(temporaldata4.IndexOf("|") + 1);
                                                            string pesos4 = temporalsinmedidor4.Substring(0, temporalsinmedidor4.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds4 = temporalsinmedidor4.Substring(temporalsinmedidor4.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor4.Length; m++)
                                                            {
                                                                if (datosMedidor4.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 2)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos4.Length; m++)
                                                                {
                                                                    if (pesos4.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds4.Length; m++)
                                                                {
                                                                    if (donalds4.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT,TipoCambio,Extras);
                                                                    PDF4(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        default:
                                                            Resultado = "STATUS: ERROR XAXM07 \n ";

                                                            Resultado += "MENSAJE:  El formato PDF seleccionado no ha sido autorizado.";
                                                            break;

                                                    }
                                                }
                                                else
                                                {
                                                    Resultado = "STATUS: ERROR XAXM11 \n ";

                                                    Resultado += "MENSAJE:  Faltan datos en DataResumen, falta el No. de Medidor.";
                                                }

                                            }
                                            catch
                                            {
                                                Resultado = "STATUS: ERROR XAXM06 \n  ";

                                                Resultado += "MENSAJE:  Algun Dato de DataResumen y/o factura esta mal formateado.";
                                            }
                                        }
                                        else
                                        {
                                            Resultado = "STATUS: ERROR XAXM10 \n ";

                                            Resultado += "MENSAJE: Faltan datos en DatosResumen,deben ser separados por [|].";

                                        }
                                    }
                                    else
                                    {
                                        Resultado = "STATUS: ERROR XAXM09 \n ";

                                        Resultado += "MENSAJE: Faltan datos en factura,deben ser separados por [|].";


                                    }
                                }
                                else
                                {
                                    Resultado = "STATUS: ERROR XAXM08 \n ";

                                    Resultado += "MENSAJE: Faltan datos en factura, deben ser 3 elementos separados por [,] antes del slogan.";

                                }
                            }
                            else if (contador < 28 && contador > 0)
                            {

                                string facturatemporal = factura.Substring(0, factura.IndexOf("|"));
                                string[] validador = System.Text.RegularExpressions.Regex.Split(facturatemporal, ",");
                                string temporalfrase = factura.Substring(factura.IndexOf("|") + 1);
                                contador = 0;
                                foreach (var elements in validador)
                                {
                                    contador++;
                                }
                                if (contador == 3)
                                {
                                    contador = 0;
                                    for (int m = 0; m < temporalfrase.Length; m++)
                                    {
                                        if (temporalfrase.Substring(m, 1) == "|")
                                        {
                                            contador++;

                                        }
                                    }
                                    if (contador == 6)
                                    {
                                        contador = 0;
                                        for (int m = 0; m < DataResumen.Length; m++)
                                        {
                                            //if()
                                            if (DataResumen.Substring(m, 1) == "|")
                                            {
                                                contador++;

                                            }
                                        }
                                        if (contador == 3)
                                        {
                                            try
                                            {
                                                string Etc = QRCFDI.Substring(QRCFDI.Length - 8, 8);
                                                int tipoTrue = Convert.ToInt16(tipoPDF);
                                                contador = 0;
                                                string TemporalMedidor = DataResumen.Substring(0, DataResumen.IndexOf("|"));
                                                //dato  numero de medidor


                                                //datos a contar en dolares.
                                                if (TemporalMedidor != "")
                                                {
                                                    switch (tipoTrue)
                                                    {
                                                        case 1:


                                                            string temporaldata = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor = temporaldata.Substring(0, temporaldata.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor = temporaldata.Substring(temporaldata.IndexOf("|") + 1);
                                                            string pesos = temporalsinmedidor.Substring(0, temporalsinmedidor.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds = temporalsinmedidor.Substring(temporalsinmedidor.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor.Length; m++)
                                                            {
                                                                if (datosMedidor.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 7)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos.Length; m++)
                                                                {
                                                                    if (pesos.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds.Length; m++)
                                                                {
                                                                    if (donalds.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT, TipoCambio, Extras);
                                                                    PDF1(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        case 2:


                                                            string temporaldata2 = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor2 = temporaldata2.Substring(0, temporaldata2.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor2 = temporaldata2.Substring(temporaldata2.IndexOf("|") + 1);
                                                            string pesos2 = temporalsinmedidor2.Substring(0, temporalsinmedidor2.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds2 = temporalsinmedidor2.Substring(temporalsinmedidor2.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor2.Length; m++)
                                                            {
                                                                if (datosMedidor2.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 8)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos2.Length; m++)
                                                                {
                                                                    if (pesos2.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds2.Length; m++)
                                                                {
                                                                    if (donalds2.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT, TipoCambio, Extras);
                                                                    PDF2(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        case 3:
                                                            string datadevalidador = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datademedidor = datadevalidador.Substring(0, datadevalidador.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string quitadatamedidor = datadevalidador.Substring(datadevalidador.IndexOf("|") + 1);
                                                            string pejecoins = quitadatamedidor.Substring(0, quitadatamedidor.IndexOf("|"));
                                                            for (int m = 0; m < datademedidor.Length; m++)
                                                            {
                                                                if (datademedidor.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 2)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pejecoins.Length; m++)
                                                                {
                                                                    if (pejecoins.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                if (contador == 4)
                                                                {
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT, TipoCambio, Extras);
                                                                    PDF3(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";

                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";



                                                                }

                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";


                                                            }

                                                            break;
                                                        case 4:
                                                            string temporaldata4 = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
                                                            string datosMedidor4 = temporaldata4.Substring(0, temporaldata4.IndexOf("|"));
                                                            //datos a contar del medidor
                                                            string temporalsinmedidor4 = temporaldata4.Substring(temporaldata4.IndexOf("|") + 1);
                                                            string pesos4 = temporalsinmedidor4.Substring(0, temporalsinmedidor4.IndexOf("|"));
                                                            //datos a contar de pesos
                                                            string donalds4 = temporalsinmedidor4.Substring(temporalsinmedidor4.IndexOf("|") + 1);
                                                            for (int m = 0; m < datosMedidor4.Length; m++)
                                                            {
                                                                if (datosMedidor4.Substring(m, 1) == "/")
                                                                {
                                                                    contador++;

                                                                }
                                                            }
                                                            if (contador == 2)
                                                            {
                                                                contador = 0;
                                                                for (int m = 0; m < pesos4.Length; m++)
                                                                {
                                                                    if (pesos4.Substring(m, 1) == "/")
                                                                    {
                                                                        contador++;

                                                                    }
                                                                }
                                                                int contador2 = 0;
                                                                for (int m = 0; m < donalds4.Length; m++)
                                                                {
                                                                    if (donalds4.Substring(m, 1) == "/")
                                                                    {
                                                                        contador2++;

                                                                    }
                                                                }
                                                                if (contador < contador2)
                                                                {

                                                                    contador = 0;
                                                                    contador2 = 0;
                                                                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT, TipoCambio, Extras);
                                                                    PDF4(Grafica, tipoPDF, TipoCambio, factura, logotipo, DataResumen, Etc);
                                                                    Resultado = "STATUS:  EXITOSO \n ";
                                                                    Resultado += "MENSAJE:  " + StringConvertedToBase64;
                                                                }
                                                                else
                                                                {
                                                                    Resultado = "STATUS: ERROR XAXM13 \n ";

                                                                    Resultado += "MENSAJE: Faltan datos de conceptos en Pesos del resumen de conceptos.";


                                                                }
                                                            }
                                                            else
                                                            {
                                                                Resultado = "STATUS: ERROR XAXM12 \n ";

                                                                Resultado += "MENSAJE: Faltan datos referentes a los datos de medicion de consumo en DataResumen.";
                                                            }
                                                            break;
                                                        default:
                                                            Resultado = "STATUS: ERROR XAXM07 \n ";

                                                            Resultado += "MENSAJE:  El formato PDF seleccionado no ha sido autorizado.";
                                                            break;

                                                    }
                                                }
                                                else
                                                {
                                                    Resultado = "STATUS: ERROR XAXM11 \n ";

                                                    Resultado += "MENSAJE:  Faltan datos en DataResumen, falta el No. de Medidor.";
                                                }

                                            }
                                            catch
                                            {
                                                Resultado = "STATUS: ERROR XAXM06 \n  ";

                                                Resultado += "MENSAJE:  Algun Dato de DataResumen y/o factura esta mal formateado.";
                                            }
                                        }
                                        else
                                        {
                                            Resultado = "STATUS: ERROR XAXM10 \n ";

                                            Resultado += "MENSAJE: Faltan datos en DatosResumen,deben ser separados por [|].";

                                        }
                                    }
                                    else
                                    {
                                        Resultado = "STATUS: ERROR XAXM09 \n ";

                                        Resultado += "MENSAJE: Faltan datos en factura,deben ser separados por [|].";


                                    }
                                }
                                else
                                {
                                    Resultado = "STATUS: ERROR XAXM08 \n ";

                                    Resultado += "MENSAJE: Faltan datos en factura, deben ser 3 elementos separados por [,] antes del slogan.";

                                }
                            }
                            else
                            {
                                Resultado = "STATUS: ERROR XAXM05 \n ";

                                Resultado += "MENSAJE:Faltan datos en Grafica.";
                            }

                        }
                        else
                        {
                            Resultado = "STATUS: ERROR XAXM04 \n ";

                            Resultado += "MENSAJE: Favor de llenar los datos para la creación del PDF.";
                        }
                    }
                    else
                    {
                        Resultado = "STATUS: ERROR XAXM03 \n ";

                        Resultado += "MENSAJE: Es necesario los strings del timbrado.";
                    }
                    DeleteFiles();
                }
                else
                {
                    Resultado = "STATUS: ERROR XAXM02 \n ";

                    Resultado += "MENSAJE: Accesos Incorrectos.";
                }
            }
            else
            {
                Resultado = "STATUS: ERROR XAXM01 \n ";
                Resultado += "MENSAJE: Es necesario proporcionar los accesos al servicio";
            }
            return Resultado;
        }
        
        [WebMethod]
        public string NewPDF(string Login, string password, string Grafica, string TipoCambio, string factura, string logotipo, string DataResumen,string conceptos,string pesos,string dolares,string conceptosresumen, string QRCFDI, string QRSAT, string QRCERTSAT,string Extras)
        {
            string message="";
            if (Login != "" && password != "")
            {
                if (Login== "Ammper"&& password== "44mmp3r")
                {
                    AmmperPdf ammperPdf = new AmmperPdf();
                    string Etc = QRCFDI.Substring(QRCFDI.Length - 8, 8);
                    string Usoreal = Extras.Substring(0, Extras.IndexOf("|"));
                    string Real = Extras.Substring(Extras.IndexOf("|") + 1);
                    ImageCFDI4QR(QRCFDI, QRSAT, QRCERTSAT, TipoCambio, Real);
                    message = ammperPdf.ObtenerBase64(Grafica, TipoCambio, factura, logotipo, DataResumen, Etc, conceptos, pesos, dolares, conceptosresumen, Usoreal);
                }
                else
                {
                    message = "Accesos Incorrectos";
                    
                }
               
            }
            else
            {
                message = "Es necesario claves para acceder";
            }
            return message;
            
        }
            static void CreateGrafica1()
        {
            string path = @"c:\PruebasPDF\Resources\PDFGrap" + GraphicExcelVersion + ".xlsx";
            string destine = @"c:\PruebasPDF\Resources\grafico.png";

            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2013;

                application.ChartToImageConverter = new ChartToImageConverter();
                application.ChartToImageConverter.ScalingMode = ScalingMode.Best;

                Syncfusion.XlsIO.IWorkbook workbook = application.Workbooks.Open(path);
                IWorksheet worksheet = workbook.Worksheets[0];

                IChart chart = worksheet.Charts[0];

                //Creating the memory stream for chart image
                MemoryStream stream = new MemoryStream();

                //Saving the chart as image
                chart.SaveAsImage(stream);

                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                //Saving image stream to file
                image.Save(destine);
            }




            //String pathExcel = @"c:\PruebasPDF\Resources\PDFGrap1.xlsx";
            //string SaveImage= @"c:\PruebasPDF\Resources\grafico.png";
            //Workbook workbook = new Workbook();
            //workbook.LoadFromFile(pathExcel, ExcelVersion.Version2010);
            //Worksheet sheet = workbook.Worksheets[0];
            ////Image[] imgs = Convert.ChangeType(workbook.SaveChartAsImage(sheet), iTextSharp.text.Image[0]);
            ////Image[] Cimagen = workbook.SaveChartAsImage(sheet);
            //System.Drawing.Image[] Cimagen = workbook.SaveChartAsImage(sheet);
            //for (int i = 0; i < Cimagen.Length; i++)
            //{
            //    Cimagen[i].Save(string.Format(SaveImage, i), ImageFormat.Jpeg);
            //}






        }
        public static string GraphicExcelVersion;
        static void CopyPasteResources()
        {
            string path = @"c:\PruebasPDF\Resources\";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string rutaactual = HttpContext.Current.Server.MapPath(".");
            string pathSRC = @rutaactual + "/Resources/";
            foreach (var file in Directory.GetFiles(pathSRC))
            {
                File.Copy(file, System.IO.Path.Combine(path, System.IO.Path.GetFileName(file)), true);
            }

        }

        // PDF PDF-USD-CARGAS
        static void PDF1(string Grafica, string tipoPDF, string tipoCambio, string factura, string logotipo, string DataResumen, string Etc)
        {
            GraphicExcelVersion = tipoPDF;
            string info = Grafica.Substring(Grafica.IndexOf("|") + 1);
            string InfoForQR = factura.Substring(0, factura.IndexOf("|"));
            string workingInfo = factura.Substring(factura.IndexOf("|") + 1);
            CreateExcelData(info);
            CreateGrafica1();
            CreateQRImage(InfoForQR, Etc);
            string Status = "";

            //declaracion y extraccion de datos
            string catchPhrase = workingInfo.Substring(0, workingInfo.IndexOf("|")); //frase que se debe dividir en 2 renglones
            string temporalsincatch = workingInfo.Substring(workingInfo.IndexOf("|") + 1);
            string InfoTotalAPagar = temporalsincatch.Substring(0, temporalsincatch.IndexOf("|")); // total a pagar
            string temporalsintotalbill = temporalsincatch.Substring(temporalsincatch.IndexOf("|") + 1);
            string InfoPeriodoConsumo = temporalsintotalbill.Substring(0, temporalsintotalbill.IndexOf("|")); //periodo de consumo plaintext
            string temporalsinPeriodoConsumo = temporalsintotalbill.Substring(temporalsintotalbill.IndexOf("|") + 1);
            string InfoFechaLimiteBill = temporalsinPeriodoConsumo.Substring(0, temporalsinPeriodoConsumo.IndexOf("|"));//FechaLimite de pago
            string temporalSinfechalimite = temporalsinPeriodoConsumo.Substring(temporalsinPeriodoConsumo.IndexOf("|") + 1);
            string InfoNumeroDeDocumento = temporalSinfechalimite.Substring(0, temporalSinfechalimite.IndexOf("|"));//numero de documento
            string temporalsinnumerodocumento = temporalSinfechalimite.Substring(temporalSinfechalimite.IndexOf("|") + 1);
            string InfoRMU = temporalsinnumerodocumento.Substring(0, temporalsinnumerodocumento.IndexOf("|"));//DATORMU
            string InfoClientesPordividir = temporalsinnumerodocumento.Substring(temporalsinnumerodocumento.IndexOf("|") + 1);//DATOS A DIVIDIR CLIENTE

            //DIVIDIR INFORMACION DEL CLIENTE DE FACTURA
            String DivisorFiscal = "%";
            String[] FacturaCliente = System.Text.RegularExpressions.Regex.Split(InfoClientesPordividir, DivisorFiscal);


            //DIVIDIR CATCHPHRASE
            String separador = " ";
            String[] fraseSuper = System.Text.RegularExpressions.Regex.Split(catchPhrase, separador);
            int contador = 0;
            foreach (var element in fraseSuper)
            {
                contador++;

            }
            String Textosuperior = "", TextoInferior = "";
            if (contador > 1)
            {
                int totalinicio = contador / 2;
                totalinicio = totalinicio + 2;

                for (int m = 0; m < totalinicio; m++)
                {
                    Textosuperior = Textosuperior + fraseSuper[m] + " ";
                }
                for (int r = totalinicio; r < contador; r++)
                {
                    TextoInferior = TextoInferior + " " + fraseSuper[r];
                }
            }
            else
            {
                Textosuperior = " ";
                TextoInferior = catchPhrase + " ";
            }



            //declaracion y extraccion de datos resumen Divididos en secciones 
            string TemporalMedidor = DataResumen.Substring(0, DataResumen.IndexOf("|"));
            string TemporalInfoMedidor = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
            string TemporalSOLOMedidor = TemporalInfoMedidor.Substring(0, TemporalInfoMedidor.IndexOf("|"));
            string TemporalInfoResumen = TemporalInfoMedidor.Substring(TemporalInfoMedidor.IndexOf("|") + 1);
            string TemporalPesosMexicanos = TemporalInfoResumen.Substring(0, TemporalInfoResumen.IndexOf("|"));
            string TemporalDolares = TemporalInfoResumen.Substring(TemporalInfoResumen.IndexOf("|") + 1);
            //Extraccion datos de Medidor

            string InfoConsumoNeto = TemporalSOLOMedidor.Substring(0, TemporalSOLOMedidor.IndexOf("/"));
            string t1 = TemporalSOLOMedidor.Substring(TemporalSOLOMedidor.IndexOf("/") + 1);
            string InfoEC = t1.Substring(0, t1.IndexOf("/"));
            string t2 = t1.Substring(t1.IndexOf("/") + 1);
            string InfoEV = t2.Substring(0, t2.IndexOf("/"));
            string t3 = t2.Substring(t2.IndexOf("/") + 1);
            string InfoAJUSTETBFJIN = t3.Substring(0, t3.IndexOf("/"));
            string t4 = t3.Substring(t3.IndexOf("/") + 1);
            string InfoPNT = t4.Substring(0, t4.IndexOf("/"));
            string t5 = t4.Substring(t4.IndexOf("/") + 1);
            string InfoCEREM = t5.Substring(0, t5.IndexOf("/"));
            string t6 = t5.Substring(t5.IndexOf("/") + 1);
            string InfoTarifaSum = t6.Substring(0, t6.IndexOf("/"));
            string InfoPotencia = t6.Substring(t6.IndexOf("/") + 1);

            //Obtener Pesos mexicanos

            String pattern = "/";
            String[] PesosConceptos = System.Text.RegularExpressions.Regex.Split(TemporalPesosMexicanos, pattern);


            //Obtener Pesos dolares
            String[] ConceptosDoralesYtotal = System.Text.RegularExpressions.Regex.Split(TemporalDolares, pattern);




            try
            {
                Document doc = new Document(PageSize.LETTER);
                string Path = @"c:\PruebasPDF\PDF-USD-CARGAS.pdf";
                //Ruta
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Path, FileMode.Create));

                //metadatos archivo
                doc.AddTitle("PDF-Amper USD Cargas");
                doc.AddCreator("Prime Consultoria");
                doc.SetMargins(5, 5, 5, 5);
                //Abrir Archivo
                doc.Open();
                //Definir Styles
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardMedidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _Medidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _secundaryfont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _headers = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
                iTextSharp.text.Font _StandardBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _TOTALBOLD = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _SecundaryBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font _resumee = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5, iTextSharp.text.Font.NORMAL, BaseColor.RED);
                iTextSharp.text.Font _resumeeheader = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.RED);

                // Escribimos el encabezamiento en el documento

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 95;

                //header.= BaseColor.RED;


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\LOGOAMPER.png");
                gif.SetAbsolutePosition(20, 750);
                gif.ScaleToFit(100f, 200F);

                iTextSharp.text.Image CFID = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\test.png");
                CFID.SetAbsolutePosition(135, 130);
                CFID.ScaleToFit(200f, 150F);
                doc.Add(CFID);
                //Declarar columnas
                PdfPCell conusoenergia = new PdfPCell(new Phrase(Textosuperior, _StandardBold));
                conusoenergia.BorderWidth = 0;
                conusoenergia.HorizontalAlignment = 2;
                //conusoenergia.BorderWidthBottom = 0.75f;
                PdfPCell metasenmexico = new PdfPCell(new Phrase(TextoInferior, _StandardBold));
                metasenmexico.BorderWidth = 0;
                metasenmexico.HorizontalAlignment = 2;
                //espacio en blanco
                PdfPCell espacio = new PdfPCell(new Phrase(" ", _StandardBold));
                espacio.BorderWidth = 0;
                espacio.HorizontalAlignment = 2;
                //linea Roja
                PdfPCell linearoja = new PdfPCell(new Phrase(" ", _StandardBold));
                linearoja.BorderWidth = 0;
                linearoja.HorizontalAlignment = 2;
                linearoja.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                PdfPCell lineaBlanca = new PdfPCell(new Phrase(" ", _StandardBold));
                lineaBlanca.BorderWidth = 0;
                lineaBlanca.HorizontalAlignment = 2;
                lineaBlanca.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);


                //Agregar Columnas header
                header.AddCell(espacio);
                header.AddCell(conusoenergia);
                header.AddCell(metasenmexico);
                header.AddCell(linearoja);
                header.AddCell(lineaBlanca);
                //agregar datos 
                doc.Add(gif);


                //comparacion condicional segundo logotipo

                if (logotipo == "1")
                {
                    iTextSharp.text.Image secondhead = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\logocabezados.png");
                    secondhead.SetAbsolutePosition(140, 750);
                    secondhead.ScaleToFit(50f, 100F);
                    doc.Add(secondhead);
                }
                doc.Add(header);
                //GRAFICA Y CODIGO QR

                iTextSharp.text.Image grafica = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\grafico.png");
                grafica.SetAbsolutePosition(15, 280);
                grafica.ScaleToFit(320f, 200F);
                doc.Add(grafica);


                iTextSharp.text.Image qr = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\qr.png");
                qr.SetAbsolutePosition(15, 150);
                qr.ScaleToFit(175f, 110F);
                doc.Add(qr);

                //ELEMENTOS NUEVA IMPLEMENTACION 27/10/20XX
                iTextSharp.text.Image ResumenFiscal = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\FolioFiscalResumen.png");
                ResumenFiscal.SetAbsolutePosition(300, 630);
                ResumenFiscal.ScaleToFit(175f, 110F);
                doc.Add(ResumenFiscal);

                iTextSharp.text.Image ValoresAdefinir = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\ValoresADefinir.png");
                ValoresAdefinir.SetAbsolutePosition(50, 70);
                ValoresAdefinir.ScaleToFit(175f, 110F);
                doc.Add(ValoresAdefinir);

                //iTextSharp.text.Image FolioFiscalUnico = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\FolioFiscal.png");
                //FolioFiscalUnico.SetAbsolutePosition(150, 300);
                //FolioFiscalUnico.ScaleToFit(175f, 110F);
                //doc.Add(FolioFiscalUnico);

                //iTextSharp.text.Image GastosGenerales = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\GastosGenerales.png");
                //GastosGenerales.SetAbsolutePosition(150, 300);
                //GastosGenerales.ScaleToFit(175f, 110F);
                //doc.Add(GastosGenerales);



                //Pie de pagina
                PdfPTable tab = new PdfPTable(1);
                PdfPCell cell = new PdfPCell(new Phrase("Este documento es una representación impresa de un CFDI", _secundaryfont));
                cell.Border = 0;
                tab.TotalWidth = 300F;
                tab.AddCell(cell);
                tab.WriteSelectedRows(0, -1, 200, 40, writer.DirectContent);
                doc.Add(Chunk.NEWLINE);


                //TERMINO HEADER
                //Empiezo Datos Cliente
                PdfPTable DatosCliente = new PdfPTable(4);
                DatosCliente.WidthPercentage = 95;
                //cells Datos Cliente
                //CellSaltoDeLinea
                PdfPCell Salto = new PdfPCell(new Phrase(" ", _headers));
                Salto.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                Salto.BorderWidth = 0;
                Salto.HorizontalAlignment = 1;
                Salto.Colspan = 3;
                //Cellcoral


                //go
                PdfPCell Dependencia = new PdfPCell(new Phrase("AMMPER Energia, S.A.P.I. de C.V. ", _standardFont));
                Dependencia.Colspan = 3;
                Dependencia.BorderWidth = 0;
                Dependencia.HorizontalAlignment = 0;

                PdfPCell TotalPago = new PdfPCell(new Phrase(" Total a pagar ", _headers));
                TotalPago.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                TotalPago.BorderWidth = 0;
                TotalPago.HorizontalAlignment = 1;

                PdfPCell periodofact = new PdfPCell(new Phrase(" del periodo facturado ", _headers));
                periodofact.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodofact.BorderWidth = 0;
                periodofact.HorizontalAlignment = 1;

                PdfPCell calle = new PdfPCell(new Phrase("Av. Paseo de la Reforma ", _standardFont));
                calle.Colspan = 3;
                calle.BorderWidth = 0;
                calle.HorizontalAlignment = 0;

                PdfPCell coral = new PdfPCell(new Phrase(" ", _headers));
                coral.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                coral.BorderWidth = 0;
                coral.HorizontalAlignment = 1;



                PdfPCell numerodireccion = new PdfPCell(new Phrase("243 Piso 19", _standardFont));
                numerodireccion.Colspan = 3;
                numerodireccion.BorderWidth = 0;
                numerodireccion.HorizontalAlignment = 0;

                PdfPCell RegimenFiscal = new PdfPCell(new Phrase("Régimen Fiscal: 601-General de Ley de Personas Morales", _standardFont));
                RegimenFiscal.Colspan = 3;
                RegimenFiscal.BorderWidth = 0;
                RegimenFiscal.HorizontalAlignment = 0;

                PdfPCell GastosenGenaral = new PdfPCell(new Phrase("Uso CFDI: Gastos en General", _standardFont));
                GastosenGenaral.Colspan = 3;
                GastosenGenaral.BorderWidth = 0;
                GastosenGenaral.HorizontalAlignment = 0;

                PdfPCell numerospago = new PdfPCell(new Phrase("$" + InfoTotalAPagar, _TOTALBOLD));
                numerospago.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numerospago.BorderWidth = 0;
                numerospago.HorizontalAlignment = 1;

                PdfPCell codigopostal = new PdfPCell(new Phrase("Cuauhtémoc, CP.06500", _standardFont));
                codigopostal.Colspan = 3;
                codigopostal.BorderWidth = 0;
                codigopostal.HorizontalAlignment = 0;

                PdfPCell saltopagar = new PdfPCell(new Phrase(" ", _StandardBold));
                saltopagar.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                saltopagar.BorderWidth = 0;
                saltopagar.HorizontalAlignment = 1;

                PdfPCell ciudadfactura = new PdfPCell(new Phrase("Ciudad de México, México", _standardFont));
                ciudadfactura.Colspan = 3;
                ciudadfactura.BorderWidth = 0;
                ciudadfactura.HorizontalAlignment = 0;

                PdfPCell periodoconsumo = new PdfPCell(new Phrase("Periodo del Consumo ", _headers));
                periodoconsumo.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodoconsumo.BorderWidth = 0;
                periodoconsumo.HorizontalAlignment = 1;

                PdfPCell rfcamper = new PdfPCell(new Phrase("R. F. C. AEN160405E56", _secundaryfont));
                rfcamper.Colspan = 3;
                rfcamper.BorderWidth = 0;
                rfcamper.HorizontalAlignment = 0;

                PdfPCell fechasperiodo = new PdfPCell(new Phrase(InfoPeriodoConsumo, _StandardBold));
                fechasperiodo.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                fechasperiodo.BorderWidth = 0;
                fechasperiodo.HorizontalAlignment = 1;

                PdfPCell direccioncliente = new PdfPCell(new Phrase("Nombre y domicilio", _SecundaryBold));
                direccioncliente.Colspan = 3;
                direccioncliente.BorderWidth = 2;
                direccioncliente.HorizontalAlignment = 0;


                direccioncliente.BorderColor = new BaseColor(255, 0, 0);

                direccioncliente.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                //Datos cliente de Amper

                PdfPCell nombreydomicilio = new PdfPCell(new Phrase(FacturaCliente[0], _standardFont));
                nombreydomicilio.Colspan = 2;
                nombreydomicilio.BorderWidth = 0;
                nombreydomicilio.HorizontalAlignment = 0;

                PdfPCell INFORMACIONDIRECCIONCLIENTE = new PdfPCell(new Phrase(FacturaCliente[1], _standardFont));
                INFORMACIONDIRECCIONCLIENTE.Colspan = 2;
                INFORMACIONDIRECCIONCLIENTE.BorderWidth = 0;
                INFORMACIONDIRECCIONCLIENTE.HorizontalAlignment = 0;

                PdfPCell NumeroDoc = new PdfPCell(new Phrase("Número de documento", _SecundaryBold));
                NumeroDoc.BorderWidth = 2;
                NumeroDoc.HorizontalAlignment = 0;
                NumeroDoc.BorderColor = new BaseColor(255, 0, 0);
                NumeroDoc.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell taimlimit = new PdfPCell(new Phrase(" Fecha límite de pago", _headers));
                taimlimit.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                taimlimit.BorderWidth = 0;
                taimlimit.HorizontalAlignment = 1;

                PdfPCell espacioderelleno = new PdfPCell(new Phrase(" ", _standardFont));
                espacioderelleno.Colspan = 2;
                espacioderelleno.BorderWidth = 0;
                espacioderelleno.HorizontalAlignment = 0;

                PdfPCell elverdaderodoc = new PdfPCell(new Phrase(InfoNumeroDeDocumento, _standardFont));

                elverdaderodoc.BorderWidth = 0;
                elverdaderodoc.HorizontalAlignment = 0;

                PdfPCell lafechalimite = new PdfPCell(new Phrase(InfoFechaLimiteBill, _StandardBold));
                lafechalimite.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                lafechalimite.BorderWidth = 0;
                lafechalimite.HorizontalAlignment = 1;

                PdfPCell RMUtitulo = new PdfPCell(new Phrase("RMU", _SecundaryBold));
                RMUtitulo.BorderWidth = 2;
                RMUtitulo.HorizontalAlignment = 0;
                RMUtitulo.BorderColor = new BaseColor(255, 0, 0);
                RMUtitulo.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell telefonoemergencia = new PdfPCell(new Phrase(" Teléfono de emergencia", _headers));
                telefonoemergencia.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                telefonoemergencia.BorderWidth = 0;
                telefonoemergencia.HorizontalAlignment = 1;

                PdfPCell NUMRMU = new PdfPCell(new Phrase(InfoRMU, _standardFont));

                NUMRMU.BorderWidth = 0;
                NUMRMU.HorizontalAlignment = 0;

                PdfPCell numero8cientos = new PdfPCell(new Phrase("800 9532 911", _StandardBold));
                numero8cientos.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numero8cientos.BorderWidth = 0;
                numero8cientos.HorizontalAlignment = 1;

                PdfPCell MedicionConsum = new PdfPCell(new Phrase("", _SecundaryBold));
                MedicionConsum.BorderWidth = 0;
                MedicionConsum.HorizontalAlignment = 0;

                PdfPCell cientos8amper = new PdfPCell(new Phrase("", _StandardBold));
                cientos8amper.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                cientos8amper.BorderWidth = 0;
                cientos8amper.HorizontalAlignment = 1;




                //Agregar Celdas a Datoscliente
                DatosCliente.AddCell(Dependencia);
                DatosCliente.AddCell(TotalPago);
                DatosCliente.AddCell(RegimenFiscal);
                DatosCliente.AddCell(periodofact);
                DatosCliente.AddCell(calle);
                DatosCliente.AddCell(coral);
                DatosCliente.AddCell(numerodireccion);
                DatosCliente.AddCell(numerospago);
                
                DatosCliente.AddCell(codigopostal);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(ciudadfactura);
                DatosCliente.AddCell(periodoconsumo);
                DatosCliente.AddCell(rfcamper);
                DatosCliente.AddCell(fechasperiodo);
                DatosCliente.AddCell(GastosenGenaral);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(direccioncliente);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(nombreydomicilio);
                DatosCliente.AddCell(NumeroDoc);
                DatosCliente.AddCell(taimlimit);
                DatosCliente.AddCell(INFORMACIONDIRECCIONCLIENTE);
                DatosCliente.AddCell(elverdaderodoc);
                DatosCliente.AddCell(lafechalimite);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(RMUtitulo);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(telefonoemergencia);
                DatosCliente.AddCell(NUMRMU);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(numero8cientos);
                DatosCliente.AddCell(MedicionConsum);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(cientos8amper);



                //agregar datos 
                doc.Add(DatosCliente);

                //Tabla de medicion de consumo
                PdfPTable DatosdeConsumo = new PdfPTable(8);
                DatosdeConsumo.WidthPercentage = 95;

                //Declaracion de celdas
                //encabezados
                PdfPCell Nummedidor = new PdfPCell(new Phrase(" No. Medidor ", _SecundaryBold));
                Nummedidor.BorderWidth = 2;
                Nummedidor.HorizontalAlignment = 0;
                Nummedidor.BorderColor = new BaseColor(255, 0, 0);
                Nummedidor.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell Tituloconsumomensual = new PdfPCell(new Phrase(" Consumo Mensual (MWh) ", _SecundaryBold));
                Tituloconsumomensual.BorderWidth = 2;
                Tituloconsumomensual.HorizontalAlignment = Element.ALIGN_CENTER;
                Tituloconsumomensual.BorderColor = new BaseColor(255, 0, 0);
                Tituloconsumomensual.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Tituloconsumomensual.Colspan = 3;

                PdfPCell TarifaSumi = new PdfPCell(new Phrase(" Tarifa de Suministro ($/MWh) ", _SecundaryBold));
                TarifaSumi.BorderWidth = 2;
                TarifaSumi.HorizontalAlignment = Element.ALIGN_CENTER;
                TarifaSumi.BorderColor = new BaseColor(255, 0, 0);
                TarifaSumi.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                TarifaSumi.Colspan = 2;

                PdfPCell Superpotencia100 = new PdfPCell(new Phrase("Potencia 100 horas críticas (MW)", _SecundaryBold));
                Superpotencia100.BorderWidth = 2;
                Superpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                Superpotencia100.BorderColor = new BaseColor(255, 0, 0);
                Superpotencia100.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Superpotencia100.Colspan = 2;

                //Contenido

                //Dejar espacios blancos en medidor
                PdfPTable DatosdeConsumo2 = new PdfPTable(10);
                DatosdeConsumo2.WidthPercentage = 95;

                PdfPCell espaciomedidor = new PdfPCell(new Phrase(" ", _standardFont));
                espaciomedidor.BorderWidth = 0;
                espaciomedidor.HorizontalAlignment = 0;

                //dejar espacios en blanco en tarifa de suministros

                PdfPCell espaciotarifasumi = new PdfPCell(new Phrase(" ", _standardFont));
                espaciotarifasumi.BorderWidth = 0;
                espaciotarifasumi.HorizontalAlignment = 0;
                espaciotarifasumi.Colspan = 2;

                //dejar espacios en blanco en potencias 100!

                PdfPCell espaciopotencia100 = new PdfPCell(new Phrase(" ", _standardFont));
                espaciopotencia100.BorderWidth = 0;
                espaciopotencia100.HorizontalAlignment = 0;
                espaciopotencia100.Colspan = 2;

                //medidor
                PdfPCell cualmedidor = new PdfPCell(new Phrase(TemporalMedidor, _Medidor));
                cualmedidor.BorderWidth = 0;
                cualmedidor.HorizontalAlignment = 0;
                int anchoss = TemporalMedidor.Length;
                cualmedidor.Colspan = 2;
                

                //consumoneto

                PdfPCell consumoneto = new PdfPCell(new Phrase(" CONSUMO DE ENERGÍA NETO ", _standardMedidor));
                consumoneto.BorderWidth = 0;
                consumoneto.HorizontalAlignment = Element.ALIGN_LEFT;
                consumoneto.Colspan = 2;

                PdfPCell cantidadconsumoneto = new PdfPCell(new Phrase(InfoConsumoNeto + "", _standardMedidor));
                cantidadconsumoneto.BorderWidth = 0;
                cantidadconsumoneto.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Tarifa
                PdfPCell cantidadtarifa = new PdfPCell(new Phrase(InfoTarifaSum, _standardMedidor));
                cantidadtarifa.BorderWidth = 0;
                cantidadtarifa.HorizontalAlignment = Element.ALIGN_LEFT;

                PdfPCell conceptotarifa = new PdfPCell(new Phrase("", _standardMedidor));
                conceptotarifa.BorderWidth = 0;
                conceptotarifa.HorizontalAlignment = 0;

                //potencia super cantidad!
                PdfPCell cantidadpotencia100 = new PdfPCell(new Phrase(InfoPotencia, _standardMedidor));
                cantidadpotencia100.BorderWidth = 0;
                cantidadpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                cantidadpotencia100.Colspan = 2;

                //Consumo energia contratada 

                PdfPCell energiacontratada = new PdfPCell(new Phrase(" ENERGÍA CONTRATADA ", _standardMedidor));
                energiacontratada.BorderWidth = 0;
                energiacontratada.HorizontalAlignment = 0;
                energiacontratada.Colspan = 2;

                PdfPCell cantidadenergiacontratada = new PdfPCell(new Phrase(InfoEC + "", _standardMedidor));
                cantidadenergiacontratada.BorderWidth = 0;
                cantidadenergiacontratada.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia vendida 

                PdfPCell energiavendida = new PdfPCell(new Phrase(" ENERGÍA VENDIDA", _standardMedidor));
                energiavendida.BorderWidth = 0;
                energiavendida.HorizontalAlignment = 0;
                energiavendida.Colspan = 2;

                PdfPCell cantidadenergiaVENDIDA = new PdfPCell(new Phrase(InfoEV + "", _standardMedidor));
                cantidadenergiaVENDIDA.BorderWidth = 0;
                cantidadenergiaVENDIDA.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia TBFIN 

                PdfPCell TBFIN = new PdfPCell(new Phrase(" AJUSTE TBFIN ", _standardMedidor));
                TBFIN.BorderWidth = 0;
                TBFIN.HorizontalAlignment = 0;
                TBFIN.Colspan = 2;

                PdfPCell cantidadTBFIN = new PdfPCell(new Phrase(InfoAJUSTETBFJIN + "", _standardMedidor));
                cantidadTBFIN.BorderWidth = 0;
                cantidadTBFIN.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia PERDIDAS NO TECNICAS

                PdfPCell PERDIDASNOTECNICAS = new PdfPCell(new Phrase(" PERDIDAS NO TECNICAS ", _standardMedidor));
                PERDIDASNOTECNICAS.BorderWidth = 0;
                PERDIDASNOTECNICAS.HorizontalAlignment = 0;
                PERDIDASNOTECNICAS.Colspan = 2;

                PdfPCell cantidadePERDIDASNOTECNICAS = new PdfPCell(new Phrase(InfoPNT + "", _standardMedidor));
                cantidadePERDIDASNOTECNICAS.BorderWidth = 0;
                cantidadePERDIDASNOTECNICAS.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia SUPERMEM

                PdfPCell SUPERMEM = new PdfPCell(new Phrase(" CONSUMO DE ENERGÍA MEM ", _standardMedidor));
                SUPERMEM.BorderWidth = 0;
                SUPERMEM.HorizontalAlignment = 0;
                SUPERMEM.Colspan = 2;

                PdfPCell CANDITADSUPERMEM = new PdfPCell(new Phrase(InfoCEREM + "", _standardMedidor));
                CANDITADSUPERMEM.BorderWidth = 0;
                CANDITADSUPERMEM.HorizontalAlignment = Element.ALIGN_RIGHT;





                //Agregar celdas a datos consumo

                DatosdeConsumo.AddCell(Nummedidor);
                DatosdeConsumo.AddCell(Tituloconsumomensual);
                DatosdeConsumo.AddCell(TarifaSumi);
                DatosdeConsumo.AddCell(Superpotencia100);

                DatosdeConsumo2.AddCell(cualmedidor);
                DatosdeConsumo2.AddCell(consumoneto);
                DatosdeConsumo2.AddCell(cantidadconsumoneto);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(cantidadtarifa);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(cantidadpotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(energiacontratada);
                DatosdeConsumo2.AddCell(cantidadenergiacontratada);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(energiavendida);
                DatosdeConsumo2.AddCell(cantidadenergiaVENDIDA);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(TBFIN);
                DatosdeConsumo2.AddCell(cantidadTBFIN);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(PERDIDASNOTECNICAS);
                DatosdeConsumo2.AddCell(cantidadePERDIDASNOTECNICAS);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(SUPERMEM);
                DatosdeConsumo2.AddCell(CANDITADSUPERMEM);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);



                //AgregardatosConsumo
                doc.Add(DatosdeConsumo);
                doc.Add(DatosdeConsumo2);
                doc.Add(Chunk.NEWLINE);

                //tabla Desglosada de resumen
                PdfPTable Resumen = new PdfPTable(14);
                Resumen.WidthPercentage = 100;

                //Espacios Blancos de resumen

                PdfPCell blancosresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosresumen.BorderWidth = 0;
                blancosresumen.HorizontalAlignment = 0;
                blancosresumen.Colspan = 8;

                PdfPCell blancosfinalresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosfinalresumen.BorderWidth = 0;
                blancosfinalresumen.HorizontalAlignment = 0;

                //Crear datos encabezado resumee

                PdfPCell concepto = new PdfPCell(new Phrase("Concepto ", _resumeeheader));
                concepto.BorderWidth = 1;
                concepto.HorizontalAlignment = 0;
                concepto.BorderColor = new BaseColor(255, 0, 0);
                concepto.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                concepto.Colspan = 3;

                PdfPCell tarifa = new PdfPCell(new Phrase(tipoCambio +"/MWh ", _resumeeheader));
                tarifa.BorderWidth = 1;
                tarifa.HorizontalAlignment = 1;
                tarifa.BorderColor = new BaseColor(255, 0, 0);
                tarifa.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;


                PdfPCell totaldolarucos = new PdfPCell(new Phrase(" Total  " + tipoCambio, _resumeeheader));
                totaldolarucos.BorderWidth = 1;
                totaldolarucos.HorizontalAlignment = 1;
                totaldolarucos.BorderColor = new BaseColor(255, 0, 0);
                totaldolarucos.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                //Comparacion de datos resumee Pesos
                for (int a = 0; a < 23; a++)
                {
                    string pesostemporales = PesosConceptos[a];
                    if (PesosConceptos[a] != "")
                    {
                        if (PesosConceptos[a].Substring(0, 1) == "-")
                        {
                            PesosConceptos[a] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            PesosConceptos[a] = " $" + pesostemporales;

                        }
                    }
                }
                //Comparacion de datos Resume Dolarucos
                for (int a = 0; a < 27; a++)
                {
                    string pesostemporales = ConceptosDoralesYtotal[a];
                    if (ConceptosDoralesYtotal[a] != "")
                    {
                        if (ConceptosDoralesYtotal[a].Substring(0, 1) == "-")
                        {
                            ConceptosDoralesYtotal[a] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            ConceptosDoralesYtotal[a] = " $" + pesostemporales;

                        }
                    }
                }
                //Creardatos resumee

                PdfPCell ENERGIACONTRATADA = new PdfPCell(new Phrase("ENERGÍA CONTRATADA ", _resumee));
                ENERGIACONTRATADA.BorderWidth = 0;
                ENERGIACONTRATADA.HorizontalAlignment = 0;
                ENERGIACONTRATADA.Colspan = 3;

                PdfPCell ECT = new PdfPCell(new Phrase(PesosConceptos[0], _resumee));
                ECT.BorderWidth = 0;
                ECT.HorizontalAlignment = 2;

                PdfPCell ECUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[0], _resumee));
                ECUSD.BorderWidth = 0;
                ECUSD.HorizontalAlignment = 2;
                //ENERGIA CONTRATADA

                PdfPCell ENERGIAMEM = new PdfPCell(new Phrase("ENERGÍA MEM ", _resumee));
                ENERGIAMEM.BorderWidth = 0;
                ENERGIAMEM.HorizontalAlignment = 0;
                ENERGIAMEM.Colspan = 3;

                PdfPCell EMT = new PdfPCell(new Phrase(PesosConceptos[1], _resumee));
                EMT.BorderWidth = 0;
                EMT.HorizontalAlignment = 2;

                PdfPCell EMUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[1], _resumee));
                EMUSD.BorderWidth = 0;
                EMUSD.HorizontalAlignment = 2;
                //ENERGIA MEM

                PdfPCell ENERGIAVENDIDA = new PdfPCell(new Phrase("ENERGÍA VENDIDA", _resumee));
                ENERGIAVENDIDA.BorderWidth = 0;
                ENERGIAVENDIDA.HorizontalAlignment = 0;
                ENERGIAVENDIDA.Colspan = 3;

                PdfPCell EVT = new PdfPCell(new Phrase(PesosConceptos[2], _resumee));
                EVT.BorderWidth = 0;
                EVT.HorizontalAlignment = 2;

                PdfPCell EVUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[2], _resumee));
                EVUSD.BorderWidth = 0;
                EVUSD.HorizontalAlignment = 2;
                //ENERGIA VENDIDA

                PdfPCell COSTOSOPERATIVOS = new PdfPCell(new Phrase("COSTOS OPERATIVOS", _resumee));
                COSTOSOPERATIVOS.BorderWidth = 0;
                COSTOSOPERATIVOS.HorizontalAlignment = 0;
                COSTOSOPERATIVOS.Colspan = 3;

                PdfPCell COT = new PdfPCell(new Phrase(PesosConceptos[3], _resumee));
                COT.BorderWidth = 0;
                COT.HorizontalAlignment = 2;

                PdfPCell COUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[3], _resumee));
                COUSD.BorderWidth = 0;
                COUSD.HorizontalAlignment = 2;
                //COSTOS OPERATIVOS

                PdfPCell AJUSTE = new PdfPCell(new Phrase("AJUSTE", _resumee));
                AJUSTE.BorderWidth = 0;
                AJUSTE.HorizontalAlignment = 0;
                AJUSTE.Colspan = 3;

                PdfPCell AJT = new PdfPCell(new Phrase(PesosConceptos[4], _resumee));
                AJT.BorderWidth = 0;
                AJT.HorizontalAlignment = 2;

                PdfPCell AJUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[4], _resumee));
                AJUSD.BorderWidth = 0;
                AJUSD.HorizontalAlignment = 2;
                //AJUSTE

                PdfPCell DESVIACION = new PdfPCell(new Phrase("DESVIACIÓN", _resumee));
                DESVIACION.BorderWidth = 0;
                DESVIACION.HorizontalAlignment = 0;
                DESVIACION.Colspan = 3;

                PdfPCell DET = new PdfPCell(new Phrase(PesosConceptos[5], _resumee));
                DET.BorderWidth = 0;
                DET.HorizontalAlignment = 2;

                PdfPCell DEUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[5], _resumee));
                DEUSD.BorderWidth = 0;
                DEUSD.HorizontalAlignment = 2;
                //DESVIACION

                PdfPCell RPERDIDASNOTECNICAS = new PdfPCell(new Phrase("PÉRDIDAS NO TÉCNICAS", _resumee));
                RPERDIDASNOTECNICAS.BorderWidth = 0;
                RPERDIDASNOTECNICAS.HorizontalAlignment = 0;
                RPERDIDASNOTECNICAS.Colspan = 3;

                PdfPCell PNTT = new PdfPCell(new Phrase(PesosConceptos[6], _resumee));
                PNTT.BorderWidth = 0;
                PNTT.HorizontalAlignment = 2;

                PdfPCell PNTUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[6], _resumee));
                PNTUSD.BorderWidth = 0;
                PNTUSD.HorizontalAlignment = 2;
                //PERDIDAS NO TECNICAS

                PdfPCell PERDIDASDELARED = new PdfPCell(new Phrase("PÉRDIDAS DE LA RED", _resumee));
                PERDIDASDELARED.BorderWidth = 0;
                PERDIDASDELARED.HorizontalAlignment = 0;
                PERDIDASDELARED.Colspan = 3;

                PdfPCell PDLRT = new PdfPCell(new Phrase(PesosConceptos[7], _resumee));
                PDLRT.BorderWidth = 0;
                PDLRT.HorizontalAlignment = 2;

                PdfPCell PDLRUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[7], _resumee));
                PDLRUSD.BorderWidth = 0;
                PDLRUSD.HorizontalAlignment = 2;
                //PERDIDAS DE LA RED

                PdfPCell SERVICIOSCONEXOS = new PdfPCell(new Phrase("SERVICIOS CONEXOS", _resumee));
                SERVICIOSCONEXOS.BorderWidth = 0;
                SERVICIOSCONEXOS.HorizontalAlignment = 0;
                SERVICIOSCONEXOS.Colspan = 3;

                PdfPCell SCT = new PdfPCell(new Phrase(PesosConceptos[8], _resumee));
                SCT.BorderWidth = 0;
                SCT.HorizontalAlignment = 2;

                PdfPCell SCUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[8], _resumee));
                SCUSD.BorderWidth = 0;
                SCUSD.HorizontalAlignment = 2;
                //SERVICIOS CONEXOS

                PdfPCell COSTOOPERACIONCENACE = new PdfPCell(new Phrase("COSTO DE OPERACIÓN DE CENACE CARGA", _resumee));
                COSTOOPERACIONCENACE.BorderWidth = 0;
                COSTOOPERACIONCENACE.HorizontalAlignment = 0;
                COSTOOPERACIONCENACE.Colspan = 3;

                PdfPCell COCCT = new PdfPCell(new Phrase(PesosConceptos[9], _resumee));
                COCCT.BorderWidth = 0;
                COCCT.HorizontalAlignment = 2;

                PdfPCell COCCUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[9], _resumee));
                COCCUSD.BorderWidth = 0;
                COCCUSD.HorizontalAlignment = 2;
                //COSTO DE OPERACION DEL CENACE CARGA

                PdfPCell OPERACIONCENACEGENERADOR = new PdfPCell(new Phrase("COSTO DE OPERACIÓN DE CENACE GENERADOR", _resumee));
                OPERACIONCENACEGENERADOR.BorderWidth = 0;
                OPERACIONCENACEGENERADOR.HorizontalAlignment = 0;
                OPERACIONCENACEGENERADOR.Colspan = 3;

                PdfPCell COCGT = new PdfPCell(new Phrase(PesosConceptos[10], _resumee));
                COCGT.BorderWidth = 0;
                COCGT.HorizontalAlignment = 2;

                PdfPCell COCGUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[10], _resumee));
                COCGUSD.BorderWidth = 0;
                COCGUSD.HorizontalAlignment = 2;
                //COSTO DE OPERACION DEL CENACE GENERADOR

                PdfPCell GARANTIASUFICIENCIA = new PdfPCell(new Phrase("GARANTÍA DE SUFICIENCIA", _resumee));
                GARANTIASUFICIENCIA.BorderWidth = 0;
                GARANTIASUFICIENCIA.HorizontalAlignment = 0;
                GARANTIASUFICIENCIA.Colspan = 3;

                PdfPCell GDST = new PdfPCell(new Phrase(PesosConceptos[11], _resumee));
                GDST.BorderWidth = 0;
                GDST.HorizontalAlignment = 2;

                PdfPCell GDSUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[11], _resumee));
                GDSUSD.BorderWidth = 0;
                GDSUSD.HorizontalAlignment = 2;
                //GARANTIA DE SUFICIENCIA

                PdfPCell GARANTIANTECENACE = new PdfPCell(new Phrase("GARANTÍA REGULATORIA ANTE EL CENACE", _resumee));
                GARANTIANTECENACE.BorderWidth = 0;
                GARANTIANTECENACE.HorizontalAlignment = 0;
                GARANTIANTECENACE.Colspan = 3;

                PdfPCell GRACT = new PdfPCell(new Phrase(PesosConceptos[12], _resumee));
                GRACT.BorderWidth = 0;
                GRACT.HorizontalAlignment = 2;

                PdfPCell GRACUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[12], _resumee));
                GRACUSD.BorderWidth = 0;
                GRACUSD.HorizontalAlignment = 2;
                //GARANTIA REGULATORIA ANTE CENACE

                PdfPCell TRANSMISIÓNCARGA = new PdfPCell(new Phrase("TRANSMISIÓN CARGA", _resumee));
                TRANSMISIÓNCARGA.BorderWidth = 0;
                TRANSMISIÓNCARGA.HorizontalAlignment = 0;
                TRANSMISIÓNCARGA.Colspan = 3;

                PdfPCell TCT = new PdfPCell(new Phrase(PesosConceptos[13], _resumee));
                TCT.BorderWidth = 0;
                TCT.HorizontalAlignment = 2;

                PdfPCell TCUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[13], _resumee));
                TCUSD.BorderWidth = 0;
                TCUSD.HorizontalAlignment = 2;
                //TRANSMISIÓN CARGA

                PdfPCell TRANSMISIÓNGENERADOR = new PdfPCell(new Phrase("TRANSMISIÓN GENERADOR", _resumee));
                TRANSMISIÓNGENERADOR.BorderWidth = 0;
                TRANSMISIÓNGENERADOR.HorizontalAlignment = 0;
                TRANSMISIÓNGENERADOR.Colspan = 3;

                PdfPCell TGT = new PdfPCell(new Phrase(PesosConceptos[14], _resumee));
                TGT.BorderWidth = 0;
                TGT.HorizontalAlignment = 2;

                PdfPCell TGUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[14], _resumee));
                TGUSD.BorderWidth = 0;
                TGUSD.HorizontalAlignment = 2;
                //TRANSMISIÓN GENERADOR

                PdfPCell POTENCIA = new PdfPCell(new Phrase("POTENCIA", _resumee));
                POTENCIA.BorderWidth = 0;
                POTENCIA.HorizontalAlignment = 0;
                POTENCIA.Colspan = 3;

                PdfPCell PT = new PdfPCell(new Phrase(PesosConceptos[15], _resumee));
                PT.BorderWidth = 0;
                PT.HorizontalAlignment = 2;

                PdfPCell PUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[15], _resumee));
                PUSD.BorderWidth = 0;
                PUSD.HorizontalAlignment = 2;
                //POTENCIA

                PdfPCell SERVICIODISTRIBUCION = new PdfPCell(new Phrase("SERVICIO DE DISTRIBUCIÓN", _resumee));
                SERVICIODISTRIBUCION.BorderWidth = 0;
                SERVICIODISTRIBUCION.HorizontalAlignment = 0;
                SERVICIODISTRIBUCION.Colspan = 3;

                PdfPCell SDT = new PdfPCell(new Phrase(PesosConceptos[16], _resumee));
                SDT.BorderWidth = 0;
                SDT.HorizontalAlignment = 2;

                PdfPCell SDUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[16], _resumee));
                SDUSD.BorderWidth = 0;
                SDUSD.HorizontalAlignment = 2;
                //SERVICIOS DE DISTRIBUCION

                PdfPCell CONGESTIONMEM = new PdfPCell(new Phrase("CONGESTIÓN MEM", _resumee));
                CONGESTIONMEM.BorderWidth = 0;
                CONGESTIONMEM.HorizontalAlignment = 0;
                CONGESTIONMEM.Colspan = 3;

                PdfPCell CMT = new PdfPCell(new Phrase(PesosConceptos[17], _resumee));
                CMT.BorderWidth = 0;
                CMT.HorizontalAlignment = 2;

                PdfPCell CMUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[17], _resumee));
                CMUSD.BorderWidth = 0;
                CMUSD.HorizontalAlignment = 2;
                //CONGESTION MEM

                PdfPCell CONGESTIONGENERADOR = new PdfPCell(new Phrase("CONGESTIÓN GENERADOR", _resumee));
                CONGESTIONGENERADOR.BorderWidth = 0;
                CONGESTIONGENERADOR.HorizontalAlignment = 0;
                CONGESTIONGENERADOR.Colspan = 3;

                PdfPCell CGT = new PdfPCell(new Phrase(PesosConceptos[18], _resumee));
                CGT.BorderWidth = 0;
                CGT.HorizontalAlignment = 2;

                PdfPCell CGUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[18], _resumee));
                CGUSD.BorderWidth = 0;
                CGUSD.HorizontalAlignment = 2;
                //CONGESTION GENERADOR

                PdfPCell CERTIFICADOSENERGIALIMPIA = new PdfPCell(new Phrase("CERTIFICADOS DE ENERGÍA LIMPIA", _resumee));
                CERTIFICADOSENERGIALIMPIA.BorderWidth = 0;
                CERTIFICADOSENERGIALIMPIA.HorizontalAlignment = 0;
                CERTIFICADOSENERGIALIMPIA.Colspan = 3;

                PdfPCell CELT = new PdfPCell(new Phrase(PesosConceptos[19], _resumee));
                CELT.BorderWidth = 0;
                CELT.HorizontalAlignment = 2;

                PdfPCell CELUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[19], _resumee));
                CELUSD.BorderWidth = 0;
                CELUSD.HorizontalAlignment = 2;
                //CERTIFICADOS ENERGIA LIMPIA

                PdfPCell ADICIONALESCERTIFICADOSENERGIALIMPIA = new PdfPCell(new Phrase("CERTIFICADOS DE ENERGÍA LIMPIA ADICIONALES", _resumee));
                ADICIONALESCERTIFICADOSENERGIALIMPIA.BorderWidth = 0;
                ADICIONALESCERTIFICADOSENERGIALIMPIA.HorizontalAlignment = 0;
                ADICIONALESCERTIFICADOSENERGIALIMPIA.Colspan = 3;

                PdfPCell CELAT = new PdfPCell(new Phrase(PesosConceptos[20], _resumee));
                CELAT.BorderWidth = 0;
                CELAT.HorizontalAlignment = 2;

                PdfPCell CELAUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[20], _resumee));
                CELAUSD.BorderWidth = 0;
                CELAUSD.HorizontalAlignment = 2;
                //CERTIFICADOS ENERGIA LIMPIA ADICIONALES

                PdfPCell RELIQUIDACIONES = new PdfPCell(new Phrase("RELIQUIDACIONES", _resumee));
                RELIQUIDACIONES.BorderWidth = 0;
                RELIQUIDACIONES.HorizontalAlignment = 0;
                RELIQUIDACIONES.Colspan = 3;

                PdfPCell RLT = new PdfPCell(new Phrase(PesosConceptos[21], _resumee));
                RLT.BorderWidth = 0;
                RLT.HorizontalAlignment = 2;

                PdfPCell RLUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[21], _resumee));
                RLUSD.BorderWidth = 0;
                RLUSD.HorizontalAlignment = 2;
                //RELIQUIDACIONES

                PdfPCell OTROSCARGOSYABONOS = new PdfPCell(new Phrase("OTROS CARGOS Y/O ABONOS", _resumee));
                OTROSCARGOSYABONOS.BorderWidth = 0;
                OTROSCARGOSYABONOS.HorizontalAlignment = 0;
                OTROSCARGOSYABONOS.Colspan = 3;

                PdfPCell OCYAT = new PdfPCell(new Phrase(PesosConceptos[22], _resumee));
                OCYAT.BorderWidth = 0;
                OCYAT.HorizontalAlignment = 2;

                PdfPCell OCYAUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[22], _resumee));
                OCYAUSD.BorderWidth = 0;
                OCYAUSD.HorizontalAlignment = 2;
                // OTROS CARGOS Y ABONOS

                PdfPCell AMMPER = new PdfPCell(new Phrase("AMMPER", _resumee));
                AMMPER.BorderWidth = 0;
                AMMPER.HorizontalAlignment = 0;
                AMMPER.Colspan = 3;

                PdfPCell AMMT = new PdfPCell(new Phrase(PesosConceptos[23], _resumee));
                AMMT.BorderWidth = 0;
                AMMT.HorizontalAlignment = 2;

                PdfPCell AMMUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[23], _resumee));
                AMMUSD.BorderWidth = 0;
                AMMUSD.HorizontalAlignment = 2;
                // AMMPER

                PdfPCell SUBTOTALRESUMEE = new PdfPCell(new Phrase("SUBTOTAL", _resumee));
                SUBTOTALRESUMEE.BorderWidth = 0;
                SUBTOTALRESUMEE.HorizontalAlignment = 0;
                SUBTOTALRESUMEE.Colspan = 3;


                PdfPCell SUBTRUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[24], _resumee));
                SUBTRUSD.BorderWidth = 0;
                SUBTRUSD.HorizontalAlignment = 2;
                // subtotal RESUMEE

                PdfPCell IVARESUMEE = new PdfPCell(new Phrase("IVA", _resumee));
                IVARESUMEE.BorderWidth = 0;
                IVARESUMEE.HorizontalAlignment = 0;
                IVARESUMEE.Colspan = 3;


                PdfPCell IVARUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[25], _resumee));
                IVARUSD.BorderWidth = 0;
                IVARUSD.HorizontalAlignment = 2;
                // IVA

                PdfPCell TOTALRESUMEE = new PdfPCell(new Phrase("TOTAL", _resumee));
                TOTALRESUMEE.BorderWidth = 0;
                TOTALRESUMEE.HorizontalAlignment = 0;
                TOTALRESUMEE.Colspan = 3;


                PdfPCell TOTALRUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[26], _resumee));
                TOTALRUSD.BorderWidth = 0;
                TOTALRUSD.HorizontalAlignment = 2;
                // TOTAL RESUMEE

                PdfPCell TIPOEXCHANGE = new PdfPCell(new Phrase("TIPO DE CAMBIO", _resumee));
                TIPOEXCHANGE.BorderWidth = 0;
                TIPOEXCHANGE.HorizontalAlignment = 0;
                TIPOEXCHANGE.Colspan = 3;


                PdfPCell TIPOEXCHRUSD = new PdfPCell(new Phrase(ConceptosDoralesYtotal[27], _resumee));
                TIPOEXCHRUSD.BorderWidth = 0;
                TIPOEXCHRUSD.HorizontalAlignment = 2;
                // TIPO DE CAMBIO

                //Agregar celdas a datos consumo
                //INICIO ENCABEZADO RESUMEN
                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(concepto);
                Resumen.AddCell(tarifa);
                Resumen.AddCell(totaldolarucos);
                Resumen.AddCell(blancosfinalresumen);
                //FIN ENCABEZADO RESUMEN
                if (PesosConceptos[0] != "")
                {

                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIACONTRATADA);
                    Resumen.AddCell(ECT);
                    Resumen.AddCell(ECUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[1] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIAMEM);
                    Resumen.AddCell(EMT);
                    Resumen.AddCell(EMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }
                if (PesosConceptos[2] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIAVENDIDA);
                    Resumen.AddCell(EVT);
                    Resumen.AddCell(EVUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[3] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(COSTOSOPERATIVOS);
                    Resumen.AddCell(COT);
                    Resumen.AddCell(COUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[4] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(AJUSTE);
                    Resumen.AddCell(AJT);
                    Resumen.AddCell(AJUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[5] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(DESVIACION);
                    Resumen.AddCell(DET);
                    Resumen.AddCell(DEUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[6] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(RPERDIDASNOTECNICAS);
                    Resumen.AddCell(PNTT);
                    Resumen.AddCell(PNTUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[7] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(PERDIDASDELARED);
                    Resumen.AddCell(PDLRT);
                    Resumen.AddCell(PDLRUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[8] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(SERVICIOSCONEXOS);
                    Resumen.AddCell(SCT);
                    Resumen.AddCell(SCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }
                if (PesosConceptos[9] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(COSTOOPERACIONCENACE);
                    Resumen.AddCell(COCCT);
                    Resumen.AddCell(COCCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[10] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(OPERACIONCENACEGENERADOR);
                    Resumen.AddCell(COCGT);
                    Resumen.AddCell(COCGUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[11] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(GARANTIASUFICIENCIA);
                    Resumen.AddCell(GDST);
                    Resumen.AddCell(GDSUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[12] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(GARANTIANTECENACE);
                    Resumen.AddCell(GRACT);
                    Resumen.AddCell(GRACUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[13] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(TRANSMISIÓNCARGA);
                    Resumen.AddCell(TCT);
                    Resumen.AddCell(TCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }
                if (PesosConceptos[14] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(TRANSMISIÓNGENERADOR);
                    Resumen.AddCell(TGT);
                    Resumen.AddCell(TGUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[15] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(POTENCIA);
                    Resumen.AddCell(PT);
                    Resumen.AddCell(PUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[16] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(SERVICIODISTRIBUCION);
                    Resumen.AddCell(SDT);
                    Resumen.AddCell(SDUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[17] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(CONGESTIONMEM);
                    Resumen.AddCell(CMT);
                    Resumen.AddCell(CMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }
                if (PesosConceptos[18] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(CONGESTIONGENERADOR);
                    Resumen.AddCell(CGT);
                    Resumen.AddCell(CGUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[19] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(CERTIFICADOSENERGIALIMPIA);
                    Resumen.AddCell(CELT);
                    Resumen.AddCell(CELUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[20] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ADICIONALESCERTIFICADOSENERGIALIMPIA);
                    Resumen.AddCell(CELAT);
                    Resumen.AddCell(CELAUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[21] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(RELIQUIDACIONES);
                    Resumen.AddCell(RLT);
                    Resumen.AddCell(RLUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[22] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(OTROSCARGOSYABONOS);
                    Resumen.AddCell(OCYAT);
                    Resumen.AddCell(OCYAUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (PesosConceptos[23] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(AMMPER);
                    Resumen.AddCell(AMMT);
                    Resumen.AddCell(AMMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }


                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(SUBTOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(SUBTRUSD);
                Resumen.AddCell(blancosfinalresumen);



                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(IVARESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(IVARUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TOTALRUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TIPOEXCHANGE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TIPOEXCHRUSD);
                Resumen.AddCell(blancosfinalresumen);
                //Agregardatosresumen
                doc.Add(Resumen);



                //tabla de paginacion
                //PdfPTable Paginacion = new PdfPTable(1);
                //Paginacion.WidthPercentage = 95;
                //Paginacion.SetAbsolutePosition(0,10);

                doc.Close();
                writer.Close();
                Status = "Proceso exitoso, terminado a las:" + DateTime.Today.ToLongTimeString();
                UpdateLogFile(Status);
                Byte[] bytespdf = File.ReadAllBytes(Path);
                StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
            }
            catch (Exception ex)
            {
                Status = "Hubo un error a las " + DateTime.Today.ToLongTimeString() + ex;
                UpdateLogFile(Status);
            }

        }

        //PDF PDF_ABASTO_AISLADO_CARGAS
        static void PDF2(string Grafica, string tipoPDF, string tipoCambio, string factura, string logotipo, string DataResumen, string Etc)
        {

            GraphicExcelVersion = tipoPDF;
            string info = Grafica.Substring(Grafica.IndexOf("|") + 1);
            string InfoForQR = factura.Substring(0, factura.IndexOf("|"));
            string workingInfo = factura.Substring(factura.IndexOf("|") + 1);
            CreateExcelData(info);
            CreateGrafica1();
            CreateQRImage(InfoForQR, Etc);
            string Status = "";

            //declaracion y extraccion de datos
            string catchPhrase = workingInfo.Substring(0, workingInfo.IndexOf("|")); //frase que se debe dividir en 2 renglones
            string temporalsincatch = workingInfo.Substring(workingInfo.IndexOf("|") + 1);
            string InfoTotalAPagar = temporalsincatch.Substring(0, temporalsincatch.IndexOf("|")); // total a pagar
            string temporalsintotalbill = temporalsincatch.Substring(temporalsincatch.IndexOf("|") + 1);
            string InfoPeriodoConsumo = temporalsintotalbill.Substring(0, temporalsintotalbill.IndexOf("|")); //periodo de consumo plaintext
            string temporalsinPeriodoConsumo = temporalsintotalbill.Substring(temporalsintotalbill.IndexOf("|") + 1);
            string InfoFechaLimiteBill = temporalsinPeriodoConsumo.Substring(0, temporalsinPeriodoConsumo.IndexOf("|"));//FechaLimite de pago
            string temporalSinfechalimite = temporalsinPeriodoConsumo.Substring(temporalsinPeriodoConsumo.IndexOf("|") + 1);
            string InfoNumeroDeDocumento = temporalSinfechalimite.Substring(0, temporalSinfechalimite.IndexOf("|"));//numero de documento
            string temporalsinnumerodocumento = temporalSinfechalimite.Substring(temporalSinfechalimite.IndexOf("|") + 1);
            string InfoRMU = temporalsinnumerodocumento.Substring(0, temporalsinnumerodocumento.IndexOf("|"));//DATORMU
            string InfoClientesPordividir = temporalsinnumerodocumento.Substring(temporalsinnumerodocumento.IndexOf("|") + 1);//DATOS A DIVIDIR CLIENTE
            //
            //DIVIDIR INFORMACION DEL CLIENTE DE FACTURA
            String DivisorFiscal = "%";
            String[] FacturaCliente = System.Text.RegularExpressions.Regex.Split(InfoClientesPordividir, DivisorFiscal);


            //DIVIDIR CATCHPHRASE
            String separador = " ";
            String[] fraseSuper = System.Text.RegularExpressions.Regex.Split(catchPhrase, separador);
            int contador = 0;
            foreach (var element in fraseSuper)
            {
                contador++;

            }
            String Textosuperior = "", TextoInferior = "";
            if (contador > 1)
            {
                int totalinicio = contador / 2;
                totalinicio = totalinicio + 2;

                for (int m = 0; m < totalinicio; m++)
                {
                    Textosuperior = Textosuperior + fraseSuper[m] + " ";
                }
                for (int r = totalinicio; r < contador; r++)
                {
                    TextoInferior = TextoInferior + " " + fraseSuper[r];
                }
            }
            else
            {
                Textosuperior = " ";
                TextoInferior = catchPhrase + " ";
            }

            //DATOS RESUMEN PARA MEDIDOR Y PARA CONCEPTOS
            string MEDIDOR = DataResumen.Substring(0, DataResumen.IndexOf("|"));
            string temporalconsumo = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
            string TablaConsumo = temporalconsumo.Substring(0, temporalconsumo.IndexOf("|"));
            string temporalConceptos = temporalconsumo.Substring(temporalconsumo.IndexOf("|") + 1);
            string ConceptosMexicanos = temporalConceptos.Substring(0, temporalConceptos.IndexOf("|"));
            string ConceptosUSD = temporalConceptos.Substring(temporalConceptos.IndexOf("|") + 1);

            //Ciclos para datos con coma
            String elseparador = "-";
            String elmejorseparador = "/";
            String[] DatosParaTablaConsumo = System.Text.RegularExpressions.Regex.Split(TablaConsumo, elmejorseparador);
            String[] DatosPesosMexicanos = System.Text.RegularExpressions.Regex.Split(ConceptosMexicanos, elmejorseparador);
            String[] DatosDonalds = System.Text.RegularExpressions.Regex.Split(ConceptosUSD, elmejorseparador);



            try
            {
                Document doc = new Document(PageSize.LETTER);
                string Path = @"c:\PruebasPDF\PDF_ABASTO_AISLADO_CARGAS.pdf";
                //Ruta
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Path, FileMode.Create));

                //metadatos archivo
                doc.AddTitle("PDF-Amper USD Cargas");
                doc.AddCreator("Prime Consultoria");
                doc.SetMargins(5, 5, 5, 5);
                //Abrir Archivo
                doc.Open();
                //Definir Styles
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardMedidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _Medidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _secundaryfont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _headers = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
                iTextSharp.text.Font _StandardBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _TOTALBOLD = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _SecundaryBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font _resumee = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5, iTextSharp.text.Font.NORMAL, BaseColor.RED);
                iTextSharp.text.Font _resumeeheader = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.RED);

                // Escribimos el encabezamiento en el documento

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 95;

                //header.= BaseColor.RED;


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\LOGOAMPER.png");
                gif.SetAbsolutePosition(20, 750);
                gif.ScaleToFit(100f, 200F);

                iTextSharp.text.Image CFID = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\test.png");
                CFID.SetAbsolutePosition(135, 100);
                CFID.ScaleToFit(200f, 150F);
                doc.Add(CFID);


                //Declarar columnas
                PdfPCell conusoenergia = new PdfPCell(new Phrase(Textosuperior, _StandardBold));
                conusoenergia.BorderWidth = 0;
                conusoenergia.HorizontalAlignment = 2;
                //conusoenergia.BorderWidthBottom = 0.75f;
                PdfPCell metasenmexico = new PdfPCell(new Phrase(TextoInferior, _StandardBold));
                metasenmexico.BorderWidth = 0;
                metasenmexico.HorizontalAlignment = 2;
                //espacio en blanco
                PdfPCell espacio = new PdfPCell(new Phrase(" ", _StandardBold));
                espacio.BorderWidth = 0;
                espacio.HorizontalAlignment = 2;
                //linea Roja
                PdfPCell linearoja = new PdfPCell(new Phrase(" ", _StandardBold));
                linearoja.BorderWidth = 0;
                linearoja.HorizontalAlignment = 2;
                linearoja.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                PdfPCell lineaBlanca = new PdfPCell(new Phrase(" ", _StandardBold));
                lineaBlanca.BorderWidth = 0;
                lineaBlanca.HorizontalAlignment = 2;
                lineaBlanca.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);


                //Agregar Columnas header
                header.AddCell(espacio);
                header.AddCell(conusoenergia);
                header.AddCell(metasenmexico);
                header.AddCell(linearoja);

                //agregar datos 
                doc.Add(gif);

                //comparacion condicional segundo logotipo

                if (logotipo == "1")
                {
                    iTextSharp.text.Image secondhead = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\logocabezados.png");
                    secondhead.SetAbsolutePosition(140, 750);
                    secondhead.ScaleToFit(50f, 100F);
                    doc.Add(secondhead);
                }

                doc.Add(header);
                //GRAFICA Y CODIGO QR

                iTextSharp.text.Image grafica = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\grafico.png");
                grafica.SetAbsolutePosition(15, 270);
                grafica.ScaleToFit(320f, 200F);
                doc.Add(grafica);


                iTextSharp.text.Image qr = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\qr.png");
                qr.SetAbsolutePosition(15, 150);
                qr.ScaleToFit(175f, 110F);
                doc.Add(qr);



                //Pie de pagina

                PdfPTable tab = new PdfPTable(1);
                PdfPCell cell = new PdfPCell(new Phrase("Este documento es una representación impresa de un CFDI", _secundaryfont));
                cell.Border = 0;
                tab.TotalWidth = 300F;
                tab.AddCell(cell);
                tab.WriteSelectedRows(0, -1, 200, 40, writer.DirectContent);
                doc.Add(Chunk.NEWLINE);

                //TERMINO HEADER
                //Empiezo Datos Cliente
                PdfPTable DatosCliente = new PdfPTable(4);
                DatosCliente.WidthPercentage = 95;
                //cells Datos Cliente
                //CellSaltoDeLinea
                PdfPCell Salto = new PdfPCell(new Phrase(" ", _headers));
                Salto.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                Salto.BorderWidth = 0;
                Salto.HorizontalAlignment = 1;
                Salto.Colspan = 3;
                //Cellcoral


                //go
                PdfPCell Dependencia = new PdfPCell(new Phrase("AMMPER Energia, S.A.P.I. de C.V. ", _standardFont));
                Dependencia.Colspan = 3;
                Dependencia.BorderWidth = 0;
                Dependencia.HorizontalAlignment = 0;

                PdfPCell TotalPago = new PdfPCell(new Phrase(" Total a pagar ", _headers));
                TotalPago.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                TotalPago.BorderWidth = 0;
                TotalPago.HorizontalAlignment = 1;

                PdfPCell periodofact = new PdfPCell(new Phrase(" del periodo facturado ", _headers));
                periodofact.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodofact.BorderWidth = 0;
                periodofact.HorizontalAlignment = 1;

                PdfPCell calle = new PdfPCell(new Phrase("Av. Paseo de la Reforma ", _standardFont));
                calle.Colspan = 3;
                calle.BorderWidth = 0;
                calle.HorizontalAlignment = 0;

                PdfPCell coral = new PdfPCell(new Phrase(" ", _headers));
                coral.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                coral.BorderWidth = 0;
                coral.HorizontalAlignment = 1;



                PdfPCell numerodireccion = new PdfPCell(new Phrase("243 Piso 19", _standardFont));
                numerodireccion.Colspan = 3;
                numerodireccion.BorderWidth = 0;
                numerodireccion.HorizontalAlignment = 0;

                PdfPCell numerospago = new PdfPCell(new Phrase("$" + InfoTotalAPagar, _TOTALBOLD));
                numerospago.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numerospago.BorderWidth = 0;
                numerospago.HorizontalAlignment = 1;

                PdfPCell codigopostal = new PdfPCell(new Phrase("Cuauhtémoc, CP.06500", _standardFont));
                codigopostal.Colspan = 3;
                codigopostal.BorderWidth = 0;
                codigopostal.HorizontalAlignment = 0;

                PdfPCell saltopagar = new PdfPCell(new Phrase(" ", _StandardBold));
                saltopagar.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                saltopagar.BorderWidth = 0;
                saltopagar.HorizontalAlignment = 1;

                PdfPCell ciudadfactura = new PdfPCell(new Phrase("Ciudad de México, México", _standardFont));
                ciudadfactura.Colspan = 3;
                ciudadfactura.BorderWidth = 0;
                ciudadfactura.HorizontalAlignment = 0;

                PdfPCell periodoconsumo = new PdfPCell(new Phrase("Periodo del Consumo ", _headers));
                periodoconsumo.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodoconsumo.BorderWidth = 0;
                periodoconsumo.HorizontalAlignment = 1;

                PdfPCell rfcamper = new PdfPCell(new Phrase("R. F. C. AEN160405E56", _secundaryfont));
                rfcamper.Colspan = 3;
                rfcamper.BorderWidth = 0;
                rfcamper.HorizontalAlignment = 0;

                PdfPCell fechasperiodo = new PdfPCell(new Phrase(InfoPeriodoConsumo, _StandardBold));
                fechasperiodo.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                fechasperiodo.BorderWidth = 0;
                fechasperiodo.HorizontalAlignment = 1;

                PdfPCell direccioncliente = new PdfPCell(new Phrase("Nombre y domicilio", _SecundaryBold));
                direccioncliente.Colspan = 3;
                direccioncliente.BorderWidth = 2;
                direccioncliente.HorizontalAlignment = 0;


                direccioncliente.BorderColor = new BaseColor(255, 0, 0);

                direccioncliente.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                //Datos cliente de Amper

                PdfPCell nombreydomicilio = new PdfPCell(new Phrase(FacturaCliente[0], _standardFont));
                nombreydomicilio.Colspan = 2;
                nombreydomicilio.BorderWidth = 0;
                nombreydomicilio.HorizontalAlignment = 0;

                PdfPCell direccionreal = new PdfPCell(new Phrase(FacturaCliente[1], _standardFont));
                direccionreal.Colspan = 2;
                direccionreal.BorderWidth = 0;
                direccionreal.HorizontalAlignment = 0;

                PdfPCell NumeroDoc = new PdfPCell(new Phrase("Número de documento", _SecundaryBold));
                NumeroDoc.BorderWidth = 2;
                NumeroDoc.HorizontalAlignment = 0;
                NumeroDoc.BorderColor = new BaseColor(255, 0, 0);
                NumeroDoc.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell taimlimit = new PdfPCell(new Phrase(" Fecha límite de pago", _headers));
                taimlimit.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                taimlimit.BorderWidth = 0;
                taimlimit.HorizontalAlignment = 1;

                PdfPCell espacioderelleno = new PdfPCell(new Phrase(" ", _standardFont));
                espacioderelleno.Colspan = 2;
                espacioderelleno.BorderWidth = 0;
                espacioderelleno.HorizontalAlignment = 0;

                PdfPCell elverdaderodoc = new PdfPCell(new Phrase(InfoNumeroDeDocumento, _standardFont));

                elverdaderodoc.BorderWidth = 0;
                elverdaderodoc.HorizontalAlignment = 0;

                PdfPCell lafechalimite = new PdfPCell(new Phrase(InfoFechaLimiteBill, _StandardBold));
                lafechalimite.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                lafechalimite.BorderWidth = 0;
                lafechalimite.HorizontalAlignment = 1;

                PdfPCell RMUtitulo = new PdfPCell(new Phrase("RMU", _SecundaryBold));
                RMUtitulo.BorderWidth = 2;
                RMUtitulo.HorizontalAlignment = 0;
                RMUtitulo.BorderColor = new BaseColor(255, 0, 0);
                RMUtitulo.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell telefonoemergencia = new PdfPCell(new Phrase(" Teléfono de emergencia", _headers));
                telefonoemergencia.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                telefonoemergencia.BorderWidth = 0;
                telefonoemergencia.HorizontalAlignment = 1;

                PdfPCell NUMRMU = new PdfPCell(new Phrase(InfoRMU, _standardFont));

                NUMRMU.BorderWidth = 0;
                NUMRMU.HorizontalAlignment = 0;

                PdfPCell numero8cientos = new PdfPCell(new Phrase("800 9532 911", _StandardBold));
                numero8cientos.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numero8cientos.BorderWidth = 0;
                numero8cientos.HorizontalAlignment = 1;

                PdfPCell MedicionConsum = new PdfPCell(new Phrase("", _SecundaryBold));
                MedicionConsum.BorderWidth = 0;
                MedicionConsum.HorizontalAlignment = 0;

                PdfPCell cientos8amper = new PdfPCell(new Phrase("", _StandardBold));
                cientos8amper.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                cientos8amper.BorderWidth = 0;
                cientos8amper.HorizontalAlignment = 1;




                //Agregar Celdas a Datoscliente
                DatosCliente.AddCell(Dependencia);
                DatosCliente.AddCell(TotalPago);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(periodofact);
                DatosCliente.AddCell(calle);
                DatosCliente.AddCell(coral);
                DatosCliente.AddCell(numerodireccion);
                DatosCliente.AddCell(numerospago);
                DatosCliente.AddCell(codigopostal);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(ciudadfactura);
                DatosCliente.AddCell(periodoconsumo);
                DatosCliente.AddCell(rfcamper);
                DatosCliente.AddCell(fechasperiodo);
                DatosCliente.AddCell(direccioncliente);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(nombreydomicilio);
                DatosCliente.AddCell(NumeroDoc);
                DatosCliente.AddCell(taimlimit);
                DatosCliente.AddCell(direccionreal);
                DatosCliente.AddCell(elverdaderodoc);
                DatosCliente.AddCell(lafechalimite);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(RMUtitulo);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(telefonoemergencia);
                DatosCliente.AddCell(NUMRMU);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(numero8cientos);
                DatosCliente.AddCell(MedicionConsum);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(cientos8amper);



                //agregar datos 
                doc.Add(DatosCliente);

                //Tabla de medicion de consumo
                PdfPTable DatosdeConsumo = new PdfPTable(8);
                DatosdeConsumo.WidthPercentage = 95;

                //Declaracion de celdas
                //encabezados
                PdfPCell Nummedidor = new PdfPCell(new Phrase(" No. Medidor ", _SecundaryBold));
                Nummedidor.BorderWidth = 2;
                Nummedidor.HorizontalAlignment = 0;
                Nummedidor.BorderColor = new BaseColor(255, 0, 0);
                Nummedidor.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell Tituloconsumomensual = new PdfPCell(new Phrase(" Consumo Mensual (MWh) ", _SecundaryBold));
                Tituloconsumomensual.BorderWidth = 2;
                Tituloconsumomensual.HorizontalAlignment = Element.ALIGN_CENTER;
                Tituloconsumomensual.BorderColor = new BaseColor(255, 0, 0);
                Tituloconsumomensual.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Tituloconsumomensual.Colspan = 3;

                PdfPCell TarifaSumi = new PdfPCell(new Phrase(" Tarifa de Suministro ($/MWh) ", _SecundaryBold));
                TarifaSumi.BorderWidth = 2;
                TarifaSumi.HorizontalAlignment = Element.ALIGN_CENTER;
                TarifaSumi.BorderColor = new BaseColor(255, 0, 0);
                TarifaSumi.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                TarifaSumi.Colspan = 2;

                PdfPCell Superpotencia100 = new PdfPCell(new Phrase("Potencia 100 horas críticas (MW)", _SecundaryBold));
                Superpotencia100.BorderWidth = 2;
                Superpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                Superpotencia100.BorderColor = new BaseColor(255, 0, 0);
                Superpotencia100.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Superpotencia100.Colspan = 2;

                //Contenido

                //Dejar espacios blancos en medidor

                PdfPTable DatosdeConsumo2 = new PdfPTable(10);
                DatosdeConsumo2.WidthPercentage = 95;

                PdfPCell espaciomedidor = new PdfPCell(new Phrase(" ", _standardFont));
                espaciomedidor.BorderWidth = 0;
                espaciomedidor.HorizontalAlignment = 0;

                //dejar espacios en blanco en tarifa de suministros

                PdfPCell espaciotarifasumi = new PdfPCell(new Phrase(" ", _standardFont));
                espaciotarifasumi.BorderWidth = 0;
                espaciotarifasumi.HorizontalAlignment = 0;
                espaciotarifasumi.Colspan = 2;

                //dejar espacios en blanco en potencias 100!

                PdfPCell espaciopotencia100 = new PdfPCell(new Phrase(" ", _standardFont));
                espaciopotencia100.BorderWidth = 0;
                espaciopotencia100.HorizontalAlignment = 0;
                espaciopotencia100.Colspan = 2;

                //medidor
                PdfPCell cualmedidor = new PdfPCell(new Phrase(" " + MEDIDOR + " ", _Medidor));
                cualmedidor.BorderWidth = 0;
                cualmedidor.HorizontalAlignment = 0;
                cualmedidor.Colspan = 2;

                //consumoneto

                PdfPCell consumoneto = new PdfPCell(new Phrase(" CONSUMO DE ENERGÍA NETO ", _standardMedidor));
                consumoneto.BorderWidth = 0;
                consumoneto.HorizontalAlignment = Element.ALIGN_LEFT;
                consumoneto.Colspan = 2;

                PdfPCell cantidadconsumoneto = new PdfPCell(new Phrase(DatosParaTablaConsumo[0] + "", _standardMedidor));
                cantidadconsumoneto.BorderWidth = 0;
                cantidadconsumoneto.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Tarifa
                PdfPCell cantidadtarifa = new PdfPCell(new Phrase(DatosParaTablaConsumo[7], _standardMedidor));
                cantidadtarifa.BorderWidth = 0;
                cantidadtarifa.HorizontalAlignment = Element.ALIGN_LEFT;

                PdfPCell conceptotarifa = new PdfPCell(new Phrase("", _standardMedidor));
                conceptotarifa.BorderWidth = 0;
                conceptotarifa.HorizontalAlignment = 0;

                //potencia super cantidad!
                PdfPCell cantidadpotencia100 = new PdfPCell(new Phrase(DatosParaTablaConsumo[8], _standardMedidor));
                cantidadpotencia100.BorderWidth = 0;
                cantidadpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                cantidadpotencia100.Colspan = 2;

                //Consumo energia contratada 

                PdfPCell energiacontratada = new PdfPCell(new Phrase(" ENERGÍA CONTRATADA ", _standardMedidor));
                energiacontratada.BorderWidth = 0;
                energiacontratada.HorizontalAlignment = 0;
                energiacontratada.Colspan = 2;

                PdfPCell cantidadenergiacontratada = new PdfPCell(new Phrase(DatosParaTablaConsumo[1] + "", _standardMedidor));
                cantidadenergiacontratada.BorderWidth = 0;
                cantidadenergiacontratada.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia vendida 

                PdfPCell energiavendida = new PdfPCell(new Phrase(" ENERGÍA VENDIDA", _standardMedidor));
                energiavendida.BorderWidth = 0;
                energiavendida.HorizontalAlignment = 0;
                energiavendida.Colspan = 2;

                PdfPCell cantidadenergiaVENDIDA = new PdfPCell(new Phrase(DatosParaTablaConsumo[2] + "", _standardMedidor));
                cantidadenergiaVENDIDA.BorderWidth = 0;
                cantidadenergiaVENDIDA.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia TBFIN 

                PdfPCell TBFIN = new PdfPCell(new Phrase(" AJUSTE TBFIN ", _standardMedidor));
                TBFIN.BorderWidth = 0;
                TBFIN.HorizontalAlignment = 0;
                TBFIN.Colspan = 2;

                PdfPCell cantidadTBFIN = new PdfPCell(new Phrase(DatosParaTablaConsumo[3] + "", _standardMedidor));
                cantidadTBFIN.BorderWidth = 0;
                cantidadTBFIN.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia PERDIDAS NO TECNICAS

                PdfPCell PERDIDASNOTECNICAS = new PdfPCell(new Phrase(" PERDIDAS NO TECNICAS ", _standardMedidor));
                PERDIDASNOTECNICAS.BorderWidth = 0;
                PERDIDASNOTECNICAS.HorizontalAlignment = 0;
                PERDIDASNOTECNICAS.Colspan = 2;

                PdfPCell cantidadePERDIDASNOTECNICAS = new PdfPCell(new Phrase(DatosParaTablaConsumo[4] + "", _standardMedidor));
                cantidadePERDIDASNOTECNICAS.BorderWidth = 0;
                cantidadePERDIDASNOTECNICAS.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo energia SUPERMEM

                PdfPCell SUPERMEM = new PdfPCell(new Phrase(" CONSUMO DE ENERGÍA MEM ", _standardMedidor));
                SUPERMEM.BorderWidth = 0;
                SUPERMEM.HorizontalAlignment = 0;
                SUPERMEM.Colspan = 2;

                PdfPCell CANDITADSUPERMEM = new PdfPCell(new Phrase(DatosParaTablaConsumo[5] + "", _standardMedidor));
                CANDITADSUPERMEM.BorderWidth = 0;
                CANDITADSUPERMEM.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Consumo ABASTO AISLADO

                PdfPCell ABASTOAISLADO = new PdfPCell(new Phrase(" CONSUMO DE ABASTO AISLADO ", _standardMedidor));
                ABASTOAISLADO.BorderWidth = 0;
                ABASTOAISLADO.HorizontalAlignment = Element.ALIGN_RIGHT;
                ABASTOAISLADO.Colspan = 3;

                PdfPCell CANDITADABASTOAISLADO = new PdfPCell(new Phrase(DatosParaTablaConsumo[6] + "", _standardMedidor));
                CANDITADABASTOAISLADO.BorderWidth = 0;
                CANDITADABASTOAISLADO.HorizontalAlignment = Element.ALIGN_RIGHT;





                //Agregar celdas a datos consumo

                DatosdeConsumo.AddCell(Nummedidor);
                DatosdeConsumo.AddCell(Tituloconsumomensual);
                DatosdeConsumo.AddCell(TarifaSumi);
                DatosdeConsumo.AddCell(Superpotencia100);


                DatosdeConsumo2.AddCell(cualmedidor);
                DatosdeConsumo2.AddCell(consumoneto);
                DatosdeConsumo2.AddCell(cantidadconsumoneto);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(cantidadtarifa);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(cantidadpotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(energiacontratada);
                DatosdeConsumo2.AddCell(cantidadenergiacontratada);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(energiavendida);
                DatosdeConsumo2.AddCell(cantidadenergiaVENDIDA);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(TBFIN);
                DatosdeConsumo2.AddCell(cantidadTBFIN);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(PERDIDASNOTECNICAS);
                DatosdeConsumo2.AddCell(cantidadePERDIDASNOTECNICAS);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(SUPERMEM);
                DatosdeConsumo2.AddCell(CANDITADSUPERMEM);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);

                DatosdeConsumo2.AddCell(espaciomedidor);
                //DatosdeConsumo2.AddCell(espaciomedidor);
                DatosdeConsumo2.AddCell(ABASTOAISLADO);
                DatosdeConsumo2.AddCell(CANDITADABASTOAISLADO);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(espaciotarifasumi);
                DatosdeConsumo2.AddCell(espaciopotencia100);



                //AgregardatosConsumo
                doc.Add(DatosdeConsumo);
                doc.Add(DatosdeConsumo2);
                doc.Add(Chunk.NEWLINE);

                //tabla Desglosada de resumen
                PdfPTable Resumen = new PdfPTable(14);
                Resumen.WidthPercentage = 100;

                //Espacios Blancos de resumen

                PdfPCell blancosresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosresumen.BorderWidth = 0;
                blancosresumen.HorizontalAlignment = 0;
                blancosresumen.Colspan = 8;

                PdfPCell blancosfinalresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosfinalresumen.BorderWidth = 0;
                blancosfinalresumen.HorizontalAlignment = 0;

                //Crear datos encabezado resumee

                PdfPCell concepto = new PdfPCell(new Phrase("Concepto ", _resumeeheader));
                concepto.BorderWidth = 1;
                concepto.HorizontalAlignment = 0;
                concepto.BorderColor = new BaseColor(255, 0, 0);
                concepto.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                concepto.Colspan = 3;

                PdfPCell tarifa = new PdfPCell(new Phrase(tipoCambio + "/MWh ", _resumeeheader));
                tarifa.BorderWidth = 1;
                tarifa.HorizontalAlignment = 1;
                tarifa.BorderColor = new BaseColor(255, 0, 0);
                tarifa.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;


                PdfPCell totaldolarucos = new PdfPCell(new Phrase(" Total  " + tipoCambio, _resumeeheader));
                totaldolarucos.BorderWidth = 1;
                totaldolarucos.HorizontalAlignment = 1;
                totaldolarucos.BorderColor = new BaseColor(255, 0, 0);
                totaldolarucos.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                //Comparacion de datos resumee Pesos
                for (int a = 0; a < 26; a++)
                {
                    string pesostemporales = DatosPesosMexicanos[a];
                    if (DatosPesosMexicanos[a] != "")
                    {
                        if (DatosPesosMexicanos[a].Substring(0, 1) == "-")
                        {
                            DatosPesosMexicanos[a] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            DatosPesosMexicanos[a] = " $" + pesostemporales;

                        }
                    }
                }
                //Comparacion de datos Resume Dolarucos
                for (int a = 0; a < 30; a++)
                {
                    string pesostemporales = DatosDonalds[a];
                    if (DatosDonalds[a] != "")
                    {
                        if (DatosDonalds[a].Substring(0, 1) == "-")
                        {
                            DatosDonalds[a] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            DatosDonalds[a] = " $" + pesostemporales;

                        }
                    }
                }

                //Creardatos resumee

                PdfPCell ENERGIAABASTOAISLADO = new PdfPCell(new Phrase("ENERGÍA ABASTO AISLADO ", _resumee));
                ENERGIAABASTOAISLADO.BorderWidth = 0;
                ENERGIAABASTOAISLADO.HorizontalAlignment = 0;
                ENERGIAABASTOAISLADO.Colspan = 3;

                PdfPCell EAAT = new PdfPCell(new Phrase(DatosPesosMexicanos[0], _resumee));
                EAAT.BorderWidth = 0;
                EAAT.HorizontalAlignment = 2;

                PdfPCell EAAUSD = new PdfPCell(new Phrase(DatosDonalds[0], _resumee));
                EAAUSD.BorderWidth = 0;
                EAAUSD.HorizontalAlignment = 2;

                //ENERGIA ABASTO AISLADO

                PdfPCell ENERGIACONTRATADA = new PdfPCell(new Phrase("ENERGÍA CONTRATADA ", _resumee));
                ENERGIACONTRATADA.BorderWidth = 0;
                ENERGIACONTRATADA.HorizontalAlignment = 0;
                ENERGIACONTRATADA.Colspan = 3;

                PdfPCell ECT = new PdfPCell(new Phrase(DatosPesosMexicanos[1], _resumee));
                ECT.BorderWidth = 0;
                ECT.HorizontalAlignment = 2;

                PdfPCell ECUSD = new PdfPCell(new Phrase(DatosDonalds[1], _resumee));
                ECUSD.BorderWidth = 0;
                ECUSD.HorizontalAlignment = 2;
                //ENERGIA CONTRATADA

                PdfPCell ENERGIAMEM = new PdfPCell(new Phrase("ENERGÍA MEM ", _resumee));
                ENERGIAMEM.BorderWidth = 0;
                ENERGIAMEM.HorizontalAlignment = 0;
                ENERGIAMEM.Colspan = 3;

                PdfPCell EMT = new PdfPCell(new Phrase(DatosPesosMexicanos[2], _resumee));
                EMT.BorderWidth = 0;
                EMT.HorizontalAlignment = 2;

                PdfPCell EMUSD = new PdfPCell(new Phrase(DatosDonalds[2], _resumee));
                EMUSD.BorderWidth = 0;
                EMUSD.HorizontalAlignment = 2;
                //ENERGIA MEM

                PdfPCell ENERGIAVENDIDA = new PdfPCell(new Phrase("ENERGÍA VENDIDA", _resumee));
                ENERGIAVENDIDA.BorderWidth = 0;
                ENERGIAVENDIDA.HorizontalAlignment = 0;
                ENERGIAVENDIDA.Colspan = 3;

                PdfPCell EVT = new PdfPCell(new Phrase(DatosPesosMexicanos[3], _resumee));
                EVT.BorderWidth = 0;
                EVT.HorizontalAlignment = 2;

                PdfPCell EVUSD = new PdfPCell(new Phrase(DatosDonalds[3], _resumee));
                EVUSD.BorderWidth = 0;
                EVUSD.HorizontalAlignment = 2;
                //ENERGIA VENDIDA

                PdfPCell COSTOSOPERATIVOS = new PdfPCell(new Phrase("COSTOS OPERATIVOS", _resumee));
                COSTOSOPERATIVOS.BorderWidth = 0;
                COSTOSOPERATIVOS.HorizontalAlignment = 0;
                COSTOSOPERATIVOS.Colspan = 3;

                PdfPCell COT = new PdfPCell(new Phrase(DatosPesosMexicanos[4], _resumee));
                COT.BorderWidth = 0;
                COT.HorizontalAlignment = 2;

                PdfPCell COUSD = new PdfPCell(new Phrase(DatosDonalds[4], _resumee));
                COUSD.BorderWidth = 0;
                COUSD.HorizontalAlignment = 2;
                //COSTOS OPERATIVOS

                PdfPCell AJUSTE = new PdfPCell(new Phrase("AJUSTE", _resumee));
                AJUSTE.BorderWidth = 0;
                AJUSTE.HorizontalAlignment = 0;
                AJUSTE.Colspan = 3;

                PdfPCell AJT = new PdfPCell(new Phrase(DatosPesosMexicanos[5], _resumee));
                AJT.BorderWidth = 0;
                AJT.HorizontalAlignment = 2;

                PdfPCell AJUSD = new PdfPCell(new Phrase(DatosDonalds[5], _resumee));
                AJUSD.BorderWidth = 0;
                AJUSD.HorizontalAlignment = 2;
                //AJUSTE

                PdfPCell DESVIACION = new PdfPCell(new Phrase("DESVIACIÓN", _resumee));
                DESVIACION.BorderWidth = 0;
                DESVIACION.HorizontalAlignment = 0;
                DESVIACION.Colspan = 3;

                PdfPCell DET = new PdfPCell(new Phrase(DatosPesosMexicanos[6], _resumee));
                DET.BorderWidth = 0;
                DET.HorizontalAlignment = 2;

                PdfPCell DEUSD = new PdfPCell(new Phrase(DatosDonalds[6], _resumee));
                DEUSD.BorderWidth = 0;
                DEUSD.HorizontalAlignment = 2;
                //DESVIACION

                PdfPCell RPERDIDASNOTECNICAS = new PdfPCell(new Phrase("PÉRDIDAS NO TÉCNICAS", _resumee));
                RPERDIDASNOTECNICAS.BorderWidth = 0;
                RPERDIDASNOTECNICAS.HorizontalAlignment = 0;
                RPERDIDASNOTECNICAS.Colspan = 3;

                PdfPCell PNTT = new PdfPCell(new Phrase(DatosPesosMexicanos[7], _resumee));
                PNTT.BorderWidth = 0;
                PNTT.HorizontalAlignment = 2;

                PdfPCell PNTUSD = new PdfPCell(new Phrase(DatosDonalds[7], _resumee));
                PNTUSD.BorderWidth = 0;
                PNTUSD.HorizontalAlignment = 2;
                //PERDIDAS NO TECNICAS

                PdfPCell PERDIDASDELARED = new PdfPCell(new Phrase("PÉRDIDAS DE LA RED", _resumee));
                PERDIDASDELARED.BorderWidth = 0;
                PERDIDASDELARED.HorizontalAlignment = 0;
                PERDIDASDELARED.Colspan = 3;

                PdfPCell PDLRT = new PdfPCell(new Phrase(DatosPesosMexicanos[8], _resumee));
                PDLRT.BorderWidth = 0;
                PDLRT.HorizontalAlignment = 2;

                PdfPCell PDLRUSD = new PdfPCell(new Phrase(DatosDonalds[8], _resumee));
                PDLRUSD.BorderWidth = 0;
                PDLRUSD.HorizontalAlignment = 2;
                //PERDIDAS DE LA RED

                PdfPCell SERVICIOSCONEXOS = new PdfPCell(new Phrase("SERVICIOS CONEXOS", _resumee));
                SERVICIOSCONEXOS.BorderWidth = 0;
                SERVICIOSCONEXOS.HorizontalAlignment = 0;
                SERVICIOSCONEXOS.Colspan = 3;

                PdfPCell SCT = new PdfPCell(new Phrase(DatosPesosMexicanos[9], _resumee));
                SCT.BorderWidth = 0;
                SCT.HorizontalAlignment = 2;

                PdfPCell SCUSD = new PdfPCell(new Phrase(DatosDonalds[9], _resumee));
                SCUSD.BorderWidth = 0;
                SCUSD.HorizontalAlignment = 2;
                //SERVICIOS CONEXOS

                PdfPCell COSTOOPERACIONCENACE = new PdfPCell(new Phrase("COSTO DE OPERACIÓN DE CENACE CARGA", _resumee));
                COSTOOPERACIONCENACE.BorderWidth = 0;
                COSTOOPERACIONCENACE.HorizontalAlignment = 0;
                COSTOOPERACIONCENACE.Colspan = 3;

                PdfPCell COCCT = new PdfPCell(new Phrase(DatosPesosMexicanos[10], _resumee));
                COCCT.BorderWidth = 0;
                COCCT.HorizontalAlignment = 2;

                PdfPCell COCCUSD = new PdfPCell(new Phrase(DatosDonalds[10], _resumee));
                COCCUSD.BorderWidth = 0;
                COCCUSD.HorizontalAlignment = 2;
                //COSTO DE OPERACION DEL CENACE CARGA

                PdfPCell OPERACIONCENACEGENERADOR = new PdfPCell(new Phrase("COSTO DE OPERACIÓN DE CENACE GENERADOR", _resumee));
                OPERACIONCENACEGENERADOR.BorderWidth = 0;
                OPERACIONCENACEGENERADOR.HorizontalAlignment = 0;
                OPERACIONCENACEGENERADOR.Colspan = 3;

                PdfPCell COCGT = new PdfPCell(new Phrase(DatosPesosMexicanos[11], _resumee));
                COCGT.BorderWidth = 0;
                COCGT.HorizontalAlignment = 2;

                PdfPCell COCGUSD = new PdfPCell(new Phrase(DatosDonalds[11], _resumee));
                COCGUSD.BorderWidth = 0;
                COCGUSD.HorizontalAlignment = 2;
                //COSTO DE OPERACION DEL CENACE GENERADOR

                PdfPCell GARANTIASUFICIENCIA = new PdfPCell(new Phrase("GARANTÍA DE SUFICIENCIA", _resumee));
                GARANTIASUFICIENCIA.BorderWidth = 0;
                GARANTIASUFICIENCIA.HorizontalAlignment = 0;
                GARANTIASUFICIENCIA.Colspan = 3;

                PdfPCell GDST = new PdfPCell(new Phrase(DatosPesosMexicanos[12], _resumee));
                GDST.BorderWidth = 0;
                GDST.HorizontalAlignment = 2;

                PdfPCell GDSUSD = new PdfPCell(new Phrase(DatosDonalds[12], _resumee));
                GDSUSD.BorderWidth = 0;
                GDSUSD.HorizontalAlignment = 2;
                //GARANTIA DE SUFICIENCIA

                PdfPCell GARANTIANTECENACE = new PdfPCell(new Phrase("GARANTÍA REGULATORIA ANTE EL CENACE", _resumee));
                GARANTIANTECENACE.BorderWidth = 0;
                GARANTIANTECENACE.HorizontalAlignment = 0;
                GARANTIANTECENACE.Colspan = 3;

                PdfPCell GRACT = new PdfPCell(new Phrase(DatosPesosMexicanos[13], _resumee));
                GRACT.BorderWidth = 0;
                GRACT.HorizontalAlignment = 2;

                PdfPCell GRACUSD = new PdfPCell(new Phrase(DatosDonalds[13], _resumee));
                GRACUSD.BorderWidth = 0;
                GRACUSD.HorizontalAlignment = 2;
                //GARANTIA REGULATORIA ANTE CENACE

                PdfPCell TRANSMISIÓNCARGA = new PdfPCell(new Phrase("TRANSMISIÓN CARGA", _resumee));
                TRANSMISIÓNCARGA.BorderWidth = 0;
                TRANSMISIÓNCARGA.HorizontalAlignment = 0;
                TRANSMISIÓNCARGA.Colspan = 3;

                PdfPCell TCT = new PdfPCell(new Phrase(DatosPesosMexicanos[14], _resumee));
                TCT.BorderWidth = 0;
                TCT.HorizontalAlignment = 2;

                PdfPCell TCUSD = new PdfPCell(new Phrase(DatosDonalds[14], _resumee));
                TCUSD.BorderWidth = 0;
                TCUSD.HorizontalAlignment = 2;
                //TRANSMISIÓN CARGA

                PdfPCell TRANSMISIÓNGENERADOR = new PdfPCell(new Phrase("TRANSMISIÓN GENERADOR", _resumee));
                TRANSMISIÓNGENERADOR.BorderWidth = 0;
                TRANSMISIÓNGENERADOR.HorizontalAlignment = 0;
                TRANSMISIÓNGENERADOR.Colspan = 3;

                PdfPCell TGT = new PdfPCell(new Phrase(DatosPesosMexicanos[15], _resumee));
                TGT.BorderWidth = 0;
                TGT.HorizontalAlignment = 2;

                PdfPCell TGUSD = new PdfPCell(new Phrase(DatosDonalds[15], _resumee));
                TGUSD.BorderWidth = 0;
                TGUSD.HorizontalAlignment = 2;
                //TRANSMISIÓN GENERADOR

                PdfPCell POTENCIA = new PdfPCell(new Phrase("POTENCIA", _resumee));
                POTENCIA.BorderWidth = 0;
                POTENCIA.HorizontalAlignment = 0;
                POTENCIA.Colspan = 3;

                PdfPCell PT = new PdfPCell(new Phrase(DatosPesosMexicanos[16], _resumee));
                PT.BorderWidth = 0;
                PT.HorizontalAlignment = 2;

                PdfPCell PUSD = new PdfPCell(new Phrase(DatosDonalds[16], _resumee));
                PUSD.BorderWidth = 0;
                PUSD.HorizontalAlignment = 2;
                //POTENCIA

                PdfPCell POTENCIABASTOAISLADO = new PdfPCell(new Phrase("POTENCIA ABASTO AISLADO", _resumee));
                POTENCIABASTOAISLADO.BorderWidth = 0;
                POTENCIABASTOAISLADO.HorizontalAlignment = 0;
                POTENCIABASTOAISLADO.Colspan = 3;

                PdfPCell PAAT = new PdfPCell(new Phrase(DatosPesosMexicanos[17], _resumee));
                PAAT.BorderWidth = 0;
                PAAT.HorizontalAlignment = 2;

                PdfPCell PAAUSD = new PdfPCell(new Phrase(DatosDonalds[17], _resumee));
                PAAUSD.BorderWidth = 0;
                PAAUSD.HorizontalAlignment = 2;
                //POTENCIA ABASTO AISLADO

                PdfPCell SERVICIODISTRIBUCION = new PdfPCell(new Phrase("SERVICIO DE DISTRIBUCIÓN", _resumee));
                SERVICIODISTRIBUCION.BorderWidth = 0;
                SERVICIODISTRIBUCION.HorizontalAlignment = 0;
                SERVICIODISTRIBUCION.Colspan = 3;

                PdfPCell SDT = new PdfPCell(new Phrase(DatosPesosMexicanos[18], _resumee));
                SDT.BorderWidth = 0;
                SDT.HorizontalAlignment = 2;

                PdfPCell SDUSD = new PdfPCell(new Phrase(DatosDonalds[18], _resumee));
                SDUSD.BorderWidth = 0;
                SDUSD.HorizontalAlignment = 2;
                //SERVICIOS DE DISTRIBUCION

                PdfPCell CONGESTIONMEM = new PdfPCell(new Phrase("CONGESTIÓN MEM", _resumee));
                CONGESTIONMEM.BorderWidth = 0;
                CONGESTIONMEM.HorizontalAlignment = 0;
                CONGESTIONMEM.Colspan = 3;

                PdfPCell CMT = new PdfPCell(new Phrase(DatosPesosMexicanos[19], _resumee));
                CMT.BorderWidth = 0;
                CMT.HorizontalAlignment = 2;

                PdfPCell CMUSD = new PdfPCell(new Phrase(DatosDonalds[19], _resumee));
                CMUSD.BorderWidth = 0;
                CMUSD.HorizontalAlignment = 2;
                //CONGESTION MEM

                PdfPCell CONGESTIONGENERADOR = new PdfPCell(new Phrase("CONGESTIÓN GENERADOR", _resumee));
                CONGESTIONGENERADOR.BorderWidth = 0;
                CONGESTIONGENERADOR.HorizontalAlignment = 0;
                CONGESTIONGENERADOR.Colspan = 3;

                PdfPCell CGT = new PdfPCell(new Phrase(DatosPesosMexicanos[20], _resumee));
                CGT.BorderWidth = 0;
                CGT.HorizontalAlignment = 2;

                PdfPCell CGUSD = new PdfPCell(new Phrase(DatosDonalds[20], _resumee));
                CGUSD.BorderWidth = 0;
                CGUSD.HorizontalAlignment = 2;
                //CONGESTION GENERADOR

                PdfPCell CERTIFICADOSENERGIALIMPIA = new PdfPCell(new Phrase("CERTIFICADOS DE ENERGÍA LIMPIA", _resumee));
                CERTIFICADOSENERGIALIMPIA.BorderWidth = 0;
                CERTIFICADOSENERGIALIMPIA.HorizontalAlignment = 0;
                CERTIFICADOSENERGIALIMPIA.Colspan = 3;

                PdfPCell CELT = new PdfPCell(new Phrase(DatosPesosMexicanos[21], _resumee));
                CELT.BorderWidth = 0;
                CELT.HorizontalAlignment = 2;

                PdfPCell CELUSD = new PdfPCell(new Phrase(DatosDonalds[21], _resumee));
                CELUSD.BorderWidth = 0;
                CELUSD.HorizontalAlignment = 2;
                //CERTIFICADOS ENERGIA LIMPIA

                PdfPCell ADICIONALESCERTIFICADOSENERGIALIMPIA = new PdfPCell(new Phrase("CERTIFICADOS DE ENERGÍA LIMPIA ADICIONALES", _resumee));
                ADICIONALESCERTIFICADOSENERGIALIMPIA.BorderWidth = 0;
                ADICIONALESCERTIFICADOSENERGIALIMPIA.HorizontalAlignment = 0;
                ADICIONALESCERTIFICADOSENERGIALIMPIA.Colspan = 3;

                PdfPCell CELAT = new PdfPCell(new Phrase(DatosPesosMexicanos[22], _resumee));
                CELAT.BorderWidth = 0;
                CELAT.HorizontalAlignment = 2;

                PdfPCell CELAUSD = new PdfPCell(new Phrase(DatosDonalds[22], _resumee));
                CELAUSD.BorderWidth = 0;
                CELAUSD.HorizontalAlignment = 2;
                //CERTIFICADOS ENERGIA LIMPIA ADICIONALES

                PdfPCell RELIQUIDACIONES = new PdfPCell(new Phrase("RELIQUIDACIONES", _resumee));
                RELIQUIDACIONES.BorderWidth = 0;
                RELIQUIDACIONES.HorizontalAlignment = 0;
                RELIQUIDACIONES.Colspan = 3;

                PdfPCell RLT = new PdfPCell(new Phrase(DatosPesosMexicanos[23], _resumee));
                RLT.BorderWidth = 0;
                RLT.HorizontalAlignment = 2;

                PdfPCell RLUSD = new PdfPCell(new Phrase(DatosDonalds[23], _resumee));
                RLUSD.BorderWidth = 0;
                RLUSD.HorizontalAlignment = 2;
                //RELIQUIDACIONES

                PdfPCell OTROSCARGOSYABONOS = new PdfPCell(new Phrase("OTROS CARGOS Y/O ABONOS", _resumee));
                OTROSCARGOSYABONOS.BorderWidth = 0;
                OTROSCARGOSYABONOS.HorizontalAlignment = 0;
                OTROSCARGOSYABONOS.Colspan = 3;

                PdfPCell OCYAT = new PdfPCell(new Phrase(DatosPesosMexicanos[24], _resumee));
                OCYAT.BorderWidth = 0;
                OCYAT.HorizontalAlignment = 2;

                PdfPCell OCYAUSD = new PdfPCell(new Phrase(DatosDonalds[24], _resumee));
                OCYAUSD.BorderWidth = 0;
                OCYAUSD.HorizontalAlignment = 2;
                // OTROS CARGOS Y ABONOS

                PdfPCell AMMPER = new PdfPCell(new Phrase("AMMPER", _resumee));
                AMMPER.BorderWidth = 0;
                AMMPER.HorizontalAlignment = 0;
                AMMPER.Colspan = 3;

                PdfPCell AMMT = new PdfPCell(new Phrase(DatosPesosMexicanos[25], _resumee));
                AMMT.BorderWidth = 0;
                AMMT.HorizontalAlignment = 2;

                PdfPCell AMMUSD = new PdfPCell(new Phrase(DatosDonalds[25], _resumee));
                AMMUSD.BorderWidth = 0;
                AMMUSD.HorizontalAlignment = 2;
                // AMMPER

                PdfPCell SUBTOTALRESUMEE = new PdfPCell(new Phrase("SUBTOTAL", _resumee));
                SUBTOTALRESUMEE.BorderWidth = 0;
                SUBTOTALRESUMEE.HorizontalAlignment = 0;
                SUBTOTALRESUMEE.Colspan = 3;


                PdfPCell SUBTRUSD = new PdfPCell(new Phrase(DatosDonalds[26], _resumee));
                SUBTRUSD.BorderWidth = 0;
                SUBTRUSD.HorizontalAlignment = 2;
                // subtotal RESUMEE

                PdfPCell IVARESUMEE = new PdfPCell(new Phrase("IVA", _resumee));
                IVARESUMEE.BorderWidth = 0;
                IVARESUMEE.HorizontalAlignment = 0;
                IVARESUMEE.Colspan = 3;


                PdfPCell IVARUSD = new PdfPCell(new Phrase(DatosDonalds[27], _resumee));
                IVARUSD.BorderWidth = 0;
                IVARUSD.HorizontalAlignment = 2;
                // IVA

                PdfPCell TOTALRESUMEE = new PdfPCell(new Phrase("TOTAL", _resumee));
                TOTALRESUMEE.BorderWidth = 0;
                TOTALRESUMEE.HorizontalAlignment = 0;
                TOTALRESUMEE.Colspan = 3;


                PdfPCell TOTALRUSD = new PdfPCell(new Phrase(DatosDonalds[28], _resumee));
                TOTALRUSD.BorderWidth = 0;
                TOTALRUSD.HorizontalAlignment = 2;
                // TOTAL RESUMEE

                PdfPCell TIPOEXCHANGE = new PdfPCell(new Phrase("TIPO DE CAMBIO", _resumee));
                TIPOEXCHANGE.BorderWidth = 0;
                TIPOEXCHANGE.HorizontalAlignment = 0;
                TIPOEXCHANGE.Colspan = 3;


                PdfPCell TIPOEXCHRUSD = new PdfPCell(new Phrase(DatosDonalds[29], _resumee));
                TIPOEXCHRUSD.BorderWidth = 0;
                TIPOEXCHRUSD.HorizontalAlignment = 2;
                // TIPO DE CAMBIO

                //Agregar celdas a datos consumo
                //INICIO ENCABEZADO RESUMEN
                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(concepto);
                Resumen.AddCell(tarifa);
                Resumen.AddCell(totaldolarucos);
                Resumen.AddCell(blancosfinalresumen);
                //FIN ENCABEZADO RESUMEN

                if (DatosPesosMexicanos[0] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIAABASTOAISLADO);
                    Resumen.AddCell(EAAT);
                    Resumen.AddCell(EAAUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[1] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIACONTRATADA);
                    Resumen.AddCell(ECT);
                    Resumen.AddCell(ECUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[2] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIAMEM);
                    Resumen.AddCell(EMT);
                    Resumen.AddCell(EMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[3] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIAVENDIDA);
                    Resumen.AddCell(EVT);
                    Resumen.AddCell(EVUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[4] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(COSTOSOPERATIVOS);
                    Resumen.AddCell(COT);
                    Resumen.AddCell(COUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[5] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(AJUSTE);
                    Resumen.AddCell(AJT);
                    Resumen.AddCell(AJUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[6] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(DESVIACION);
                    Resumen.AddCell(DET);
                    Resumen.AddCell(DEUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[7] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(RPERDIDASNOTECNICAS);
                    Resumen.AddCell(PNTT);
                    Resumen.AddCell(PNTUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[8] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(PERDIDASDELARED);
                    Resumen.AddCell(PDLRT);
                    Resumen.AddCell(PDLRUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[9] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(SERVICIOSCONEXOS);
                    Resumen.AddCell(SCT);
                    Resumen.AddCell(SCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[10] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(COSTOOPERACIONCENACE);
                    Resumen.AddCell(COCCT);
                    Resumen.AddCell(COCCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[11] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(OPERACIONCENACEGENERADOR);
                    Resumen.AddCell(COCGT);
                    Resumen.AddCell(COCGUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[12] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(GARANTIASUFICIENCIA);
                    Resumen.AddCell(GDST);
                    Resumen.AddCell(GDSUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[13] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(GARANTIANTECENACE);
                    Resumen.AddCell(GRACT);
                    Resumen.AddCell(GRACUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }
                if (DatosPesosMexicanos[14] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(TRANSMISIÓNCARGA);
                    Resumen.AddCell(TCT);
                    Resumen.AddCell(TCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[15] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(TRANSMISIÓNGENERADOR);
                    Resumen.AddCell(TGT);
                    Resumen.AddCell(TGUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[16] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(POTENCIA);
                    Resumen.AddCell(PT);
                    Resumen.AddCell(PUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[17] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(POTENCIABASTOAISLADO);
                    Resumen.AddCell(PAAT);
                    Resumen.AddCell(PAAUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[18] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(SERVICIODISTRIBUCION);
                    Resumen.AddCell(SDT);
                    Resumen.AddCell(SDUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[19] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(CONGESTIONMEM);
                    Resumen.AddCell(CMT);
                    Resumen.AddCell(CMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[20] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(CONGESTIONGENERADOR);
                    Resumen.AddCell(CGT);
                    Resumen.AddCell(CGUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[21] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(CERTIFICADOSENERGIALIMPIA);
                    Resumen.AddCell(CELT);
                    Resumen.AddCell(CELUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[22] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ADICIONALESCERTIFICADOSENERGIALIMPIA);
                    Resumen.AddCell(CELAT);
                    Resumen.AddCell(CELAUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[23] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(RELIQUIDACIONES);
                    Resumen.AddCell(RLT);
                    Resumen.AddCell(RLUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[24] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(OTROSCARGOSYABONOS);
                    Resumen.AddCell(OCYAT);
                    Resumen.AddCell(OCYAUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[25] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(AMMPER);
                    Resumen.AddCell(AMMT);
                    Resumen.AddCell(AMMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(SUBTOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(SUBTRUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(IVARESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(IVARUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TOTALRUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TIPOEXCHANGE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TIPOEXCHRUSD);
                Resumen.AddCell(blancosfinalresumen);
                //Agregardatosresumen
                doc.Add(Resumen);



                //tabla de paginacion
                //PdfPTable Paginacion = new PdfPTable(1);
                //Paginacion.WidthPercentage = 95;
                //Paginacion.SetAbsolutePosition(0,10);

                doc.Close();
                writer.Close();
                Status = "Proceso exitoso, terminado a las:" + DateTime.Today.ToLongTimeString();
                UpdateLogFile(Status);
                Byte[] bytespdf = File.ReadAllBytes(Path);
                StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
            }
            catch (Exception ex)
            {
                Status = "Hubo un error a las " + DateTime.Today.ToLongTimeString() + ex;
                UpdateLogFile(Status);
            }

        }

        // PDF PDF-RESUMEN
        static void PDF3(string Grafica, string tipoPDF, string tipoCambio, string factura, string logotipo, string DataResumen, string Etc)
        {
            GraphicExcelVersion = tipoPDF;
            string info = Grafica.Substring(Grafica.IndexOf("|") + 1);
            string InfoForQR = factura.Substring(0, factura.IndexOf("|"));
            string workingInfo = factura.Substring(factura.IndexOf("|") + 1);
            CreateExcelData(info);
            CreateGrafica1();
            CreateQRImage(InfoForQR, Etc);
            string Status = "";

            //declaracion y extraccion de datos
            string catchPhrase = workingInfo.Substring(0, workingInfo.IndexOf("|")); //frase que se debe dividir en 2 renglones
            string temporalsincatch = workingInfo.Substring(workingInfo.IndexOf("|") + 1);
            string InfoTotalAPagar = temporalsincatch.Substring(0, temporalsincatch.IndexOf("|")); // total a pagar
            string temporalsintotalbill = temporalsincatch.Substring(temporalsincatch.IndexOf("|") + 1);
            string InfoPeriodoConsumo = temporalsintotalbill.Substring(0, temporalsintotalbill.IndexOf("|")); //periodo de consumo plaintext
            string temporalsinPeriodoConsumo = temporalsintotalbill.Substring(temporalsintotalbill.IndexOf("|") + 1);
            string InfoFechaLimiteBill = temporalsinPeriodoConsumo.Substring(0, temporalsinPeriodoConsumo.IndexOf("|"));//FechaLimite de pago
            string temporalSinfechalimite = temporalsinPeriodoConsumo.Substring(temporalsinPeriodoConsumo.IndexOf("|") + 1);
            string InfoNumeroDeDocumento = temporalSinfechalimite.Substring(0, temporalSinfechalimite.IndexOf("|"));//numero de documento
            string temporalsinnumerodocumento = temporalSinfechalimite.Substring(temporalSinfechalimite.IndexOf("|") + 1);
            string InfoRMU = temporalsinnumerodocumento.Substring(0, temporalsinnumerodocumento.IndexOf("|"));//DATORMU
            string InfoClientesPordividir = temporalsinnumerodocumento.Substring(temporalsinnumerodocumento.IndexOf("|") + 1);//DATOS A DIVIDIR CLIENTE
                                                                                                                              //
                                                                                                                              //DIVIDIR INFORMACION DEL CLIENTE DE FACTURA
            String DivisorFiscal = "%";
            String[] FacturaCliente = System.Text.RegularExpressions.Regex.Split(InfoClientesPordividir, DivisorFiscal);


            //DIVIDIR CATCHPHRASE
            String separador = " ";
            String[] fraseSuper = System.Text.RegularExpressions.Regex.Split(catchPhrase, separador);
            int contador = 0;
            foreach (var element in fraseSuper)
            {
                contador++;

            }
            String Textosuperior = "", TextoInferior = "";
            if (contador > 1)
            {
                int totalinicio = contador / 2;
                totalinicio = totalinicio + 2;

                for (int m = 0; m < totalinicio; m++)
                {
                    Textosuperior = Textosuperior + fraseSuper[m] + " ";
                }
                for (int r = totalinicio; r < contador; r++)
                {
                    TextoInferior = TextoInferior + " " + fraseSuper[r];
                }
            }
            else
            {
                Textosuperior = " ";
                TextoInferior = catchPhrase + " ";
            }

            //DATOS RESUMEN PARA MEDIDOR Y PARA CONCEPTOS
            string MEDIDOR = DataResumen.Substring(0, DataResumen.IndexOf("|"));
            string temporalconsumo = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
            string TablaConsumo = temporalconsumo.Substring(0, temporalconsumo.IndexOf("|"));
            string temporalConceptos = temporalconsumo.Substring(temporalconsumo.IndexOf("|") + 1);
            string ConceptosMexicanos = temporalConceptos.Substring(0, temporalConceptos.IndexOf("|"));

            //Ciclos para datos con coma
            String elseparador = "-";
            String separadordolar = "/";
            String[] DatosParaTablaConsumo = System.Text.RegularExpressions.Regex.Split(TablaConsumo, separadordolar);
            String[] DatosDolares = System.Text.RegularExpressions.Regex.Split(ConceptosMexicanos, separadordolar);

            try
            {
                Document doc = new Document(PageSize.LETTER);
                string Path = @"c:\PruebasPDF\PDF-RESUMEN.pdf";
                //Ruta
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Path, FileMode.Create));

                //metadatos archivo
                doc.AddTitle("PDF-Amper USD Cargas");
                doc.AddCreator("Prime Consultoria");
                doc.SetMargins(5, 5, 5, 5);
                //Abrir Archivo
                doc.Open();
                //Definir Styles
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardMedidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _Medidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _secundaryfont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _headers = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
                iTextSharp.text.Font _StandardBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _TOTALBOLD = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _SecundaryBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font _resumee = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5, iTextSharp.text.Font.NORMAL, BaseColor.RED);
                iTextSharp.text.Font _resumeeheader = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.RED);

                // Escribimos el encabezamiento en el documento

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 95;

                //header.= BaseColor.RED;


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\LOGOAMPER.png");
                gif.SetAbsolutePosition(20, 750);
                gif.ScaleToFit(100f, 200F);

                iTextSharp.text.Image CFID = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\test.png");
                CFID.SetAbsolutePosition(135, 180);  //135,130
                CFID.ScaleToFit(200f, 110F);
                doc.Add(CFID);
                //Declarar columnas
                PdfPCell conusoenergia = new PdfPCell(new Phrase(Textosuperior, _StandardBold));
                conusoenergia.BorderWidth = 0;
                conusoenergia.HorizontalAlignment = 2;
                //conusoenergia.BorderWidthBottom = 0.75f;
                PdfPCell metasenmexico = new PdfPCell(new Phrase(TextoInferior, _StandardBold));
                metasenmexico.BorderWidth = 0;
                metasenmexico.HorizontalAlignment = 2;
                //espacio en blanco
                PdfPCell espacio = new PdfPCell(new Phrase(" ", _StandardBold));
                espacio.BorderWidth = 0;
                espacio.HorizontalAlignment = 2;
                //linea Roja
                PdfPCell linearoja = new PdfPCell(new Phrase(" ", _StandardBold));
                linearoja.BorderWidth = 0;
                linearoja.HorizontalAlignment = 2;
                linearoja.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                PdfPCell lineaBlanca = new PdfPCell(new Phrase(" ", _StandardBold));
                lineaBlanca.BorderWidth = 0;
                lineaBlanca.HorizontalAlignment = 2;
                lineaBlanca.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);


                //Agregar Columnas header
                header.AddCell(espacio);
                header.AddCell(conusoenergia);
                header.AddCell(metasenmexico);
                header.AddCell(linearoja);
                header.AddCell(lineaBlanca);
                //agregar datos 
                doc.Add(gif);

                //comparacion condicional segundo logotipo

                if (logotipo == "1")
                {
                    iTextSharp.text.Image secondhead = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\logocabezados.png");
                    secondhead.SetAbsolutePosition(140, 750);
                    secondhead.ScaleToFit(50f, 100F);
                    doc.Add(secondhead);
                }
                doc.Add(header);
                //GRAFICA Y CODIGO QR

                iTextSharp.text.Image grafica = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\grafico.png");
                grafica.SetAbsolutePosition(15, 300);
                grafica.ScaleToFit(320f, 200F);
                doc.Add(grafica);


                iTextSharp.text.Image qr = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\qr.png");
                qr.SetAbsolutePosition(15, 170);
                qr.ScaleToFit(175f, 110F);
                doc.Add(qr);


                //Pie de pagina

                PdfPTable tab = new PdfPTable(1);
                PdfPCell cell = new PdfPCell(new Phrase("Este documento es una representación impresa de un CFDI", _secundaryfont));
                cell.Border = 0;
                tab.TotalWidth = 300F;
                tab.AddCell(cell);
                tab.WriteSelectedRows(0, -1, 200, 40, writer.DirectContent);
                doc.Add(Chunk.NEWLINE);

                //TERMINO HEADER
                //Empiezo Datos Cliente
                PdfPTable DatosCliente = new PdfPTable(4);
                DatosCliente.WidthPercentage = 95;
                //cells Datos Cliente
                //CellSaltoDeLinea
                PdfPCell Salto = new PdfPCell(new Phrase(" ", _headers));
                Salto.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                Salto.BorderWidth = 0;
                Salto.HorizontalAlignment = 1;
                Salto.Colspan = 3;
                //Cellcoral


                //go
                PdfPCell Dependencia = new PdfPCell(new Phrase("AMMPER Energia, S.A.P.I. de C.V. ", _standardFont));
                Dependencia.Colspan = 3;
                Dependencia.BorderWidth = 0;
                Dependencia.HorizontalAlignment = 0;

                PdfPCell TotalPago = new PdfPCell(new Phrase(" Total a pagar ", _headers));
                TotalPago.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                TotalPago.BorderWidth = 0;
                TotalPago.HorizontalAlignment = 1;

                PdfPCell periodofact = new PdfPCell(new Phrase(" del periodo facturado ", _headers));
                periodofact.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodofact.BorderWidth = 0;
                periodofact.HorizontalAlignment = 1;

                PdfPCell calle = new PdfPCell(new Phrase("Av. Paseo de la Reforma ", _standardFont));
                calle.Colspan = 3;
                calle.BorderWidth = 0;
                calle.HorizontalAlignment = 0;

                PdfPCell coral = new PdfPCell(new Phrase(" ", _headers));
                coral.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                coral.BorderWidth = 0;
                coral.HorizontalAlignment = 1;



                PdfPCell numerodireccion = new PdfPCell(new Phrase("243 Piso 19", _standardFont));
                numerodireccion.Colspan = 3;
                numerodireccion.BorderWidth = 0;
                numerodireccion.HorizontalAlignment = 0;

                PdfPCell numerospago = new PdfPCell(new Phrase(InfoTotalAPagar, _TOTALBOLD));
                numerospago.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numerospago.BorderWidth = 0;
                numerospago.HorizontalAlignment = 1;

                PdfPCell codigopostal = new PdfPCell(new Phrase("Cuauhtémoc, CP.06500", _standardFont));
                codigopostal.Colspan = 3;
                codigopostal.BorderWidth = 0;
                codigopostal.HorizontalAlignment = 0;

                PdfPCell saltopagar = new PdfPCell(new Phrase(" ", _StandardBold));
                saltopagar.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                saltopagar.BorderWidth = 0;
                saltopagar.HorizontalAlignment = 1;

                PdfPCell ciudadfactura = new PdfPCell(new Phrase("Ciudad de México, México", _standardFont));
                ciudadfactura.Colspan = 3;
                ciudadfactura.BorderWidth = 0;
                ciudadfactura.HorizontalAlignment = 0;

                PdfPCell periodoconsumo = new PdfPCell(new Phrase("Periodo del Consumo ", _headers));
                periodoconsumo.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodoconsumo.BorderWidth = 0;
                periodoconsumo.HorizontalAlignment = 1;

                PdfPCell rfcamper = new PdfPCell(new Phrase("R. F. C. AEN160405E56", _secundaryfont));
                rfcamper.Colspan = 3;
                rfcamper.BorderWidth = 0;
                rfcamper.HorizontalAlignment = 0;

                PdfPCell fechasperiodo = new PdfPCell(new Phrase(InfoPeriodoConsumo, _StandardBold));
                fechasperiodo.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                fechasperiodo.BorderWidth = 0;
                fechasperiodo.HorizontalAlignment = 1;

                PdfPCell direccioncliente = new PdfPCell(new Phrase("Nombre y domicilio", _SecundaryBold));
                direccioncliente.Colspan = 3;
                direccioncliente.BorderWidth = 2;
                direccioncliente.HorizontalAlignment = 0;


                direccioncliente.BorderColor = new BaseColor(255, 0, 0);

                direccioncliente.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                //RMU

                PdfPCell RMUENCABEZADO = new PdfPCell(new Phrase("RMU", _headers));
                RMUENCABEZADO.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                RMUENCABEZADO.BorderWidth = 0;
                RMUENCABEZADO.HorizontalAlignment = 1;


                PdfPCell RMUARRIBA = new PdfPCell(new Phrase(InfoRMU, _StandardBold));
                RMUARRIBA.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                RMUARRIBA.BorderWidth = 0;
                RMUARRIBA.HorizontalAlignment = 1;

                //Datos cliente de Amper

                PdfPCell nombreydomicilio = new PdfPCell(new Phrase(FacturaCliente[0], _standardFont));
                nombreydomicilio.Colspan = 2;
                nombreydomicilio.BorderWidth = 0;
                nombreydomicilio.HorizontalAlignment = 0;

                PdfPCell DOMICILIOREAL = new PdfPCell(new Phrase(FacturaCliente[1], _standardFont));
                DOMICILIOREAL.Colspan = 2;
                DOMICILIOREAL.BorderWidth = 0;
                DOMICILIOREAL.HorizontalAlignment = 0;

                //PdfPCell NumeroDoc = new PdfPCell(new Phrase("Número de documento", _SecundaryBold));
                //NumeroDoc.BorderWidth = 2;
                //NumeroDoc.HorizontalAlignment = 0;
                //NumeroDoc.BorderColor = new BaseColor(255, 0, 0);
                //NumeroDoc.Border = Rectangle.BOTTOM_BORDER;

                PdfPCell taimlimit = new PdfPCell(new Phrase(" Fecha límite de pago", _headers));
                taimlimit.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                taimlimit.BorderWidth = 0;
                taimlimit.HorizontalAlignment = 1;

                PdfPCell espacioderelleno = new PdfPCell(new Phrase(" ", _standardFont));
                espacioderelleno.Colspan = 2;
                espacioderelleno.BorderWidth = 0;
                espacioderelleno.HorizontalAlignment = 0;


                PdfPCell lafechalimite = new PdfPCell(new Phrase(InfoFechaLimiteBill, _StandardBold));
                lafechalimite.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                lafechalimite.BorderWidth = 0;
                lafechalimite.HorizontalAlignment = 1;

                PdfPCell RMUtitulo = new PdfPCell(new Phrase("Cuenta de Orden", _SecundaryBold));
                RMUtitulo.BorderWidth = 2;
                RMUtitulo.HorizontalAlignment = 0;
                RMUtitulo.BorderColor = new BaseColor(255, 0, 0);
                RMUtitulo.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell telefonoemergencia = new PdfPCell(new Phrase(" Teléfono de emergencia", _headers));
                telefonoemergencia.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                telefonoemergencia.BorderWidth = 0;
                telefonoemergencia.HorizontalAlignment = 1;

                PdfPCell NUMRMU = new PdfPCell(new Phrase(InfoNumeroDeDocumento, _standardFont));

                NUMRMU.BorderWidth = 0;
                NUMRMU.HorizontalAlignment = 0;

                PdfPCell numero8cientos = new PdfPCell(new Phrase("01 8009 266737", _StandardBold));
                numero8cientos.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numero8cientos.BorderWidth = 0;
                numero8cientos.HorizontalAlignment = 1;

                PdfPCell MedicionConsum = new PdfPCell(new Phrase("", _SecundaryBold));
                MedicionConsum.BorderWidth = 0;
                MedicionConsum.HorizontalAlignment = 0;

                PdfPCell cientos8amper = new PdfPCell(new Phrase("   (AMMPER)", _StandardBold));
                cientos8amper.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                cientos8amper.BorderWidth = 0;
                cientos8amper.HorizontalAlignment = 1;




                //Agregar Celdas a Datoscliente
                DatosCliente.AddCell(Dependencia);
                DatosCliente.AddCell(TotalPago);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(periodofact);
                DatosCliente.AddCell(calle);
                DatosCliente.AddCell(coral);
                DatosCliente.AddCell(numerodireccion);
                DatosCliente.AddCell(numerospago);
                DatosCliente.AddCell(codigopostal);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(ciudadfactura);
                DatosCliente.AddCell(RMUENCABEZADO);
                DatosCliente.AddCell(rfcamper);
                DatosCliente.AddCell(RMUARRIBA);
                DatosCliente.AddCell(direccioncliente);
                DatosCliente.AddCell(periodoconsumo);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(fechasperiodo);
                DatosCliente.AddCell(nombreydomicilio);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(taimlimit);
                DatosCliente.AddCell(DOMICILIOREAL);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(lafechalimite);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(RMUtitulo);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(telefonoemergencia);
                DatosCliente.AddCell(NUMRMU);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(numero8cientos);
                DatosCliente.AddCell(MedicionConsum);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(cientos8amper);



                //agregar datos 
                doc.Add(DatosCliente);

                //Tabla de medicion de consumo
                PdfPTable DatosdeConsumo = new PdfPTable(8);
                DatosdeConsumo.WidthPercentage = 95;

                //Declaracion de celdas
                //declaracion salto de linea completo
                PdfPCell saltosupermegagenial = new PdfPCell(new Phrase("", _secundaryfont));
                saltosupermegagenial.BorderWidth = 0;
                saltosupermegagenial.Colspan = 8;
                //encabezados
                PdfPCell Nummedidor = new PdfPCell(new Phrase(" No. Medidor ", _SecundaryBold));
                Nummedidor.BorderWidth = 2;
                Nummedidor.HorizontalAlignment = 0;
                Nummedidor.BorderColor = new BaseColor(255, 0, 0);
                Nummedidor.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell Tituloconsumomensual = new PdfPCell(new Phrase(" Consumo Mensual (MWh) ", _SecundaryBold));
                Tituloconsumomensual.BorderWidth = 2;
                Tituloconsumomensual.HorizontalAlignment = Element.ALIGN_CENTER;
                Tituloconsumomensual.BorderColor = new BaseColor(255, 0, 0);
                Tituloconsumomensual.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Tituloconsumomensual.Colspan = 3;

                PdfPCell TarifaSumi = new PdfPCell(new Phrase(" Tarifa de Suministro ($/MWh) ", _SecundaryBold));
                TarifaSumi.BorderWidth = 2;
                TarifaSumi.HorizontalAlignment = Element.ALIGN_CENTER;
                TarifaSumi.BorderColor = new BaseColor(255, 0, 0);
                TarifaSumi.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                TarifaSumi.Colspan = 2;

                PdfPCell Superpotencia100 = new PdfPCell(new Phrase("Potencia 100 horas críticas (MW)", _SecundaryBold));
                Superpotencia100.BorderWidth = 2;
                Superpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                Superpotencia100.BorderColor = new BaseColor(255, 0, 0);
                Superpotencia100.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Superpotencia100.Colspan = 2;

                //Contenido

                //Dejar espacios blancos en medidor

                PdfPCell espaciomedidor = new PdfPCell(new Phrase(" ", _standardFont));
                espaciomedidor.BorderWidth = 0;
                espaciomedidor.HorizontalAlignment = 0;


                //medidor
                PdfPCell cualmedidor = new PdfPCell(new Phrase(MEDIDOR, _Medidor));
                cualmedidor.BorderWidth = 0;
                cualmedidor.HorizontalAlignment = 0;
                cualmedidor.Colspan = 2;

                //consumoneto




                PdfPCell cantidadconsumoneto = new PdfPCell(new Phrase(DatosParaTablaConsumo[0], _standardMedidor));
                cantidadconsumoneto.BorderWidth = 0;
                cantidadconsumoneto.HorizontalAlignment = 0;
                cantidadconsumoneto.Colspan = 3;

                //Tarifa
                PdfPCell cantidadtarifa = new PdfPCell(new Phrase(DatosParaTablaConsumo[1], _standardMedidor));
                cantidadtarifa.BorderWidth = 0;
                cantidadtarifa.HorizontalAlignment = 0;
                cantidadtarifa.Colspan = 2;



                //potencia super cantidad!
                PdfPCell cantidadpotencia100 = new PdfPCell(new Phrase(DatosParaTablaConsumo[2], _standardMedidor));
                cantidadpotencia100.BorderWidth = 0;
                cantidadpotencia100.HorizontalAlignment = Element.ALIGN_LEFT;
                cantidadpotencia100.Colspan = 2;

                //Header Indicador de consumo grafica e

                PdfPCell headergrafica = new PdfPCell(new Phrase("Indicador de consumo: E", _SecundaryBold));
                headergrafica.BorderWidth = 1;
                headergrafica.HorizontalAlignment = 0;
                headergrafica.BorderColor = new BaseColor(255, 0, 0);
                headergrafica.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                headergrafica.Colspan = 2;
                //Agregar celdas a datos consumo

                DatosdeConsumo.AddCell(Nummedidor);
                DatosdeConsumo.AddCell(Tituloconsumomensual);
                DatosdeConsumo.AddCell(TarifaSumi);
                DatosdeConsumo.AddCell(Superpotencia100);
                DatosdeConsumo.AddCell(saltosupermegagenial);
                DatosdeConsumo.AddCell(cualmedidor);
                //DatosdeConsumo.AddCell(consumoneto);
                DatosdeConsumo.AddCell(cantidadconsumoneto);
                DatosdeConsumo.AddCell(cantidadtarifa);
                //DatosdeConsumo.AddCell(conceptotarifa);
                DatosdeConsumo.AddCell(cantidadpotencia100);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(saltosupermegagenial);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(saltosupermegagenial);
                DatosdeConsumo.AddCell(headergrafica);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(espaciomedidor);

                //AgregardatosConsumo
                doc.Add(DatosdeConsumo);

                doc.Add(Chunk.NEWLINE);

                //tabla Desglosada de resumen
                PdfPTable Resumen = new PdfPTable(14);
                Resumen.WidthPercentage = 100;

                //Espacios Blancos de resumen

                PdfPCell blancosresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosresumen.BorderWidth = 0;
                blancosresumen.HorizontalAlignment = 0;
                blancosresumen.Colspan = 8;

                PdfPCell blancosfinalresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosfinalresumen.BorderWidth = 0;
                blancosfinalresumen.HorizontalAlignment = 0;

                //Crear datos encabezado resumee

                PdfPCell concepto = new PdfPCell(new Phrase("Concepto ", _resumeeheader));
                concepto.BorderWidth = 1;
                concepto.HorizontalAlignment = 0;
                concepto.BorderColor = new BaseColor(255, 0, 0);
                concepto.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                concepto.Colspan = 3;

                PdfPCell tarifa = new PdfPCell(new Phrase(" Precio ("+ tipoCambio + "/MWh) ", _resumeeheader));
                tarifa.BorderWidth = 1;
                tarifa.HorizontalAlignment = 1;
                tarifa.BorderColor = new BaseColor(255, 0, 0);
                tarifa.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;


                PdfPCell totaldolarucos = new PdfPCell(new Phrase(" Precio Total (" + tipoCambio + ") ", _resumeeheader));
                totaldolarucos.BorderWidth = 1;
                totaldolarucos.HorizontalAlignment = 1;
                totaldolarucos.BorderColor = new BaseColor(255, 0, 0);
                totaldolarucos.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                //Comparacion de datos Resume Dolarucos
                for (int a = 0; a < 5; a++)
                {
                    string pesostemporales = DatosDolares[a];
                    if (DatosDolares[a] != "")
                    {
                        if (DatosDolares[a].Substring(0, 1) == "-")
                        {
                            DatosDolares[a] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            DatosDolares[a] = " $" + pesostemporales;

                        }
                    }
                }

                //Creardatos resumee

                PdfPCell ENERGIACONTRATADA = new PdfPCell(new Phrase("ENERGÍA, POTENCIA, CELS Y AMMPER", _resumee));
                ENERGIACONTRATADA.BorderWidth = 0;
                ENERGIACONTRATADA.HorizontalAlignment = 0;
                ENERGIACONTRATADA.Colspan = 3;

                PdfPCell ECT = new PdfPCell(new Phrase(DatosDolares[0], _resumee));
                ECT.BorderWidth = 0;
                ECT.HorizontalAlignment = 2;

                PdfPCell ECUSD = new PdfPCell(new Phrase(DatosDolares[1], _resumee));
                ECUSD.BorderWidth = 0;
                ECUSD.HorizontalAlignment = 2;


                PdfPCell SUBTOTALRESUMEE = new PdfPCell(new Phrase("SUBTOTAL", _resumee));
                SUBTOTALRESUMEE.BorderWidth = 0;
                SUBTOTALRESUMEE.HorizontalAlignment = 0;
                SUBTOTALRESUMEE.Colspan = 3;


                PdfPCell SUBTRUSD = new PdfPCell(new Phrase(DatosDolares[2], _resumee));
                SUBTRUSD.BorderWidth = 0;
                SUBTRUSD.HorizontalAlignment = 2;
                // subtotal RESUMEE

                PdfPCell IVARESUMEE = new PdfPCell(new Phrase("IVA", _resumee));
                IVARESUMEE.BorderWidth = 0;
                IVARESUMEE.HorizontalAlignment = 0;
                IVARESUMEE.Colspan = 3;


                PdfPCell IVARUSD = new PdfPCell(new Phrase(DatosDolares[3], _resumee));
                IVARUSD.BorderWidth = 0;
                IVARUSD.HorizontalAlignment = 2;
                // IVA

                PdfPCell TOTALRESUMEE = new PdfPCell(new Phrase("TOTAL", _resumee));
                TOTALRESUMEE.BorderWidth = 0;
                TOTALRESUMEE.HorizontalAlignment = 0;
                TOTALRESUMEE.Colspan = 3;


                PdfPCell TOTALRUSD = new PdfPCell(new Phrase(DatosDolares[4], _resumee));
                TOTALRUSD.BorderWidth = 0;
                TOTALRUSD.HorizontalAlignment = 2;
                // TOTAL RESUMEE



                //Agregar celdas a datos consumo
                //INICIO ENCABEZADO RESUMEN
                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(concepto);
                Resumen.AddCell(tarifa);
                Resumen.AddCell(totaldolarucos);
                Resumen.AddCell(blancosfinalresumen);
                //FIN ENCABEZADO RESUMEN


                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(ENERGIACONTRATADA);
                Resumen.AddCell(ECT);
                Resumen.AddCell(ECUSD);
                Resumen.AddCell(blancosfinalresumen);



                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(SUBTOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(SUBTRUSD);
                Resumen.AddCell(blancosfinalresumen);



                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(IVARESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(IVARUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TOTALRUSD);
                Resumen.AddCell(blancosfinalresumen);


                //Agregardatosresumen
                doc.Add(Resumen);



                //tabla de paginacion
                //PdfPTable Paginacion = new PdfPTable(1);
                //Paginacion.WidthPercentage = 95;
                //Paginacion.SetAbsolutePosition(0,10);

                doc.Close();
                writer.Close();
                Status = "Proceso exitoso, terminado a las:" + DateTime.Today.ToLongTimeString();
                UpdateLogFile(Status);
                Byte[] bytespdf = File.ReadAllBytes(Path);
                StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
            }
            catch (Exception ex)
            {
                Status = "Hubo un error a las " + DateTime.Today.ToLongTimeString() + ex;
                UpdateLogFile(Status);
            }

        }

        // PDF CONSUMIDO-CONTRATADO
        static void PDF4(string Grafica, string tipoPDF, string tipoCambio, string factura, string logotipo, string DataResumen, string Etc)
        {
            GraphicExcelVersion = tipoPDF;
            string info = Grafica.Substring(Grafica.IndexOf("|") + 1);
            string InfoForQR = factura.Substring(0, factura.IndexOf("|"));
            string workingInfo = factura.Substring(factura.IndexOf("|") + 1);
            CreateExcelData2(info);
            CreateGrafica1();
            CreateQRImage(InfoForQR, Etc);

            //declaracion y extraccion de datos
            string catchPhrase = workingInfo.Substring(0, workingInfo.IndexOf("|")); //frase que se debe dividir en 2 renglones
            string temporalsincatch = workingInfo.Substring(workingInfo.IndexOf("|") + 1);
            string InfoTotalAPagar = temporalsincatch.Substring(0, temporalsincatch.IndexOf("|")); // total a pagar
            string temporalsintotalbill = temporalsincatch.Substring(temporalsincatch.IndexOf("|") + 1);
            string InfoPeriodoConsumo = temporalsintotalbill.Substring(0, temporalsintotalbill.IndexOf("|")); //periodo de consumo plaintext
            string temporalsinPeriodoConsumo = temporalsintotalbill.Substring(temporalsintotalbill.IndexOf("|") + 1);
            string InfoFechaLimiteBill = temporalsinPeriodoConsumo.Substring(0, temporalsinPeriodoConsumo.IndexOf("|"));//FechaLimite de pago
            string temporalSinfechalimite = temporalsinPeriodoConsumo.Substring(temporalsinPeriodoConsumo.IndexOf("|") + 1);
            string InfoNumeroDeDocumento = temporalSinfechalimite.Substring(0, temporalSinfechalimite.IndexOf("|"));//numero de documento
            string temporalsinnumerodocumento = temporalSinfechalimite.Substring(temporalSinfechalimite.IndexOf("|") + 1);
            string InfoRMU = temporalsinnumerodocumento.Substring(0, temporalsinnumerodocumento.IndexOf("|"));//DATORMU
            string InfoClientesPordividir = temporalsinnumerodocumento.Substring(temporalsinnumerodocumento.IndexOf("|") + 1);//DATOS A DIVIDIR CLIENTE
            //
            //DIVIDIR INFORMACION DEL CLIENTE DE FACTURA
            String DivisorFiscal = "%";
            String[] FacturaCliente = System.Text.RegularExpressions.Regex.Split(InfoClientesPordividir, DivisorFiscal);


            //DIVIDIR CATCHPHRASE
            String separador = " ";
            String[] fraseSuper = System.Text.RegularExpressions.Regex.Split(catchPhrase, separador);
            int contador = 0;
            foreach (var element in fraseSuper)
            {
                contador++;

            }
            String Textosuperior = "", TextoInferior = "";
            if (contador > 1)
            {
                int totalinicio = contador / 2;
                totalinicio = totalinicio + 2;

                for (int m = 0; m < totalinicio; m++)
                {
                    Textosuperior = Textosuperior + fraseSuper[m] + " ";
                }
                for (int r = totalinicio; r < contador; r++)
                {
                    TextoInferior = TextoInferior + " " + fraseSuper[r];
                }
            }
            else
            {
                Textosuperior = " ";
                TextoInferior = catchPhrase + " ";
            }

            //DATOS RESUMEN PARA MEDIDOR Y PARA CONCEPTOS
            string MEDIDOR = DataResumen.Substring(0, DataResumen.IndexOf("|"));
            string temporalconsumo = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
            string TablaConsumo = temporalconsumo.Substring(0, temporalconsumo.IndexOf("|"));
            string temporalConceptos = temporalconsumo.Substring(temporalconsumo.IndexOf("|") + 1);
            string ConceptosMexicanos = temporalConceptos.Substring(0, temporalConceptos.IndexOf("|"));
            string ConceptosUSD = temporalConceptos.Substring(temporalConceptos.IndexOf("|") + 1);

            //Ciclos para datos con coma
            String elseparador = "-";
            string elmegaseparador = "/";
            String[] DatosParaTablaConsumo = System.Text.RegularExpressions.Regex.Split(TablaConsumo, elmegaseparador);
            String[] DatosPesosMexicanos = System.Text.RegularExpressions.Regex.Split(ConceptosMexicanos, elmegaseparador);
            String[] DatosDonalds = System.Text.RegularExpressions.Regex.Split(ConceptosUSD, elmegaseparador);

            string Status;
            try
            {
                Document doc = new Document(PageSize.LETTER);
                string Path = @"c:\PruebasPDF\CONSUMIDO-CONTRATADO.pdf";
                //Ruta
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Path, FileMode.Create));

                //metadatos archivo
                doc.AddTitle("PDF-Amper USD Cargas");
                doc.AddCreator("Prime Consultoria");
                doc.SetMargins(5, 5, 5, 5);
                //Abrir Archivo
                doc.Open();
                //Definir Styles
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardMedidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _Medidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _secundaryfont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _headers = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
                iTextSharp.text.Font _StandardBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _TOTALBOLD = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _SecundaryBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font _resumee = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5, iTextSharp.text.Font.NORMAL, BaseColor.RED);
                iTextSharp.text.Font _resumeeheader = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.RED);

                // Escribimos el encabezamiento en el documento

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 95;

                //header.= BaseColor.RED;


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\LOGOAMPER.png");
                gif.SetAbsolutePosition(20, 750);
                gif.ScaleToFit(100f, 200F);

                iTextSharp.text.Image CFID = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\test.png");
                CFID.SetAbsolutePosition(135, 150); //135, 130
                CFID.ScaleToFit(200f, 110F);
                doc.Add(CFID);
                //Declarar columnas
                PdfPCell conusoenergia = new PdfPCell(new Phrase(Textosuperior, _StandardBold));
                conusoenergia.BorderWidth = 0;
                conusoenergia.HorizontalAlignment = 2;
                //conusoenergia.BorderWidthBottom = 0.75f;
                PdfPCell metasenmexico = new PdfPCell(new Phrase(TextoInferior, _StandardBold));
                metasenmexico.BorderWidth = 0;
                metasenmexico.HorizontalAlignment = 2;
                //espacio en blanco
                PdfPCell espacio = new PdfPCell(new Phrase(" ", _StandardBold));
                espacio.BorderWidth = 0;
                espacio.HorizontalAlignment = 2;
                //linea Roja
                PdfPCell linearoja = new PdfPCell(new Phrase(" ", _StandardBold));
                linearoja.BorderWidth = 0;
                linearoja.HorizontalAlignment = 2;
                linearoja.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                PdfPCell lineaBlanca = new PdfPCell(new Phrase(" ", _StandardBold));
                lineaBlanca.BorderWidth = 0;
                lineaBlanca.HorizontalAlignment = 2;
                lineaBlanca.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);


                //Agregar Columnas header
                header.AddCell(espacio);
                header.AddCell(conusoenergia);
                header.AddCell(metasenmexico);
                header.AddCell(linearoja);
                header.AddCell(lineaBlanca);
                //agregar datos 
                doc.Add(gif);
                //comparacion condicional segundo logotipo

                if (logotipo == "1")
                {
                    iTextSharp.text.Image secondhead = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\logocabezados.png");
                    secondhead.SetAbsolutePosition(140, 750);
                    secondhead.ScaleToFit(50f, 100F);
                    doc.Add(secondhead);
                }


                doc.Add(header);
                //GRAFICA Y CODIGO QR

                iTextSharp.text.Image grafica = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\grafico.png");
                grafica.SetAbsolutePosition(15, 280);
                grafica.ScaleToFit(320f, 200F);
                doc.Add(grafica);


                iTextSharp.text.Image qr = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\qr.png");
                qr.SetAbsolutePosition(15, 170);
                qr.ScaleToFit(175f, 110F);
                doc.Add(qr);



                //Pie de pagina
                PdfPTable tab = new PdfPTable(1);
                PdfPCell cell = new PdfPCell(new Phrase("Este documento es una representación impresa de un CFDI", _secundaryfont));
                cell.Border = 0;
                tab.TotalWidth = 300F;
                tab.AddCell(cell);
                tab.WriteSelectedRows(0, -1, 200, 40, writer.DirectContent);
                doc.Add(Chunk.NEWLINE);

                //TERMINO HEADER
                //Empiezo Datos Cliente
                PdfPTable DatosCliente = new PdfPTable(4);
                DatosCliente.WidthPercentage = 95;
                //cells Datos Cliente
                //CellSaltoDeLinea
                PdfPCell Salto = new PdfPCell(new Phrase(" ", _headers));
                Salto.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                Salto.BorderWidth = 0;
                Salto.HorizontalAlignment = 1;
                Salto.Colspan = 3;
                //Cellcoral


                //go
                PdfPCell Dependencia = new PdfPCell(new Phrase("AMMPER Energia, S.A.P.I. de C.V. ", _standardFont));
                Dependencia.Colspan = 3;
                Dependencia.BorderWidth = 0;
                Dependencia.HorizontalAlignment = 0;

                PdfPCell TotalPago = new PdfPCell(new Phrase(" Total a pagar ", _headers));
                TotalPago.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                TotalPago.BorderWidth = 0;
                TotalPago.HorizontalAlignment = 1;

                PdfPCell periodofact = new PdfPCell(new Phrase(" del periodo facturado ", _headers));
                periodofact.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodofact.BorderWidth = 0;
                periodofact.HorizontalAlignment = 1;

                PdfPCell calle = new PdfPCell(new Phrase("Av. Paseo de la Reforma ", _standardFont));
                calle.Colspan = 3;
                calle.BorderWidth = 0;
                calle.HorizontalAlignment = 0;

                PdfPCell coral = new PdfPCell(new Phrase(" ", _headers));
                coral.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                coral.BorderWidth = 0;
                coral.HorizontalAlignment = 1;



                PdfPCell numerodireccion = new PdfPCell(new Phrase("243 Piso 19", _standardFont));
                numerodireccion.Colspan = 3;
                numerodireccion.BorderWidth = 0;
                numerodireccion.HorizontalAlignment = 0;

                PdfPCell numerospago = new PdfPCell(new Phrase(" $" + InfoTotalAPagar, _TOTALBOLD));
                numerospago.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numerospago.BorderWidth = 0;
                numerospago.HorizontalAlignment = 1;

                PdfPCell codigopostal = new PdfPCell(new Phrase("Cuauhtémoc, CP.06500", _standardFont));
                codigopostal.Colspan = 3;
                codigopostal.BorderWidth = 0;
                codigopostal.HorizontalAlignment = 0;

                PdfPCell saltopagar = new PdfPCell(new Phrase(" ", _StandardBold));
                saltopagar.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                saltopagar.BorderWidth = 0;
                saltopagar.HorizontalAlignment = 1;

                PdfPCell ciudadfactura = new PdfPCell(new Phrase("Ciudad de México, México", _standardFont));
                ciudadfactura.Colspan = 3;
                ciudadfactura.BorderWidth = 0;
                ciudadfactura.HorizontalAlignment = 0;

                PdfPCell periodoconsumo = new PdfPCell(new Phrase("Periodo del Consumo ", _headers));
                periodoconsumo.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodoconsumo.BorderWidth = 0;
                periodoconsumo.HorizontalAlignment = 1;

                PdfPCell rfcamper = new PdfPCell(new Phrase("R. F. C. AEN160405E56", _secundaryfont));
                rfcamper.Colspan = 3;
                rfcamper.BorderWidth = 0;
                rfcamper.HorizontalAlignment = 0;

                PdfPCell fechasperiodo = new PdfPCell(new Phrase(InfoPeriodoConsumo, _StandardBold));
                fechasperiodo.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                fechasperiodo.BorderWidth = 0;
                fechasperiodo.HorizontalAlignment = 1;

                PdfPCell direccioncliente = new PdfPCell(new Phrase("Nombre y domicilio", _SecundaryBold));
                direccioncliente.Colspan = 3;
                direccioncliente.BorderWidth = 2;
                direccioncliente.HorizontalAlignment = 0;


                direccioncliente.BorderColor = new BaseColor(255, 0, 0);

                direccioncliente.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                //RMU

                PdfPCell RMUENCABEZADO = new PdfPCell(new Phrase("RMU", _headers));
                RMUENCABEZADO.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                RMUENCABEZADO.BorderWidth = 0;
                RMUENCABEZADO.HorizontalAlignment = 1;


                PdfPCell RMUARRIBA = new PdfPCell(new Phrase(InfoRMU, _StandardBold));
                RMUARRIBA.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                RMUARRIBA.BorderWidth = 0;
                RMUARRIBA.HorizontalAlignment = 1;

                //Datos cliente de Amper

                PdfPCell nombreydomicilio = new PdfPCell(new Phrase(FacturaCliente[0], _standardFont));
                nombreydomicilio.Colspan = 2;
                nombreydomicilio.BorderWidth = 0;
                nombreydomicilio.HorizontalAlignment = 0;

                PdfPCell TherealPlace = new PdfPCell(new Phrase(FacturaCliente[1], _standardFont));
                TherealPlace.Colspan = 2;
                TherealPlace.BorderWidth = 0;
                TherealPlace.HorizontalAlignment = 0;


                PdfPCell taimlimit = new PdfPCell(new Phrase(" Fecha límite de pago", _headers));
                taimlimit.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                taimlimit.BorderWidth = 0;
                taimlimit.HorizontalAlignment = 1;

                PdfPCell espacioderelleno = new PdfPCell(new Phrase(" ", _standardFont));
                espacioderelleno.Colspan = 2;
                espacioderelleno.BorderWidth = 0;
                espacioderelleno.HorizontalAlignment = 0;

                //PdfPCell elverdaderodoc = new PdfPCell(new Phrase("2500000221 ", _standardFont));

                //elverdaderodoc.BorderWidth = 0;
                //elverdaderodoc.HorizontalAlignment = 0;

                PdfPCell lafechalimite = new PdfPCell(new Phrase(" " + InfoFechaLimiteBill + " ", _StandardBold));
                lafechalimite.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                lafechalimite.BorderWidth = 0;
                lafechalimite.HorizontalAlignment = 1;

                PdfPCell RMUtitulo = new PdfPCell(new Phrase("Cuenta de Orden", _SecundaryBold));
                RMUtitulo.BorderWidth = 2;
                RMUtitulo.HorizontalAlignment = 0;
                RMUtitulo.BorderColor = new BaseColor(255, 0, 0);
                RMUtitulo.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell telefonoemergencia = new PdfPCell(new Phrase(" Teléfono de emergencia", _headers));
                telefonoemergencia.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                telefonoemergencia.BorderWidth = 0;
                telefonoemergencia.HorizontalAlignment = 1;

                PdfPCell NUMRMU = new PdfPCell(new Phrase(InfoNumeroDeDocumento + " ", _standardFont));

                NUMRMU.BorderWidth = 0;
                NUMRMU.HorizontalAlignment = 0;

                PdfPCell numero8cientos = new PdfPCell(new Phrase("01 8009 266737", _StandardBold));
                numero8cientos.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numero8cientos.BorderWidth = 0;
                numero8cientos.HorizontalAlignment = 1;

                PdfPCell MedicionConsum = new PdfPCell(new Phrase("", _SecundaryBold));
                MedicionConsum.BorderWidth = 0;
                MedicionConsum.HorizontalAlignment = 0;

                PdfPCell cientos8amper = new PdfPCell(new Phrase("   (AMMPER)", _StandardBold));
                cientos8amper.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                cientos8amper.BorderWidth = 0;
                cientos8amper.HorizontalAlignment = 1;




                //Agregar Celdas a Datoscliente
                DatosCliente.AddCell(Dependencia);
                DatosCliente.AddCell(TotalPago);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(periodofact);
                DatosCliente.AddCell(calle);
                DatosCliente.AddCell(coral);
                DatosCliente.AddCell(numerodireccion);
                DatosCliente.AddCell(numerospago);
                DatosCliente.AddCell(codigopostal);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(ciudadfactura);
                DatosCliente.AddCell(RMUENCABEZADO);
                DatosCliente.AddCell(rfcamper);
                DatosCliente.AddCell(RMUARRIBA);
                DatosCliente.AddCell(direccioncliente);
                DatosCliente.AddCell(periodoconsumo);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(fechasperiodo);
                DatosCliente.AddCell(nombreydomicilio);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(taimlimit);
                DatosCliente.AddCell(TherealPlace);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(lafechalimite);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(RMUtitulo);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(telefonoemergencia);
                DatosCliente.AddCell(NUMRMU);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(numero8cientos);
                DatosCliente.AddCell(MedicionConsum);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(cientos8amper);



                //agregar datos 
                doc.Add(DatosCliente);

                //Tabla de medicion de consumo
                PdfPTable DatosdeConsumo = new PdfPTable(8);
                DatosdeConsumo.WidthPercentage = 95;

                //Declaracion de celdas
                //declaracion salto de linea completo
                PdfPCell saltosupermegagenial = new PdfPCell(new Phrase("", _secundaryfont));
                saltosupermegagenial.BorderWidth = 0;
                saltosupermegagenial.Colspan = 8;
                //encabezados
                PdfPCell Nummedidor = new PdfPCell(new Phrase(" No. Medidor ", _SecundaryBold));
                Nummedidor.BorderWidth = 2;
                Nummedidor.HorizontalAlignment = 0;
                Nummedidor.BorderColor = new BaseColor(255, 0, 0);
                Nummedidor.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell Tituloconsumomensual = new PdfPCell(new Phrase(" Consumo Mensual (MWh) ", _SecundaryBold));
                Tituloconsumomensual.BorderWidth = 2;
                Tituloconsumomensual.HorizontalAlignment = Element.ALIGN_CENTER;
                Tituloconsumomensual.BorderColor = new BaseColor(255, 0, 0);
                Tituloconsumomensual.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Tituloconsumomensual.Colspan = 3;

                PdfPCell TarifaSumi = new PdfPCell(new Phrase(" Tarifa de Suministro ($/MWh) ", _SecundaryBold));
                TarifaSumi.BorderWidth = 2;
                TarifaSumi.HorizontalAlignment = Element.ALIGN_CENTER;
                TarifaSumi.BorderColor = new BaseColor(255, 0, 0);
                TarifaSumi.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                TarifaSumi.Colspan = 2;

                PdfPCell Superpotencia100 = new PdfPCell(new Phrase("Potencia 100 horas críticas (MW)", _SecundaryBold));
                Superpotencia100.BorderWidth = 2;
                Superpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                Superpotencia100.BorderColor = new BaseColor(255, 0, 0);
                Superpotencia100.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Superpotencia100.Colspan = 2;

                //Contenido

                //Dejar espacios blancos en medidor

                PdfPCell espaciomedidor = new PdfPCell(new Phrase(" ", _standardFont));
                espaciomedidor.BorderWidth = 0;
                espaciomedidor.HorizontalAlignment = 0;



                //medidor
                PdfPCell cualmedidor = new PdfPCell(new Phrase(" " + MEDIDOR + " ", _Medidor));
                cualmedidor.BorderWidth = 0;
                cualmedidor.HorizontalAlignment = 0;
                cualmedidor.Colspan = 2;

                //consumoneto




                PdfPCell cantidadconsumoneto = new PdfPCell(new Phrase(" " + DatosParaTablaConsumo[0] + " ", _standardMedidor));
                cantidadconsumoneto.BorderWidth = 0;
                cantidadconsumoneto.HorizontalAlignment = 0;
                cantidadconsumoneto.Colspan = 3;

                //Tarifa
                PdfPCell cantidadtarifa = new PdfPCell(new Phrase(DatosParaTablaConsumo[1], _standardMedidor));
                cantidadtarifa.BorderWidth = 0;
                cantidadtarifa.HorizontalAlignment = 0;
                cantidadtarifa.Colspan = 2;



                //potencia super cantidad!
                PdfPCell cantidadpotencia100 = new PdfPCell(new Phrase(DatosParaTablaConsumo[2], _standardMedidor));
                cantidadpotencia100.BorderWidth = 0;
                cantidadpotencia100.HorizontalAlignment = Element.ALIGN_LEFT;
                cantidadpotencia100.Colspan = 2;


                //Agregar celdas a datos consumo

                DatosdeConsumo.AddCell(Nummedidor);
                DatosdeConsumo.AddCell(Tituloconsumomensual);
                DatosdeConsumo.AddCell(TarifaSumi);
                DatosdeConsumo.AddCell(Superpotencia100);
                DatosdeConsumo.AddCell(saltosupermegagenial);
                DatosdeConsumo.AddCell(cualmedidor);
                //DatosdeConsumo.AddCell(consumoneto);
                DatosdeConsumo.AddCell(cantidadconsumoneto);
                DatosdeConsumo.AddCell(cantidadtarifa);
                //DatosdeConsumo.AddCell(conceptotarifa);
                DatosdeConsumo.AddCell(cantidadpotencia100);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(saltosupermegagenial);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(saltosupermegagenial);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(saltosupermegagenial);
                DatosdeConsumo.AddCell(espaciomedidor);
                DatosdeConsumo.AddCell(saltosupermegagenial);

                //AgregardatosConsumo
                doc.Add(DatosdeConsumo);

                doc.Add(Chunk.NEWLINE);

                //tabla Desglosada de resumen
                PdfPTable Resumen = new PdfPTable(14);
                Resumen.WidthPercentage = 100;

                //Espacios Blancos de resumen

                PdfPCell blancosresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosresumen.BorderWidth = 0;
                blancosresumen.HorizontalAlignment = 0;
                blancosresumen.Colspan = 8;

                PdfPCell blancosfinalresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosfinalresumen.BorderWidth = 0;
                blancosfinalresumen.HorizontalAlignment = 0;

                //Crear datos encabezado resumee

                PdfPCell concepto = new PdfPCell(new Phrase("Concepto ", _resumeeheader));
                concepto.BorderWidth = 1;
                concepto.HorizontalAlignment = 0;
                concepto.BorderColor = new BaseColor(255, 0, 0);
                concepto.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                concepto.Colspan = 3;

                PdfPCell tarifa = new PdfPCell(new Phrase(" Precio ("+tipoCambio+"/MWh) ", _resumeeheader)); //DGG 09092020
                tarifa.BorderWidth = 1;
                tarifa.HorizontalAlignment = 1;
                tarifa.BorderColor = new BaseColor(255, 0, 0);
                tarifa.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;


                PdfPCell totaldolarucos = new PdfPCell(new Phrase(" Precio Total (" + tipoCambio + ") ", _resumeeheader));
                totaldolarucos.BorderWidth = 1;
                totaldolarucos.HorizontalAlignment = 1;
                totaldolarucos.BorderColor = new BaseColor(255, 0, 0);
                totaldolarucos.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                //Comparacion de datos resumee Pesos
                for (int a = 0; a < 11; a++)
                {
                    string pesostemporales = DatosPesosMexicanos[a];
                    if (DatosPesosMexicanos[a] != "")
                    {
                        if (DatosPesosMexicanos[a].Substring(0, 1) == "-")
                        {
                            DatosPesosMexicanos[a] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            DatosPesosMexicanos[a] = " $" + pesostemporales;

                        }
                    }
                }
                //Comparacion de datos Resume Dolarucos
                for (int a = 0; a < 14; a++)
                {
                    string pesostemporales = DatosDonalds[a];
                    if (DatosDonalds[a] != "")
                    {
                        if (DatosDonalds[a].Substring(0, 1) == "-")
                        {
                            DatosDonalds[a] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            DatosDonalds[a] = " $" + pesostemporales;

                        }
                    }
                    //Creardatos resumee
                }

                PdfPCell ENERGIAMEM = new PdfPCell(new Phrase("ENERGÍA MEM ", _resumee));
                ENERGIAMEM.BorderWidth = 0;
                ENERGIAMEM.HorizontalAlignment = 0;
                ENERGIAMEM.Colspan = 3;

                PdfPCell EMT = new PdfPCell(new Phrase(DatosPesosMexicanos[0], _resumee));
                EMT.BorderWidth = 0;
                EMT.HorizontalAlignment = 2;

                PdfPCell EMUSD = new PdfPCell(new Phrase(DatosDonalds[0], _resumee));
                EMUSD.BorderWidth = 0;
                EMUSD.HorizontalAlignment = 2;
                //ENERGIA MEM

                PdfPCell ENERGIAVENDIDA = new PdfPCell(new Phrase("ENERGÍA VENDIDA", _resumee));
                ENERGIAVENDIDA.BorderWidth = 0;
                ENERGIAVENDIDA.HorizontalAlignment = 0;
                ENERGIAVENDIDA.Colspan = 3;

                PdfPCell EVT = new PdfPCell(new Phrase(DatosPesosMexicanos[1], _resumee));
                EVT.BorderWidth = 0;
                EVT.HorizontalAlignment = 2;

                PdfPCell EVUSD = new PdfPCell(new Phrase(DatosDonalds[1], _resumee));
                EVUSD.BorderWidth = 0;
                EVUSD.HorizontalAlignment = 2;
                //ENERGIA VENDIDA






                PdfPCell PERDIDASDELARED = new PdfPCell(new Phrase("PÉRDIDAS DE LA RED", _resumee));
                PERDIDASDELARED.BorderWidth = 0;
                PERDIDASDELARED.HorizontalAlignment = 0;
                PERDIDASDELARED.Colspan = 3;

                PdfPCell PDLRT = new PdfPCell(new Phrase(DatosPesosMexicanos[2], _resumee));
                PDLRT.BorderWidth = 0;
                PDLRT.HorizontalAlignment = 2;

                PdfPCell PDLRUSD = new PdfPCell(new Phrase(DatosDonalds[2], _resumee));
                PDLRUSD.BorderWidth = 0;
                PDLRUSD.HorizontalAlignment = 2;
                //PERDIDAS DE LA RED

                PdfPCell SERVICIOSCONEXOS = new PdfPCell(new Phrase("SERVICIOS CONEXOS", _resumee));
                SERVICIOSCONEXOS.BorderWidth = 0;
                SERVICIOSCONEXOS.HorizontalAlignment = 0;
                SERVICIOSCONEXOS.Colspan = 3;

                PdfPCell SCT = new PdfPCell(new Phrase(DatosPesosMexicanos[3], _resumee));
                SCT.BorderWidth = 0;
                SCT.HorizontalAlignment = 2;

                PdfPCell SCUSD = new PdfPCell(new Phrase(DatosDonalds[3], _resumee));
                SCUSD.BorderWidth = 0;
                SCUSD.HorizontalAlignment = 2;
                //SERVICIOS CONEXOS

                PdfPCell COSTOOPERACIONCENACE = new PdfPCell(new Phrase("COSTO DE OPERACIÓN DE CENACE ", _resumee));
                COSTOOPERACIONCENACE.BorderWidth = 0;
                COSTOOPERACIONCENACE.HorizontalAlignment = 0;
                COSTOOPERACIONCENACE.Colspan = 3;

                PdfPCell COCCT = new PdfPCell(new Phrase(DatosPesosMexicanos[4], _resumee));
                COCCT.BorderWidth = 0;
                COCCT.HorizontalAlignment = 2;

                PdfPCell COCCUSD = new PdfPCell(new Phrase(DatosDonalds[4], _resumee));
                COCCUSD.BorderWidth = 0;
                COCCUSD.HorizontalAlignment = 2;
                //COSTO DE OPERACION DEL CENACE CARGA



                PdfPCell GARANTIASUFICIENCIA = new PdfPCell(new Phrase("GARANTÍA DE SUFICIENCIA", _resumee));
                GARANTIASUFICIENCIA.BorderWidth = 0;
                GARANTIASUFICIENCIA.HorizontalAlignment = 0;
                GARANTIASUFICIENCIA.Colspan = 3;

                PdfPCell GDST = new PdfPCell(new Phrase(DatosPesosMexicanos[5], _resumee));
                GDST.BorderWidth = 0;
                GDST.HorizontalAlignment = 2;

                PdfPCell GDSUSD = new PdfPCell(new Phrase(DatosDonalds[5], _resumee));
                GDSUSD.BorderWidth = 0;
                GDSUSD.HorizontalAlignment = 2;
                //GARANTIA DE SUFICIENCIA


                PdfPCell TRANSMISIÓNCARGA = new PdfPCell(new Phrase("TRANSMISIÓN ", _resumee));
                TRANSMISIÓNCARGA.BorderWidth = 0;
                TRANSMISIÓNCARGA.HorizontalAlignment = 0;
                TRANSMISIÓNCARGA.Colspan = 3;

                PdfPCell TCT = new PdfPCell(new Phrase(DatosPesosMexicanos[6], _resumee));
                TCT.BorderWidth = 0;
                TCT.HorizontalAlignment = 2;

                PdfPCell TCUSD = new PdfPCell(new Phrase(DatosDonalds[6], _resumee));
                TCUSD.BorderWidth = 0;
                TCUSD.HorizontalAlignment = 2;
                //TRANSMISIÓN CARGA



                PdfPCell CONGESTIONMEM = new PdfPCell(new Phrase("CONGESTIÓN ", _resumee));
                CONGESTIONMEM.BorderWidth = 0;
                CONGESTIONMEM.HorizontalAlignment = 0;
                CONGESTIONMEM.Colspan = 3;

                PdfPCell CMT = new PdfPCell(new Phrase(DatosPesosMexicanos[7], _resumee));
                CMT.BorderWidth = 0;
                CMT.HorizontalAlignment = 2;

                PdfPCell CMUSD = new PdfPCell(new Phrase(DatosDonalds[7], _resumee));
                CMUSD.BorderWidth = 0;
                CMUSD.HorizontalAlignment = 2;
                //CONGESTION MEM


                PdfPCell RELIQUIDACIONES = new PdfPCell(new Phrase("RELIQUIDACIONES", _resumee));
                RELIQUIDACIONES.BorderWidth = 0;
                RELIQUIDACIONES.HorizontalAlignment = 0;
                RELIQUIDACIONES.Colspan = 3;

                PdfPCell RLT = new PdfPCell(new Phrase(DatosPesosMexicanos[8], _resumee));
                RLT.BorderWidth = 0;
                RLT.HorizontalAlignment = 2;

                PdfPCell RLUSD = new PdfPCell(new Phrase(DatosDonalds[8], _resumee));
                RLUSD.BorderWidth = 0;
                RLUSD.HorizontalAlignment = 2;
                //RELIQUIDACIONES

                PdfPCell OTROSCARGOSYABONOS = new PdfPCell(new Phrase("OTROS CARGOS Y/O ABONOS", _resumee));
                OTROSCARGOSYABONOS.BorderWidth = 0;
                OTROSCARGOSYABONOS.HorizontalAlignment = 0;
                OTROSCARGOSYABONOS.Colspan = 3;

                PdfPCell OCYAT = new PdfPCell(new Phrase(DatosPesosMexicanos[9], _resumee));
                OCYAT.BorderWidth = 0;
                OCYAT.HorizontalAlignment = 2;

                PdfPCell OCYAUSD = new PdfPCell(new Phrase(DatosDonalds[9], _resumee));
                OCYAUSD.BorderWidth = 0;
                OCYAUSD.HorizontalAlignment = 2;
                // OTROS CARGOS Y ABONOS

                PdfPCell AMMPER = new PdfPCell(new Phrase("BALANCE ANUAL DEL MBP " + DateTime.Now.AddYears(-1).Year.ToString(), _resumee));
                AMMPER.BorderWidth = 0;
                AMMPER.HorizontalAlignment = 0;
                AMMPER.Colspan = 3;

                PdfPCell AMMT = new PdfPCell(new Phrase(DatosPesosMexicanos[10], _resumee));
                AMMT.BorderWidth = 0;
                AMMT.HorizontalAlignment = 2;

                PdfPCell AMMUSD = new PdfPCell(new Phrase(DatosDonalds[10], _resumee));
                AMMUSD.BorderWidth = 0;
                AMMUSD.HorizontalAlignment = 2;
                // BALANCE ANUAL DL BP

                PdfPCell SUBTOTALRESUMEE = new PdfPCell(new Phrase("SUBTOTAL", _resumee));
                SUBTOTALRESUMEE.BorderWidth = 0;
                SUBTOTALRESUMEE.HorizontalAlignment = 0;
                SUBTOTALRESUMEE.Colspan = 3;


                PdfPCell SUBTRUSD = new PdfPCell(new Phrase(DatosDonalds[11], _resumee));
                SUBTRUSD.BorderWidth = 0;
                SUBTRUSD.HorizontalAlignment = 2;
                // subtotal RESUMEE

                PdfPCell IVARESUMEE = new PdfPCell(new Phrase("IVA", _resumee));
                IVARESUMEE.BorderWidth = 0;
                IVARESUMEE.HorizontalAlignment = 0;
                IVARESUMEE.Colspan = 3;


                PdfPCell IVARUSD = new PdfPCell(new Phrase(DatosDonalds[12], _resumee));
                IVARUSD.BorderWidth = 0;
                IVARUSD.HorizontalAlignment = 2;
                // IVA

                PdfPCell TOTALRESUMEE = new PdfPCell(new Phrase("TOTAL", _resumee));
                TOTALRESUMEE.BorderWidth = 0;
                TOTALRESUMEE.HorizontalAlignment = 0;
                TOTALRESUMEE.Colspan = 3;


                PdfPCell TOTALRUSD = new PdfPCell(new Phrase(DatosDonalds[13], _resumee));
                TOTALRUSD.BorderWidth = 0;
                TOTALRUSD.HorizontalAlignment = 2;
                // TOTAL RESUMEE


                //Agregar celdas a datos consumo
                //INICIO ENCABEZADO RESUMEN
                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(concepto);
                Resumen.AddCell(tarifa);
                Resumen.AddCell(totaldolarucos);
                Resumen.AddCell(blancosfinalresumen);
                //FIN ENCABEZADO RESUMEN

                if (DatosPesosMexicanos[0] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIAMEM);
                    Resumen.AddCell(EMT);
                    Resumen.AddCell(EMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[1] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(ENERGIAVENDIDA);
                    Resumen.AddCell(EVT);
                    Resumen.AddCell(EVUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[2] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(PERDIDASDELARED);
                    Resumen.AddCell(PDLRT);
                    Resumen.AddCell(PDLRUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[3] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(SERVICIOSCONEXOS);
                    Resumen.AddCell(SCT);
                    Resumen.AddCell(SCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[4] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(COSTOOPERACIONCENACE);
                    Resumen.AddCell(COCCT);
                    Resumen.AddCell(COCCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[5] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(GARANTIASUFICIENCIA);
                    Resumen.AddCell(GDST);
                    Resumen.AddCell(GDSUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[6] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(TRANSMISIÓNCARGA);
                    Resumen.AddCell(TCT);
                    Resumen.AddCell(TCUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[7] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(CONGESTIONMEM);
                    Resumen.AddCell(CMT);
                    Resumen.AddCell(CMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[8] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(RELIQUIDACIONES);
                    Resumen.AddCell(RLT);
                    Resumen.AddCell(RLUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[9] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(OTROSCARGOSYABONOS);
                    Resumen.AddCell(OCYAT);
                    Resumen.AddCell(OCYAUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }

                if (DatosPesosMexicanos[10] != "")
                {
                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(AMMPER);
                    Resumen.AddCell(AMMT);
                    Resumen.AddCell(AMMUSD);
                    Resumen.AddCell(blancosfinalresumen);
                }


                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(SUBTOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(SUBTRUSD);
                Resumen.AddCell(blancosfinalresumen);



                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(IVARESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(IVARUSD);
                Resumen.AddCell(blancosfinalresumen);



                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TOTALRUSD);
                Resumen.AddCell(blancosfinalresumen);



                //Agregardatosresumen
                doc.Add(Resumen);



                //tabla de paginacion
                //PdfPTable Paginacion = new PdfPTable(1);
                //Paginacion.WidthPercentage = 95;
                //Paginacion.SetAbsolutePosition(0,10);

                doc.Close();
                writer.Close();
                Status = "Proceso exitoso, terminado a las:" + DateTime.Today.ToLongTimeString();
                UpdateLogFile(Status);
                Byte[] bytespdf = File.ReadAllBytes(Path);
                StringConvertedToBase64 = Convert.ToBase64String(bytespdf);
            }
            catch (Exception ex)
            {
                Status = "Hubo un error a las " + DateTime.Today.ToLongTimeString() + ex;
                UpdateLogFile(Status);
            }

        }

        static void DeleteFiles()
        {

            if (Directory.Exists(@"c:\PruebasPDF\Resources"))
            {
                Directory.Delete(@"c:\PruebasPDF\Resources", true);//cleanup created folder which has any content inside it.
                //true ensures that folder is deleted even if it is not empty. 

            }

        }
        static void UpdateLogFile(string Status)
        {
            string path = @"c:\PruebasPDF\LogFile.txt";//Donde guardar el PDF y los archivos necesarios
            if (File.Exists(path))
            {
                // Create a file to write to.
                string archivo = File.ReadAllText(path);
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(archivo + Status);

                }
            }
            else
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(Status);

                }

            }

        }
        static void CreateExcelData(string info)
        {
            string path = @"c:\PruebasPDF\Resources\PDFGrap" + GraphicExcelVersion + ".xlsx";
            string fecha = info.Substring(0, info.IndexOf("|"));

            DateTime superdate = DateTime.Parse(fecha);
            string workingdata = info.Substring(info.IndexOf("|") + 1);

            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2013;
                Syncfusion.XlsIO.IWorkbook workbook = application.Workbooks.Open(path);
                IWorksheet sheet = workbook.Worksheets[0];
                //Fill CELLS ABOUT A AND B
                string CasillaA = "A", CasillaB = "B";
                string pattern = "-";
                string[] mwhInfo = System.Text.RegularExpressions.Regex.Split(workingdata, pattern);
                int u = 3;
                foreach (var INFOMWH in mwhInfo)
                {

                    //Specify cell address FECHAS
                    sheet.Range[CasillaA + u].Text = superdate.ToString("d");
                    //Specify Cell address MWh Consumido
                    sheet.Range[CasillaB + u].Number = Convert.ToDouble(INFOMWH);
                    superdate = superdate.AddDays(+1);
                    u++;
                }


                ////Access a range by specifying cell address
                //sheet.Range["A7"].Text = "Accessing a Range by specify cell address ";

                ////Access a range by specifying cell row and column index
                //sheet.Range[9, 1].Text = "Accessing a Range by specify cell row and column index ";

                ////Access a Range by specifying using defined name
                //Syncfusion.XlsIO.IName name = workbook.Names.Add("Name");
                //name.RefersToRange = sheet.Range["A11"];
                //sheet.Range["Name"].Text = "Accessing a Range by specifying using defined name.";

                ////Accessing a Range of cells by specifying cells address
                //sheet.Range["A13:C13"].Text = "Accessing a Range of Cells (Method 1)";

                ////Accessing a Range of cells specifying cell row and column index
                //sheet.Range[15, 1, 15, 3].Text = "Accessing a Range of Cells (Method 2)";

                workbook.SaveAs(path);
            }


        }

        static void CreateExcelData2(string info)
        {
            string path = @"c:\PruebasPDF\Resources\PDFGrap" + GraphicExcelVersion + ".xlsx";
            string fecha = info.Substring(0, info.IndexOf("|"));

            DateTime superdate = DateTime.Parse(fecha);
            string workingdata = info.Substring(info.IndexOf("|") + 1);
            string consumo = workingdata.Substring(0, workingdata.IndexOf("|"));
            string contratado = workingdata.Substring(workingdata.IndexOf("|") + 1);

            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2013;
                Syncfusion.XlsIO.IWorkbook workbook = application.Workbooks.Open(path);
                IWorksheet sheet = workbook.Worksheets[0];
                //Fill CELLS ABOUT A AND B
                string CasillaA = "A", CasillaB = "B", CasillaC = "C";
                string pattern = "-";
                string[] mwhInfo = System.Text.RegularExpressions.Regex.Split(consumo, pattern);
                int u = 3;
                foreach (var INFOMWH in mwhInfo)
                {

                    //Specify cell address FECHAS
                    sheet.Range[CasillaA + u].Text = superdate.ToString("d");
                    //Specify Cell address MWh Consumido
                    sheet.Range[CasillaB + u].Number = Convert.ToDouble(INFOMWH);
                    superdate = superdate.AddDays(+1);
                    u++;
                }
                int s = 3;
                string[] mwhContract = System.Text.RegularExpressions.Regex.Split(contratado, pattern);
                foreach (var ContractMWH in mwhContract)
                {

                    //Specify Cell address MWh Contratado
                    sheet.Range[CasillaC + s].Number = Convert.ToDouble(ContractMWH);

                    s++;
                }

                ////Access a range by specifying cell address
                //sheet.Range["A7"].Text = "Accessing a Range by specify cell address ";

                ////Access a range by specifying cell row and column index
                //sheet.Range[9, 1].Text = "Accessing a Range by specify cell row and column index ";

                ////Access a Range by specifying using defined name
                //Syncfusion.XlsIO.IName name = workbook.Names.Add("Name");
                //name.RefersToRange = sheet.Range["A11"];
                //sheet.Range["Name"].Text = "Accessing a Range by specifying using defined name.";

                ////Accessing a Range of cells by specifying cells address
                //sheet.Range["A13:C13"].Text = "Accessing a Range of Cells (Method 1)";

                ////Accessing a Range of cells specifying cell row and column index
                //sheet.Range[15, 1, 15, 3].Text = "Accessing a Range of Cells (Method 2)";

                workbook.SaveAs(path);
            }



        }

        static void CreateQRImage(string InfoForQR, string Etc)
        {
            QRCodeEncoder generarCodigoQR = new QRCodeEncoder();
            generarCodigoQR.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;

            generarCodigoQR.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;

            generarCodigoQR.QRCodeVersion = 0;

            generarCodigoQR.QRCodeBackgroundColor = System.Drawing.Color.FromArgb(-1);
            generarCodigoQR.QRCodeForegroundColor = System.Drawing.Color.FromArgb(-16777216);
            string id = InfoForQR.Substring(0, InfoForQR.IndexOf(","));
            string temp1 = InfoForQR.Substring(InfoForQR.IndexOf(",") + 1);
            string re = temp1.Substring(0, temp1.IndexOf(","));
            string rr = temp1.Substring(temp1.IndexOf(",") + 1);



            string ULTIMOS8 = Etc;
            string xresult = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=" + id + "&re=" + re + "&rr=" + rr + "&tt=0&fe=" + ULTIMOS8;
            //string tbase64 = GetStringFromImage(generarCodigoQR.Encode(xresult, System.Text.Encoding.UTF8));
            String imaqrcode = @"c:\PruebasPDF\Resources\qr.png";

            generarCodigoQR.Encode(xresult, System.Text.Encoding.UTF8).Save(imaqrcode);
            generarCodigoQR.Encode(xresult, System.Text.Encoding.UTF8).Dispose();


        }

        static void ImageCFDI4QR(string CFDItext, string SATtext, string verdaderotext,string moneda,string extras)
        {
            Bitmap newBitmap = null;
            Graphics g = null;

            // obtenerdatosfolio
            string datosfolio = extras.Substring(0, extras.IndexOf("|"));

            //obtener datosformato
            string datosformato = extras.Substring(extras.IndexOf("|") + 1);
            CrearEstampadosNuevaImplementacion Estampados = new CrearEstampadosNuevaImplementacion();
            Estampados.CrearFolio(verdaderotext,datosfolio);
            Estampados.CrearFormatoPago(datosformato,moneda);
            Estampados.CrearGastosGenerales();
            Estampados.CrearFiscal();

            try
            {
                System.Drawing.Font fontCounter = new System.Drawing.Font("Lucida Sans Unicode", 12);
                System.Drawing.Font Negritas = new System.Drawing.Font("Lucida Sans Unicode", 12, FontStyle.Bold);
                //Calculate the side of the text
                int cantidad = CFDItext.Length;
                int supercantidad = SATtext.Length;
                int FontSizeIneed = cantidad / 7; // /4

                int cantida2 = verdaderotext.Length;
                int FontSizeIneed2 = cantida2 / 9;
                int FontSizeIneedF = 50;

                //DIVIDIR CFDI
                string PrimerRenglon = CFDItext.Substring(0, FontSizeIneed);
                string SegundoRenglon = CFDItext.Substring(FontSizeIneed, FontSizeIneed);
                string TercerRenglon = CFDItext.Substring(FontSizeIneed * 2, FontSizeIneed);
                string CuartoRenglon = CFDItext.Substring(3 * FontSizeIneed, FontSizeIneed);
                string QuintoRenglon = CFDItext.Substring(4 * FontSizeIneed, FontSizeIneed); //14-09-2020
                string SextoRenglon = CFDItext.Substring(5 * FontSizeIneed, FontSizeIneed);
                string SeptimoRenglon = CFDItext.Substring(6 * FontSizeIneed, FontSizeIneed);


                //DIVIDIR SAT
                string PrimerRenglonSAT = SATtext.Substring(0, FontSizeIneed);
                string SegundoRenglonSAT = SATtext.Substring(FontSizeIneed, FontSizeIneed);
                string TercerRenglonSAT = SATtext.Substring((FontSizeIneed * 2), FontSizeIneed);
                string CuartoRenglonSAT = SATtext.Substring(3 * FontSizeIneed, FontSizeIneed);
                string QuintoRenglonSAT = SATtext.Substring(4 * FontSizeIneed, FontSizeIneed); //14-09-2020
                string SextoRenglonSAT = SATtext.Substring(5 * FontSizeIneed, FontSizeIneed);
                string SeptimoRenglonSAT = SATtext.Substring(6 * FontSizeIneed, FontSizeIneed);

                //DIVIDIR STRING LARGO
                //string PrimerRenglonVERDADERO = verdaderotext.Substring(0, FontSizeIneed);
                //string SegundoRenglonVERDADERO = verdaderotext.Substring(FontSizeIneed, FontSizeIneed);
                //string TercerRenglonVERDADERO = verdaderotext.Substring((FontSizeIneed * 2), FontSizeIneed);
                //string CuartoRenglonVERDADERO = verdaderotext.Substring(3 * FontSizeIneed, FontSizeIneed);
                string PrimerRenglonVERDADERO = verdaderotext.Substring(0, FontSizeIneed2);
                string SegundoRenglonVERDADERO = verdaderotext.Substring(FontSizeIneed2, FontSizeIneed2);
                string TercerRenglonVERDADERO = verdaderotext.Substring((FontSizeIneed2 * 2), FontSizeIneed2);
                string CuartoRenglonVERDADERO = verdaderotext.Substring(3 * FontSizeIneed2, FontSizeIneed2);
                string QuintoRenglonVERDADERO = verdaderotext.Substring(4 * FontSizeIneed2, FontSizeIneed2);
                string SextoRenglonVERDADERO = verdaderotext.Substring(5 * FontSizeIneed2, FontSizeIneed2);
                string SeptimoRenglonVERDADERO = verdaderotext.Substring(6 * FontSizeIneed2, FontSizeIneed2);
                string OctavoRenglonVERDADERO = verdaderotext.Substring(7 * FontSizeIneed2, FontSizeIneed2);
                string NovenoRenglonVERDADERO = verdaderotext.Substring(8 * FontSizeIneed2, FontSizeIneed2);
                // calculate size of the string.
                newBitmap = new Bitmap(1520, 720, PixelFormat.Format32bppArgb);
                g = Graphics.FromImage(newBitmap);
                SizeF stringSize = g.MeasureString(PrimerRenglon, fontCounter);
                //int nWidth = (int)stringSize.Width;
                int nWidth = 555; //508
                int nHeight = (int)stringSize.Height;
                g.Dispose();
                newBitmap.Dispose();

                newBitmap = new Bitmap(nWidth, 570, PixelFormat.Format32bppArgb); //350
                g = Graphics.FromImage(newBitmap);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 0, nWidth, nHeight));
                g.DrawString("Sello Digital del CFDI", Negritas, new SolidBrush(Color.Black), 0, 0);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 20, nWidth, nHeight));
                g.DrawString(PrimerRenglon, fontCounter, new SolidBrush(Color.Black), 0, 20);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 40, nWidth, nHeight));
                g.DrawString(SegundoRenglon, fontCounter, new SolidBrush(Color.Black), 0, 40);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 60, nWidth, nHeight));
                g.DrawString(TercerRenglon, fontCounter, new SolidBrush(Color.Black), 0, 60);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 80, nWidth, nHeight));
                g.DrawString(CuartoRenglon, fontCounter, new SolidBrush(Color.Black), 0, 80);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 100, nWidth, nHeight));
                g.DrawString(QuintoRenglon, fontCounter, new SolidBrush(Color.Black), 0, 100);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 120, nWidth, nHeight));
                g.DrawString(SextoRenglon, fontCounter, new SolidBrush(Color.Black), 0, 120);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 140, nWidth, nHeight));
                g.DrawString(SeptimoRenglon, fontCounter, new SolidBrush(Color.Black), 0, 140);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 160, nWidth, nHeight));
                //
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 180, nWidth, nHeight));
                g.DrawString("Sello Digital del SAT", Negritas, new SolidBrush(Color.Black), 0, 180);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 200, nWidth, nHeight));
                g.DrawString(PrimerRenglonSAT, fontCounter, new SolidBrush(Color.Black), 0, 200);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 220, nWidth, nHeight));
                g.DrawString(SegundoRenglonSAT, fontCounter, new SolidBrush(Color.Black), 0, 220);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 240, nWidth, nHeight));
                g.DrawString(TercerRenglonSAT, fontCounter, new SolidBrush(Color.Black), 0, 240);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 260, nWidth, nHeight));
                g.DrawString(CuartoRenglonSAT, fontCounter, new SolidBrush(Color.Black), 0, 260);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 280, nWidth, nHeight));
                g.DrawString(QuintoRenglonSAT, fontCounter, new SolidBrush(Color.Black), 0, 280);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 300, nWidth, nHeight));
                g.DrawString(SextoRenglonSAT, fontCounter, new SolidBrush(Color.Black), 0, 300);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 320, nWidth, nHeight));
                g.DrawString(SeptimoRenglonSAT, fontCounter, new SolidBrush(Color.Black), 0, 320);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 340, nWidth, nHeight));

                //
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 360, nWidth, nHeight));
                g.DrawString("Cadena Original del complemento de certificación del SAT", Negritas, new SolidBrush(Color.Black), 0, 360);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 380, nWidth, nHeight));
                g.DrawString(PrimerRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 380);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 400, nWidth, nHeight));
                g.DrawString(SegundoRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 400);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 420, nWidth, nHeight));
                g.DrawString(TercerRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 420);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 440, nWidth, nHeight));
                g.DrawString(CuartoRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 440);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 460, nWidth, nHeight));
                g.DrawString(QuintoRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 460);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 480, nWidth, nHeight));
                g.DrawString(SextoRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 480);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 500, nWidth, nHeight));
                g.DrawString(SeptimoRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 500);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 520, nWidth, nHeight));
                g.DrawString(OctavoRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 520);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 540, nWidth, nHeight));
                g.DrawString(NovenoRenglonVERDADERO, fontCounter, new SolidBrush(Color.Black), 0, 540);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 560, nWidth, nHeight));

                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 580, nWidth, nHeight));
                newBitmap.Save(@"C:\PruebasPDF\Resources\test.png", ImageFormat.Png);

                

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (null != g) g.Dispose();
                if (null != newBitmap) newBitmap.Dispose();
            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace PDF_Ammper
{
    public class CrearEstampadosNuevaImplementacion
    {
        public CrearEstampadosNuevaImplementacion()
        {
            
        }
        public string separador = "/";
        public void CrearFolio(string Constructor,string listaElementos)
        {
            //obtener lista
            
            String[] elementos = System.Text.RegularExpressions.Regex.Split(listaElementos,separador);

            Bitmap newBitmap = null;
            Graphics g = null;
            string folio = Constructor.Substring(6, 36);
            string fechayhora = Constructor.Substring(43, 19);
            try
            {
                System.Drawing.Font fontCounter = new System.Drawing.Font("Lucida Sans Unicode", 14);
                System.Drawing.Font Negritas = new System.Drawing.Font("Lucida Sans Unicode", 12, FontStyle.Bold);
                
                newBitmap = new Bitmap(1520, 360, PixelFormat.Format32bppArgb);
                g = Graphics.FromImage(newBitmap);
                //int PrimerRenglon = 38;
                //SizeF stringSize = g.MeasureString(PrimerRenglon, fontCounter);
                ////int nWidth = (int)stringSize.Width;
                int nWidth = 555; //508
                int nHeight = 55;
                g.Dispose();
                newBitmap.Dispose();

                newBitmap = new Bitmap(nWidth, 290, PixelFormat.Format32bppArgb); //350
                g = Graphics.FromImage(newBitmap);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 0, nWidth, nHeight));
                g.DrawString("Folio Fiscal (UUID):", fontCounter, new SolidBrush(Color.Black), 0, 0);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 20, nWidth, nHeight));
                g.DrawString($"{folio} ", fontCounter, new SolidBrush(Color.Black), 0, 20);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 40, nWidth, nHeight));
                g.DrawString("No. Serie  del certificado del SAT:", fontCounter, new SolidBrush(Color.Black), 0, 40);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 60, nWidth, nHeight));
                g.DrawString($"{elementos[0]}", fontCounter, new SolidBrush(Color.Black), 0, 60);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 80, nWidth, nHeight));
                g.DrawString("No. de serie del certificado del emisor:", fontCounter, new SolidBrush(Color.Black), 0, 80);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 100, nWidth, nHeight));
                g.DrawString($"{elementos[1]}", fontCounter, new SolidBrush(Color.Black), 0, 100);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 120, nWidth, nHeight));
                g.DrawString("Fecha y hora de certificación:", fontCounter, new SolidBrush(Color.Black), 0, 120);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 140, nWidth, nHeight));
                g.DrawString($"{elementos[2]}", fontCounter, new SolidBrush(Color.Black), 0, 140);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 160, nWidth, nHeight));
                //
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 180, nWidth, nHeight));
                g.DrawString("Tipo de comprobante:", Negritas, new SolidBrush(Color.Black), 0, 180);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 200, nWidth, nHeight));
                g.DrawString($"{elementos[3]}", fontCounter, new SolidBrush(Color.Black), 0, 200);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 220, nWidth, nHeight));
                newBitmap.Save(@"C:\PruebasPDF\Resources\FolioFiscalResumen.png", ImageFormat.Png);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (null != g) g.Dispose();
                if (null != newBitmap) newBitmap.Dispose();
            }

        }

    
            
        
        public void CrearFormatoPago(string listapordefinir,string moneda)
        {
            
            String[] definidos = System.Text.RegularExpressions.Regex.Split(listapordefinir, separador);

            Bitmap newBitmap = null;
            Graphics g = null;

            try
            {
                System.Drawing.Font fontCounter = new System.Drawing.Font("Lucida Sans Unicode", 12);
                System.Drawing.Font Negritas = new System.Drawing.Font("Lucida Sans Unicode", 12, FontStyle.Bold);

                newBitmap = new Bitmap(1520,10, PixelFormat.Format32bppArgb);
                g = Graphics.FromImage(newBitmap);
                //int PrimerRenglon = 38;
                //SizeF stringSize = g.MeasureString(PrimerRenglon, fontCounter);
                ////int nWidth = (int)stringSize.Width;
                int nWidth = 555; //508
                int nHeight = 80;
                g.Dispose();
                newBitmap.Dispose();

                newBitmap = new Bitmap(nWidth, nHeight, PixelFormat.Format32bppArgb); //350
                g = Graphics.FromImage(newBitmap);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 0, nWidth, nHeight));
                g.DrawString($"Moneda :           {moneda}", fontCounter, new SolidBrush(Color.Black), 0, 0);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 20, nWidth, nHeight));
                g.DrawString($"Forma de pago :  {definidos[0]}", fontCounter, new SolidBrush(Color.Black), 0, 20);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 40, nWidth, nHeight));
                g.DrawString($"Método de pago : {definidos[1]}", fontCounter, new SolidBrush(Color.Black), 0, 40);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 60, nWidth, nHeight));
               
                newBitmap.Save(@"C:\PruebasPDF\Resources\ValoresADefinir.png", ImageFormat.Png);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (null != g) g.Dispose();
                if (null != newBitmap) newBitmap.Dispose();
            }

        }
        public void CrearFiscal()
        {
            Bitmap newBitmap = null;
            Graphics g = null;

            try
            {
                System.Drawing.Font fontCounter = new System.Drawing.Font("Lucida Sans Unicode", 12);
                System.Drawing.Font Negritas = new System.Drawing.Font("Lucida Sans Unicode", 12, FontStyle.Bold);

                newBitmap = new Bitmap(1520, 720, PixelFormat.Format32bppArgb);
                g = Graphics.FromImage(newBitmap);
                //int PrimerRenglon = 38;
                //SizeF stringSize = g.MeasureString(PrimerRenglon, fontCounter);
                ////int nWidth = (int)stringSize.Width;
                int nWidth = 555; //508
                int nHeight = 38;
                g.Dispose();
                newBitmap.Dispose();

                newBitmap = new Bitmap(nWidth, nHeight, PixelFormat.Format32bppArgb); //350
                g = Graphics.FromImage(newBitmap);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 0, nWidth, nHeight));
                g.DrawString("Régimen Fiscal 601-General de Ley Personas Morales", fontCounter, new SolidBrush(Color.Black), 0, 0);
                

                newBitmap.Save(@"C:\PruebasPDF\Resources\FolioFiscal.png", ImageFormat.Png);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (null != g) g.Dispose();
                if (null != newBitmap) newBitmap.Dispose();
            }

        }
        public void CrearGastosGenerales()
        {
            Bitmap newBitmap = null;
            Graphics g = null;

            try
            {
                System.Drawing.Font fontCounter = new System.Drawing.Font("Lucida Sans Unicode", 12);
                System.Drawing.Font Negritas = new System.Drawing.Font("Lucida Sans Unicode", 12, FontStyle.Bold);

                newBitmap = new Bitmap(1520, 720, PixelFormat.Format32bppArgb);
                g = Graphics.FromImage(newBitmap);
                //int PrimerRenglon = 38;
                //SizeF stringSize = g.MeasureString(PrimerRenglon, fontCounter);
                ////int nWidth = (int)stringSize.Width;
                int nWidth = 555; //508
                int nHeight = 38;
                g.Dispose();
                newBitmap.Dispose();

                newBitmap = new Bitmap(nWidth, nHeight, PixelFormat.Format32bppArgb); //350
                g = Graphics.FromImage(newBitmap);
                g.FillRectangle(new SolidBrush(Color.White), new System.Drawing.Rectangle(0, 0, nWidth, nHeight));
                g.DrawString("Uso CFDI: Gastos en general", fontCounter, new SolidBrush(Color.Black), 0, 0);


                newBitmap.Save(@"C:\PruebasPDF\Resources\GastosGenerales.png", ImageFormat.Png);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (null != g) g.Dispose();
                if (null != newBitmap) newBitmap.Dispose();
            }

        }
    }
}
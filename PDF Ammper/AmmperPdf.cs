﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Syncfusion.ExcelChartToImageConverter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ThoughtWorks.QRCode.Codec;

namespace PDF_Ammper
{
    public class AmmperPdf
    {
        public AmmperPdf()
        {
            CopyDataResourcesToWork();




        }
        public string ObtenerBase64(string Grafica, string tipoCambio, string factura, string logotipo, string DataResumen, string Etc, string Conceptos, string Pesos, string dolares, string ConceptosResumen,string UsoCfi)
        {

            return ConstruirPDF(Grafica,tipoCambio,factura,logotipo,DataResumen,Etc,Conceptos,Pesos,dolares,ConceptosResumen,UsoCfi);
        }
        static string ConstruirPDF(string Grafica, string tipoCambio, string factura, string logotipo, string DataResumen, string Etc, string Conceptos, string Pesos, string dolares, string ConceptosResumen,string UsoCFI)

        {
            string Status;
            string info = Grafica;
            string InfoForQR = factura.Substring(0, factura.IndexOf("|"));
            string workingInfo = factura.Substring(factura.IndexOf("|") + 1);
            InsertDataToGraphic(info);
            

            CreateQRImage(InfoForQR, Etc);
            

            //declaracion y extraccion de datos
            string catchPhrase = workingInfo.Substring(0, workingInfo.IndexOf("|")); //frase que se debe dividir en 2 renglones
            string temporalsincatch = workingInfo.Substring(workingInfo.IndexOf("|") + 1);
            string InfoTotalAPagar = temporalsincatch.Substring(0, temporalsincatch.IndexOf("|")); // total a pagar
            string temporalsintotalbill = temporalsincatch.Substring(temporalsincatch.IndexOf("|") + 1);
            string InfoPeriodoConsumo = temporalsintotalbill.Substring(0, temporalsintotalbill.IndexOf("|")); //periodo de consumo plaintext
            string temporalsinPeriodoConsumo = temporalsintotalbill.Substring(temporalsintotalbill.IndexOf("|") + 1);
            string InfoFechaLimiteBill = temporalsinPeriodoConsumo.Substring(0, temporalsinPeriodoConsumo.IndexOf("|"));//FechaLimite de pago
            string temporalSinfechalimite = temporalsinPeriodoConsumo.Substring(temporalsinPeriodoConsumo.IndexOf("|") + 1);
            string InfoNumeroDeDocumento = temporalSinfechalimite.Substring(0, temporalSinfechalimite.IndexOf("|"));//numero de documento
            string temporalsinnumerodocumento = temporalSinfechalimite.Substring(temporalSinfechalimite.IndexOf("|") + 1);
            string InfoRMU = temporalsinnumerodocumento.Substring(0, temporalsinnumerodocumento.IndexOf("|"));//DATORMU
            string InfoClientesPordividir = temporalsinnumerodocumento.Substring(temporalsinnumerodocumento.IndexOf("|") + 1);//DATOS A DIVIDIR CLIENTE

            //DIVIDIR INFORMACION DEL CLIENTE DE FACTURA
            String DivisorFiscal = "%";
            String[] FacturaCliente = System.Text.RegularExpressions.Regex.Split(InfoClientesPordividir, DivisorFiscal);


            //DIVIDIR CATCHPHRASE
            String separador = " ";
            String[] fraseSuper = System.Text.RegularExpressions.Regex.Split(catchPhrase, separador);
            int contador = 0;
            foreach (var element in fraseSuper)
            {
                contador++;

            }
            String Textosuperior = "", TextoInferior = "";
            if (contador > 1)
            {
                int totalinicio = contador / 2;
                totalinicio = totalinicio + 2;

                for (int m = 0; m < totalinicio; m++)
                {
                    Textosuperior = Textosuperior + fraseSuper[m] + " ";
                }
                for (int r = totalinicio; r < contador; r++)
                {
                    TextoInferior = TextoInferior + " " + fraseSuper[r];
                }
            }
            else
            {
                Textosuperior = " ";
                TextoInferior = catchPhrase + " ";
            }


            String pattern = "/";
            //declaracion y extraccion de datos resumen Divididos en secciones 
            string TemporalMedidor = DataResumen.Substring(0, DataResumen.IndexOf("|"));

            string TemporalInfoMedidor = DataResumen.Substring(DataResumen.IndexOf("|") + 1);
            string ConceptosMedidor = TemporalInfoMedidor.Substring(0, TemporalInfoMedidor.IndexOf("|"));
            String[] listaConceptosMedidor = System.Text.RegularExpressions.Regex.Split(ConceptosMedidor, pattern);
            //lista de conceptosMedidor
            string TemporalSinConceptos = TemporalInfoMedidor.Substring(TemporalInfoMedidor.IndexOf("|") + 1);
            string CantidadesConceptosMedidor = TemporalSinConceptos.Substring(0, TemporalSinConceptos.IndexOf("|"));
            string[] ListaCantidadesConceptosMedidor = System.Text.RegularExpressions.Regex.Split(CantidadesConceptosMedidor, pattern);
            //lista de COnceptos Cantidades medidor
            string UltimosDatosMedidor = TemporalSinConceptos.Substring(TemporalSinConceptos.IndexOf("|") + 1);
            string[] ListaUltimosDatosMedidor = System.Text.RegularExpressions.Regex.Split(UltimosDatosMedidor, pattern);

            //

            //Obtener Pesos mexicanos


            String[] PesosConceptos = System.Text.RegularExpressions.Regex.Split(Pesos, pattern);


            //Obtener Pesos dolares
            String[] ConceptosDoralesYtotal = System.Text.RegularExpressions.Regex.Split(dolares, pattern);

            //Obtener Conceptos
            String separadorConceptos = ",";
            String[] ConceptosLista = System.Text.RegularExpressions.Regex.Split(Conceptos, separadorConceptos);

            string[] Conceptosfinales= System.Text.RegularExpressions.Regex.Split(ConceptosResumen, pattern);




            try
            {
                Document doc = new Document(PageSize.LETTER);
                string Path = @"c:\PruebasPDF\PDF-USD-CARGAS.pdf";
                //Ruta
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Path, FileMode.Create));

                //metadatos archivo
                doc.AddTitle("PDF-Amper USD Cargas");
                doc.AddCreator("Prime Consultoria");
                doc.SetMargins(5, 5, 5, 5);
                //Abrir Archivo
                doc.Open();
                //Definir Styles
                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _standardMedidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _Medidor = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                iTextSharp.text.Font _secundaryfont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                iTextSharp.text.Font _headers = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
                iTextSharp.text.Font _StandardBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _TOTALBOLD = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font _SecundaryBold = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.BOLD, BaseColor.RED);
                iTextSharp.text.Font _resumee = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 5, iTextSharp.text.Font.NORMAL, BaseColor.RED);
                iTextSharp.text.Font _resumeeheader = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7, iTextSharp.text.Font.BOLD, BaseColor.RED);

                // Escribimos el encabezamiento en el documento

                PdfPTable header = new PdfPTable(1);
                header.WidthPercentage = 95;

                //header.= BaseColor.RED;


                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\LOGOAMPER.png");
                gif.SetAbsolutePosition(20, 750);
                gif.ScaleToFit(100f, 200F);

                iTextSharp.text.Image CFID = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\test.png");
                CFID.SetAbsolutePosition(135, 130);
                CFID.ScaleToFit(200f, 150F);
                doc.Add(CFID);
                //Declarar columnas
                PdfPCell conusoenergia = new PdfPCell(new Phrase(Textosuperior, _StandardBold));
                conusoenergia.BorderWidth = 0;
                conusoenergia.HorizontalAlignment = 2;
                //conusoenergia.BorderWidthBottom = 0.75f;
                PdfPCell metasenmexico = new PdfPCell(new Phrase(TextoInferior, _StandardBold));
                metasenmexico.BorderWidth = 0;
                metasenmexico.HorizontalAlignment = 2;
                //espacio en blanco
                PdfPCell espacio = new PdfPCell(new Phrase(" ", _StandardBold));
                espacio.BorderWidth = 0;
                espacio.HorizontalAlignment = 2;
                //linea Roja
                PdfPCell linearoja = new PdfPCell(new Phrase(" ", _StandardBold));
                linearoja.BorderWidth = 0;
                linearoja.HorizontalAlignment = 2;
                linearoja.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                PdfPCell lineaBlanca = new PdfPCell(new Phrase(" ", _StandardBold));
                lineaBlanca.BorderWidth = 0;
                lineaBlanca.HorizontalAlignment = 2;
                lineaBlanca.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);


                //Agregar Columnas header
                header.AddCell(espacio);
                header.AddCell(conusoenergia);
                header.AddCell(metasenmexico);
                header.AddCell(linearoja);
                header.AddCell(lineaBlanca);
                //agregar datos 
                doc.Add(gif);


                //comparacion condicional segundo logotipo

                if (logotipo == "1")
                {
                    iTextSharp.text.Image secondhead = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\logocabezados.png");
                    secondhead.SetAbsolutePosition(140, 750);
                    secondhead.ScaleToFit(50f, 100F);
                    doc.Add(secondhead);
                }
                doc.Add(header);
                //GRAFICA Y CODIGO QR

                iTextSharp.text.Image grafica = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\grafico.png");
                grafica.SetAbsolutePosition(15, 280);
                grafica.ScaleToFit(320f, 200F);
                doc.Add(grafica);


                iTextSharp.text.Image qr = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\qr.png");
                qr.SetAbsolutePosition(15, 150);
                qr.ScaleToFit(175f, 110F);
                doc.Add(qr);

                //ELEMENTOS NUEVA IMPLEMENTACION 27/10/20XX
                iTextSharp.text.Image ResumenFiscal = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\FolioFiscalResumen.png");
                ResumenFiscal.SetAbsolutePosition(300, 630);
                ResumenFiscal.ScaleToFit(175f, 110F);
                doc.Add(ResumenFiscal);

                iTextSharp.text.Image ValoresAdefinir = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\ValoresADefinir.png");
                ValoresAdefinir.SetAbsolutePosition(50, 70);
                ValoresAdefinir.ScaleToFit(175f, 110F);
                doc.Add(ValoresAdefinir);

                //iTextSharp.text.Image FolioFiscalUnico = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\FolioFiscal.png");
                //FolioFiscalUnico.SetAbsolutePosition(150, 300);
                //FolioFiscalUnico.ScaleToFit(175f, 110F);
                //doc.Add(FolioFiscalUnico);

                //iTextSharp.text.Image GastosGenerales = iTextSharp.text.Image.GetInstance(@"C:\PruebasPDF\Resources\GastosGenerales.png");
                //GastosGenerales.SetAbsolutePosition(150, 300);
                //GastosGenerales.ScaleToFit(175f, 110F);
                //doc.Add(GastosGenerales);



                //Pie de pagina
                PdfPTable tab = new PdfPTable(1);
                PdfPCell cell = new PdfPCell(new Phrase("Este documento es una representación impresa de un CFDI", _secundaryfont));
                cell.Border = 0;
                tab.TotalWidth = 300F;
                tab.AddCell(cell);
                tab.WriteSelectedRows(0, -1, 200, 40, writer.DirectContent);
                doc.Add(Chunk.NEWLINE);


                //TERMINO HEADER
                //Empiezo Datos Cliente
                PdfPTable DatosCliente = new PdfPTable(4);
                DatosCliente.WidthPercentage = 95;
                //cells Datos Cliente
                //CellSaltoDeLinea
                PdfPCell Salto = new PdfPCell(new Phrase(" ", _headers));
                Salto.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                Salto.BorderWidth = 0;
                Salto.HorizontalAlignment = 1;
                Salto.Colspan = 3;
                //Cellcoral


                //go
                PdfPCell Dependencia = new PdfPCell(new Phrase("AMMPER Energia, S.A.P.I. de C.V. ", _standardFont));
                Dependencia.Colspan = 3;
                Dependencia.BorderWidth = 0;
                Dependencia.HorizontalAlignment = 0;

                PdfPCell TotalPago = new PdfPCell(new Phrase(" Total a pagar ", _headers));
                TotalPago.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                TotalPago.BorderWidth = 0;
                TotalPago.HorizontalAlignment = 1;

                PdfPCell periodofact = new PdfPCell(new Phrase(" del periodo facturado ", _headers));
                periodofact.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodofact.BorderWidth = 0;
                periodofact.HorizontalAlignment = 1;

                PdfPCell calle = new PdfPCell(new Phrase("Av. Paseo de la Reforma ", _standardFont));
                calle.Colspan = 3;
                calle.BorderWidth = 0;
                calle.HorizontalAlignment = 0;

                PdfPCell coral = new PdfPCell(new Phrase(" ", _headers));
                coral.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                coral.BorderWidth = 0;
                coral.HorizontalAlignment = 1;



                PdfPCell numerodireccion = new PdfPCell(new Phrase("243 Piso 19", _standardFont));
                numerodireccion.Colspan = 3;
                numerodireccion.BorderWidth = 0;
                numerodireccion.HorizontalAlignment = 0;

                PdfPCell RegimenFiscal = new PdfPCell(new Phrase("Régimen Fiscal: 601-General de Ley de Personas Morales", _standardFont));
                RegimenFiscal.Colspan = 3;
                RegimenFiscal.BorderWidth = 0;
                RegimenFiscal.HorizontalAlignment = 0;

                PdfPCell GastosenGenaral = new PdfPCell(new Phrase($"Uso CFDI:{UsoCFI}", _standardFont));
                GastosenGenaral.Colspan = 3;
                GastosenGenaral.BorderWidth = 0;
                GastosenGenaral.HorizontalAlignment = 0;

                PdfPCell numerospago = new PdfPCell(new Phrase("$" + InfoTotalAPagar, _TOTALBOLD));
                numerospago.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numerospago.BorderWidth = 0;
                numerospago.HorizontalAlignment = 1;

                PdfPCell codigopostal = new PdfPCell(new Phrase("Cuauhtémoc, CP.06500", _standardFont));
                codigopostal.Colspan = 3;
                codigopostal.BorderWidth = 0;
                codigopostal.HorizontalAlignment = 0;

                PdfPCell saltopagar = new PdfPCell(new Phrase(" ", _StandardBold));
                saltopagar.BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255);
                saltopagar.BorderWidth = 0;
                saltopagar.HorizontalAlignment = 1;

                PdfPCell ciudadfactura = new PdfPCell(new Phrase("Ciudad de México, México", _standardFont));
                ciudadfactura.Colspan = 3;
                ciudadfactura.BorderWidth = 0;
                ciudadfactura.HorizontalAlignment = 0;

                PdfPCell periodoconsumo = new PdfPCell(new Phrase("Periodo del Consumo ", _headers));
                periodoconsumo.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                periodoconsumo.BorderWidth = 0;
                periodoconsumo.HorizontalAlignment = 1;

                PdfPCell rfcamper = new PdfPCell(new Phrase("R. F. C. AEN160405E56", _secundaryfont));
                rfcamper.Colspan = 3;
                rfcamper.BorderWidth = 0;
                rfcamper.HorizontalAlignment = 0;

                PdfPCell fechasperiodo = new PdfPCell(new Phrase(InfoPeriodoConsumo, _StandardBold));
                fechasperiodo.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                fechasperiodo.BorderWidth = 0;
                fechasperiodo.HorizontalAlignment = 1;

                PdfPCell direccioncliente = new PdfPCell(new Phrase("Nombre y domicilio", _SecundaryBold));
                direccioncliente.Colspan = 3;
                direccioncliente.BorderWidth = 2;
                direccioncliente.HorizontalAlignment = 0;


                direccioncliente.BorderColor = new BaseColor(255, 0, 0);

                direccioncliente.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                //Datos cliente de Amper

                PdfPCell nombreydomicilio = new PdfPCell(new Phrase(FacturaCliente[0], _standardFont));
                nombreydomicilio.Colspan = 2;
                nombreydomicilio.BorderWidth = 0;
                nombreydomicilio.HorizontalAlignment = 0;

                PdfPCell INFORMACIONDIRECCIONCLIENTE = new PdfPCell(new Phrase(FacturaCliente[1], _standardFont));
                INFORMACIONDIRECCIONCLIENTE.Colspan = 2;
                INFORMACIONDIRECCIONCLIENTE.BorderWidth = 0;
                INFORMACIONDIRECCIONCLIENTE.HorizontalAlignment = 0;

                PdfPCell NumeroDoc = new PdfPCell(new Phrase("Número de documento", _SecundaryBold));
                NumeroDoc.BorderWidth = 2;
                NumeroDoc.HorizontalAlignment = 0;
                NumeroDoc.BorderColor = new BaseColor(255, 0, 0);
                NumeroDoc.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell taimlimit = new PdfPCell(new Phrase(" Fecha límite de pago", _headers));
                taimlimit.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                taimlimit.BorderWidth = 0;
                taimlimit.HorizontalAlignment = 1;

                PdfPCell espacioderelleno = new PdfPCell(new Phrase(" ", _standardFont));
                espacioderelleno.Colspan = 2;
                espacioderelleno.BorderWidth = 0;
                espacioderelleno.HorizontalAlignment = 0;

                PdfPCell elverdaderodoc = new PdfPCell(new Phrase(InfoNumeroDeDocumento, _standardFont));

                elverdaderodoc.BorderWidth = 0;
                elverdaderodoc.HorizontalAlignment = 0;

                PdfPCell lafechalimite = new PdfPCell(new Phrase(InfoFechaLimiteBill, _StandardBold));
                lafechalimite.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                lafechalimite.BorderWidth = 0;
                lafechalimite.HorizontalAlignment = 1;

                PdfPCell RMUtitulo = new PdfPCell(new Phrase("RMU", _SecundaryBold));
                RMUtitulo.BorderWidth = 2;
                RMUtitulo.HorizontalAlignment = 0;
                RMUtitulo.BorderColor = new BaseColor(255, 0, 0);
                RMUtitulo.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell telefonoemergencia = new PdfPCell(new Phrase(" Teléfono de emergencia", _headers));
                telefonoemergencia.BackgroundColor = new iTextSharp.text.BaseColor(255, 0, 0);
                telefonoemergencia.BorderWidth = 0;
                telefonoemergencia.HorizontalAlignment = 1;

                PdfPCell NUMRMU = new PdfPCell(new Phrase(InfoRMU, _standardFont));

                NUMRMU.BorderWidth = 0;
                NUMRMU.HorizontalAlignment = 0;

                PdfPCell numero8cientos = new PdfPCell(new Phrase("800 9532 911", _StandardBold));
                numero8cientos.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                numero8cientos.BorderWidth = 0;
                numero8cientos.HorizontalAlignment = 1;

                PdfPCell MedicionConsum = new PdfPCell(new Phrase("", _SecundaryBold));
                MedicionConsum.BorderWidth = 0;
                MedicionConsum.HorizontalAlignment = 0;

                PdfPCell cientos8amper = new PdfPCell(new Phrase("", _StandardBold));
                cientos8amper.BackgroundColor = new iTextSharp.text.BaseColor(252, 209, 198);
                cientos8amper.BorderWidth = 0;
                cientos8amper.HorizontalAlignment = 1;




                //Agregar Celdas a Datoscliente
                DatosCliente.AddCell(Dependencia);
                DatosCliente.AddCell(TotalPago);
                DatosCliente.AddCell(RegimenFiscal);
                DatosCliente.AddCell(periodofact);
                DatosCliente.AddCell(calle);
                DatosCliente.AddCell(coral);
                DatosCliente.AddCell(numerodireccion);
                DatosCliente.AddCell(numerospago);

                DatosCliente.AddCell(codigopostal);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(ciudadfactura);
                DatosCliente.AddCell(periodoconsumo);
                DatosCliente.AddCell(rfcamper);
                DatosCliente.AddCell(fechasperiodo);
                DatosCliente.AddCell(GastosenGenaral);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(direccioncliente);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(Salto);
                DatosCliente.AddCell(saltopagar);
                DatosCliente.AddCell(nombreydomicilio);
                DatosCliente.AddCell(NumeroDoc);
                DatosCliente.AddCell(taimlimit);
                DatosCliente.AddCell(INFORMACIONDIRECCIONCLIENTE);
                DatosCliente.AddCell(elverdaderodoc);
                DatosCliente.AddCell(lafechalimite);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(RMUtitulo);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(telefonoemergencia);
                DatosCliente.AddCell(NUMRMU);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(numero8cientos);
                DatosCliente.AddCell(MedicionConsum);
                DatosCliente.AddCell(espacioderelleno);
                DatosCliente.AddCell(cientos8amper);



                //agregar datos 
                doc.Add(DatosCliente);

                //Tabla de medicion de consumo
                PdfPTable DatosdeConsumo = new PdfPTable(8);
                DatosdeConsumo.WidthPercentage = 95;

                //Declaracion de celdas
                //encabezados
                PdfPCell Nummedidor = new PdfPCell(new Phrase(" No. Medidor ", _SecundaryBold));
                Nummedidor.BorderWidth = 2;
                Nummedidor.HorizontalAlignment = 0;
                Nummedidor.BorderColor = new BaseColor(255, 0, 0);
                Nummedidor.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;

                PdfPCell Tituloconsumomensual = new PdfPCell(new Phrase(" Consumo Mensual (MWh) ", _SecundaryBold));
                Tituloconsumomensual.BorderWidth = 2;
                Tituloconsumomensual.HorizontalAlignment = Element.ALIGN_CENTER;
                Tituloconsumomensual.BorderColor = new BaseColor(255, 0, 0);
                Tituloconsumomensual.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Tituloconsumomensual.Colspan = 3;

                PdfPCell TarifaSumi = new PdfPCell(new Phrase(" Tarifa de Suministro ($/MWh) ", _SecundaryBold));
                TarifaSumi.BorderWidth = 2;
                TarifaSumi.HorizontalAlignment = Element.ALIGN_CENTER;
                TarifaSumi.BorderColor = new BaseColor(255, 0, 0);
                TarifaSumi.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                TarifaSumi.Colspan = 2;

                PdfPCell Superpotencia100 = new PdfPCell(new Phrase("Potencia 100 horas críticas (MW)", _SecundaryBold));
                Superpotencia100.BorderWidth = 2;
                Superpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                Superpotencia100.BorderColor = new BaseColor(255, 0, 0);
                Superpotencia100.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                Superpotencia100.Colspan = 2;

                //Contenido

                //Dejar espacios blancos en medidor
                PdfPTable DatosdeConsumo2 = new PdfPTable(10);
                DatosdeConsumo2.WidthPercentage = 95;

                PdfPCell espaciomedidor = new PdfPCell(new Phrase(" ", _standardFont));
                espaciomedidor.BorderWidth = 0;
                espaciomedidor.HorizontalAlignment = 0;

                //dejar espacios en blanco en tarifa de suministros

                PdfPCell espaciotarifasumi = new PdfPCell(new Phrase(" ", _standardFont));
                espaciotarifasumi.BorderWidth = 0;
                espaciotarifasumi.HorizontalAlignment = 0;
                espaciotarifasumi.Colspan = 2;

                //dejar espacios en blanco en potencias 100!

                PdfPCell espaciopotencia100 = new PdfPCell(new Phrase(" ", _standardFont));
                espaciopotencia100.BorderWidth = 0;
                espaciopotencia100.HorizontalAlignment = 0;
                espaciopotencia100.Colspan = 2;

                //medidor
                PdfPCell cualmedidor = new PdfPCell(new Phrase(TemporalMedidor, _Medidor));
                cualmedidor.BorderWidth = 0;
                cualmedidor.HorizontalAlignment = 0;
                int anchoss = TemporalMedidor.Length;
                cualmedidor.Colspan = 2;


                //consumoneto

                PdfPCell consumoneto = new PdfPCell(new Phrase($" {listaConceptosMedidor[0]} ", _standardMedidor));
                consumoneto.BorderWidth = 0;
                consumoneto.HorizontalAlignment = Element.ALIGN_LEFT;
                consumoneto.Colspan = 2;

                PdfPCell cantidadconsumoneto = new PdfPCell(new Phrase(ListaCantidadesConceptosMedidor[0], _standardMedidor));
                cantidadconsumoneto.BorderWidth = 0;
                cantidadconsumoneto.HorizontalAlignment = Element.ALIGN_RIGHT;

                //Tarifa
                PdfPCell cantidadtarifa = new PdfPCell(new Phrase(ListaUltimosDatosMedidor[0], _standardMedidor));
                cantidadtarifa.BorderWidth = 0;
                cantidadtarifa.HorizontalAlignment = Element.ALIGN_LEFT;

                PdfPCell conceptotarifa = new PdfPCell(new Phrase("", _standardMedidor));
                conceptotarifa.BorderWidth = 0;
                conceptotarifa.HorizontalAlignment = 0;

                //potencia super cantidad!
                PdfPCell cantidadpotencia100 = new PdfPCell(new Phrase(ListaUltimosDatosMedidor[1], _standardMedidor));
                cantidadpotencia100.BorderWidth = 0;
                cantidadpotencia100.HorizontalAlignment = Element.ALIGN_CENTER;
                cantidadpotencia100.Colspan = 2;

               




                //Agregar celdas a datos consumo

                DatosdeConsumo.AddCell(Nummedidor);
                DatosdeConsumo.AddCell(Tituloconsumomensual);
                DatosdeConsumo.AddCell(TarifaSumi);
                DatosdeConsumo.AddCell(Superpotencia100);

                DatosdeConsumo2.AddCell(cualmedidor);
                DatosdeConsumo2.AddCell(consumoneto);
                DatosdeConsumo2.AddCell(cantidadconsumoneto);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(cantidadtarifa);
                DatosdeConsumo2.AddCell(conceptotarifa);
                DatosdeConsumo2.AddCell(cantidadpotencia100);
                //insertar ciclo para dinanismo conceptos medidor
                int CuentaConceptosMedidor = listaConceptosMedidor.Count();
                for (int i = 1; i <CuentaConceptosMedidor; i++)
                {
                    //ciclo dinanismo conceptos

                    PdfPCell conceptoslistas = new PdfPCell(new Phrase($" {listaConceptosMedidor[i]} ", _standardMedidor));
                    conceptoslistas.BorderWidth = 0;
                    conceptoslistas.HorizontalAlignment = Element.ALIGN_LEFT;
                    conceptoslistas.Colspan = 2;

                    PdfPCell cantidadconceptoslistas = new PdfPCell(new Phrase(ListaCantidadesConceptosMedidor[i], _standardMedidor));
                    cantidadconceptoslistas.BorderWidth = 0;
                    cantidadconceptoslistas.HorizontalAlignment = Element.ALIGN_RIGHT;

                    

                    DatosdeConsumo2.AddCell(espaciomedidor);
                    DatosdeConsumo2.AddCell(espaciomedidor);
                    DatosdeConsumo2.AddCell(conceptoslistas);
                    DatosdeConsumo2.AddCell(cantidadconceptoslistas);
                    DatosdeConsumo2.AddCell(conceptotarifa);
                    DatosdeConsumo2.AddCell(espaciotarifasumi);
                    DatosdeConsumo2.AddCell(espaciopotencia100);
                    
                }

                

                
                //AgregardatosConsumo
                doc.Add(DatosdeConsumo);
                doc.Add(DatosdeConsumo2);
                doc.Add(Chunk.NEWLINE);

                //tabla Desglosada de resumen
                PdfPTable Resumen = new PdfPTable(14);
                Resumen.WidthPercentage = 100;

                //Espacios Blancos de resumen

                PdfPCell blancosresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosresumen.BorderWidth = 0;
                blancosresumen.HorizontalAlignment = 0;
                blancosresumen.Colspan = 8;

                PdfPCell blancosfinalresumen = new PdfPCell(new Phrase(" ", _standardFont));
                blancosfinalresumen.BorderWidth = 0;
                blancosfinalresumen.HorizontalAlignment = 0;

                //Crear datos encabezado resumee

                PdfPCell concepto = new PdfPCell(new Phrase("Concepto ", _resumeeheader));
                concepto.BorderWidth = 1;
                concepto.HorizontalAlignment = 0;
                concepto.BorderColor = new BaseColor(255, 0, 0);
                concepto.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                concepto.Colspan = 3;

                PdfPCell tarifa = new PdfPCell(new Phrase(tipoCambio + "/MWh ", _resumeeheader));
                tarifa.BorderWidth = 1;
                tarifa.HorizontalAlignment = 1;
                tarifa.BorderColor = new BaseColor(255, 0, 0);
                tarifa.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;


                PdfPCell totaldolarucos = new PdfPCell(new Phrase(" Total  " + tipoCambio, _resumeeheader));
                totaldolarucos.BorderWidth = 1;
                totaldolarucos.HorizontalAlignment = 1;
                totaldolarucos.BorderColor = new BaseColor(255, 0, 0);
                totaldolarucos.Border = iTextSharp.text.Rectangle.BOTTOM_BORDER;
                //Comparacion de datos resumee Pesos
                //for (int a = 0; a < 23; a++)
                //{
                //    string pesostemporales = PesosConceptos[a];
                //    if (PesosConceptos[a] != "")
                //    {
                //        if (PesosConceptos[a].Substring(0, 1) == "-")
                //        {
                //            PesosConceptos[a] = "( $" + pesostemporales.Substring(1) + ")";
                //        }
                //        else
                //        {
                //            PesosConceptos[a] = " $" + pesostemporales;

                //        }
                //    }
                //}
                ////Comparacion de datos Resume Dolarucos
                //for (int a = 0; a < 27; a++)
                //{
                //    string pesostemporales = ConceptosDoralesYtotal[a];
                //    if (ConceptosDoralesYtotal[a] != "")
                //    {
                //        if (ConceptosDoralesYtotal[a].Substring(0, 1) == "-")
                //        {
                //            ConceptosDoralesYtotal[a] = "( $" + pesostemporales.Substring(1) + ")";
                //        }
                //        else
                //        {
                //            ConceptosDoralesYtotal[a] = " $" + pesostemporales;

                //        }
                //    }
                //}
                int conceptoscuenta = 0;
                //INICIO ENCABEZADO RESUMEN
                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(concepto);
                Resumen.AddCell(tarifa);
                Resumen.AddCell(totaldolarucos);
                Resumen.AddCell(blancosfinalresumen);
                foreach (var element in ConceptosLista)
                {
                    
                    PdfPCell conceptoreal= new PdfPCell(new Phrase($"{element} ", _resumee));
                    conceptoreal.BorderWidth = 0;
                    conceptoreal.HorizontalAlignment = 0;
                    conceptoreal.Colspan = 3;
                    string pesostemporales = PesosConceptos[conceptoscuenta];
                    
                        if (PesosConceptos[conceptoscuenta].Substring(0, 1) == "-")
                        {
                            PesosConceptos[conceptoscuenta] = "( $" + pesostemporales.Substring(1) + ")";
                        }
                        else
                        {
                            PesosConceptos[conceptoscuenta] = " $" + pesostemporales;

                        }
                    
                    PdfPCell pesosmexicanos = new PdfPCell(new Phrase(PesosConceptos[conceptoscuenta], _resumee));
                    pesosmexicanos.BorderWidth = 0;
                    pesosmexicanos.HorizontalAlignment = 2;

                    string pesostemporales2 = ConceptosDoralesYtotal[conceptoscuenta];
                   
                        if (ConceptosDoralesYtotal[conceptoscuenta].Substring(0, 1) == "-")
                        {
                            ConceptosDoralesYtotal[conceptoscuenta] = "( $" + pesostemporales2.Substring(1) + ")";
                        }
                        else
                        {
                            ConceptosDoralesYtotal[conceptoscuenta] = " $" + pesostemporales2;

                        }
                    

                    PdfPCell dolaresreales = new PdfPCell(new Phrase(ConceptosDoralesYtotal[conceptoscuenta], _resumee));
                    dolaresreales.BorderWidth = 0;
                    dolaresreales.HorizontalAlignment = 2;
                    //ENERGIA CONTRATADA

                    Resumen.AddCell(blancosresumen);
                    Resumen.AddCell(conceptoreal);
                    Resumen.AddCell(pesosmexicanos);
                    Resumen.AddCell(dolaresreales);
                    Resumen.AddCell(blancosfinalresumen);
                    conceptoscuenta++;
                }
                //Creardatos resumee


                // AMMPER
                for (int a = 0; a < 3; a++)
                {
                    string conceptosfinales = Conceptosfinales[a];

                    if (Conceptosfinales[a].Substring(0, 1) == "-")
                    {
                        Conceptosfinales[a] = "( $" + conceptosfinales.Substring(1) + ")";
                    }
                    else
                    {
                        Conceptosfinales[a] = " $" +conceptosfinales;

                    }

                }
                PdfPCell SUBTOTALRESUMEE = new PdfPCell(new Phrase("SUBTOTAL", _resumee));
                SUBTOTALRESUMEE.BorderWidth = 0;
                SUBTOTALRESUMEE.HorizontalAlignment = 0;
                SUBTOTALRESUMEE.Colspan = 3;

                
                PdfPCell SUBTRUSD = new PdfPCell(new Phrase(Conceptosfinales[0], _resumee));
                SUBTRUSD.BorderWidth = 0;
                SUBTRUSD.HorizontalAlignment = 2;
                // subtotal RESUMEE

                PdfPCell IVARESUMEE = new PdfPCell(new Phrase("IVA", _resumee));
                IVARESUMEE.BorderWidth = 0;
                IVARESUMEE.HorizontalAlignment = 0;
                IVARESUMEE.Colspan = 3;


                PdfPCell IVARUSD = new PdfPCell(new Phrase(Conceptosfinales[1], _resumee));
                IVARUSD.BorderWidth = 0;
                IVARUSD.HorizontalAlignment = 2;
                // IVA

                PdfPCell TOTALRESUMEE = new PdfPCell(new Phrase("TOTAL", _resumee));
                TOTALRESUMEE.BorderWidth = 0;
                TOTALRESUMEE.HorizontalAlignment = 0;
                TOTALRESUMEE.Colspan = 3;


                PdfPCell TOTALRUSD = new PdfPCell(new Phrase(Conceptosfinales[2], _resumee));
                TOTALRUSD.BorderWidth = 0;
                TOTALRUSD.HorizontalAlignment = 2;
                // TOTAL RESUMEE

                PdfPCell TIPOEXCHANGE = new PdfPCell(new Phrase("TIPO DE CAMBIO", _resumee));
                TIPOEXCHANGE.BorderWidth = 0;
                TIPOEXCHANGE.HorizontalAlignment = 0;
                TIPOEXCHANGE.Colspan = 3;


                PdfPCell TIPOEXCHRUSD = new PdfPCell(new Phrase(Conceptosfinales[3], _resumee));
                TIPOEXCHRUSD.BorderWidth = 0;
                TIPOEXCHRUSD.HorizontalAlignment = 2;
                // TIPO DE CAMBIO

                //Agregar celdas a datos consumo
               
                //FIN ENCABEZADO RESUMEN
               


                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(SUBTOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(SUBTRUSD);
                Resumen.AddCell(blancosfinalresumen);



                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(IVARESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(IVARUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TOTALRESUMEE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TOTALRUSD);
                Resumen.AddCell(blancosfinalresumen);

                Resumen.AddCell(blancosresumen);
                Resumen.AddCell(TIPOEXCHANGE);
                Resumen.AddCell(blancosfinalresumen);
                Resumen.AddCell(TIPOEXCHRUSD);
                Resumen.AddCell(blancosfinalresumen);
                //Agregardatosresumen
                doc.Add(Resumen);



                //tabla de paginacion
                //PdfPTable Paginacion = new PdfPTable(1);
                //Paginacion.WidthPercentage = 95;
                //Paginacion.SetAbsolutePosition(0,10);

                doc.Close();
                writer.Close();
                Status = "Proceso exitoso, terminado a las:" + DateTime.Today.ToLongTimeString();

                Byte[] bytespdf = File.ReadAllBytes(Path);
                return Convert.ToBase64String(bytespdf);
            }
            catch (Exception ex)
            {
                Status = "Hubo un error a las " + DateTime.Today.ToLongTimeString() + ex+".Mande el error a administración con Prime Consultoria para solucionarlo lo más pronto posible.";
                return Status;
            }

        }
        static void CreateGraphic(string opcion)
        {
            string path = $@"c:\PruebasPDF\Resources\PDFGrap{opcion}.xlsx";
            string destine = @"c:\PruebasPDF\Resources\grafico.png";

            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2013;

                application.ChartToImageConverter = new ChartToImageConverter();
                application.ChartToImageConverter.ScalingMode = ScalingMode.Best;

                Syncfusion.XlsIO.IWorkbook workbook = application.Workbooks.Open(path);
                IWorksheet worksheet = workbook.Worksheets[0];

                IChart chart = worksheet.Charts[0];

                //Creating the memory stream for chart image
                MemoryStream stream = new MemoryStream();

                //Saving the chart as image
                chart.SaveAsImage(stream);

                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                //Saving image stream to file
                image.Save(destine);
            }
        }
        static void InsertDataToGraphic(string info)
        {
            string pathopcion = info.Substring(0, info.IndexOf("|"));
            if((pathopcion.Substring(0,1)=="1") || (pathopcion.Substring(0, 1) == "2"))
            { 
                string temporalsinopcion = info.Substring(info.IndexOf("|") + 1);
                string path = $@"c:\PruebasPDF\Resources\PDFGrap{pathopcion}.xlsx";
                string fecha = temporalsinopcion.Substring(0, temporalsinopcion.IndexOf("|"));

                DateTime superdate = DateTime.Parse(fecha);
                string workingdata = temporalsinopcion.Substring(temporalsinopcion.IndexOf("|") + 1);

                using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2013;
                Syncfusion.XlsIO.IWorkbook workbook = application.Workbooks.Open(path);
                IWorksheet sheet = workbook.Worksheets[0];
                //Fill CELLS ABOUT A AND B
                string CasillaA = "A", CasillaB = "B";
                string pattern = "-";
                string[] mwhInfo = System.Text.RegularExpressions.Regex.Split(workingdata, pattern);
                int u = 3;
                foreach (var INFOMWH in mwhInfo)
                {

                    //Specify cell address FECHAS
                    sheet.Range[CasillaA + u].Text = superdate.ToString("d");
                    //Specify Cell address MWh Consumido
                    sheet.Range[CasillaB + u].Number = Convert.ToDouble(INFOMWH);
                    superdate = superdate.AddDays(+1);
                    u++;
                }


                ////Access a range by specifying cell address
                //sheet.Range["A7"].Text = "Accessing a Range by specify cell address ";

                ////Access a range by specifying cell row and column index
                //sheet.Range[9, 1].Text = "Accessing a Range by specify cell row and column index ";

                ////Access a Range by specifying using defined name
                //Syncfusion.XlsIO.IName name = workbook.Names.Add("Name");
                //name.RefersToRange = sheet.Range["A11"];
                //sheet.Range["Name"].Text = "Accessing a Range by specifying using defined name.";

                ////Accessing a Range of cells by specifying cells address
                //sheet.Range["A13:C13"].Text = "Accessing a Range of Cells (Method 1)";

                ////Accessing a Range of cells specifying cell row and column index
                //sheet.Range[15, 1, 15, 3].Text = "Accessing a Range of Cells (Method 2)";

                workbook.SaveAs(path);
            }
                CreateGraphic(pathopcion);
    
            }
            else if(pathopcion.Substring(0, 1) == "3")
            {
                string path = @"c:\PruebasPDF\Resources\PDFGrap" + pathopcion + ".xlsx";
                string fecha = info.Substring(0, info.IndexOf("|"));

                DateTime superdate = DateTime.Parse(fecha);
                string workingdata = info.Substring(info.IndexOf("|") + 1);
                string consumo = workingdata.Substring(0, workingdata.IndexOf("|"));
                string contratado = workingdata.Substring(workingdata.IndexOf("|") + 1);

                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication application = excelEngine.Excel;
                    application.DefaultVersion = ExcelVersion.Excel2013;
                    Syncfusion.XlsIO.IWorkbook workbook = application.Workbooks.Open(path);
                    IWorksheet sheet = workbook.Worksheets[0];
                    //Fill CELLS ABOUT A AND B
                    string CasillaA = "A", CasillaB = "B", CasillaC = "C";
                    string pattern = "-";
                    string[] mwhInfo = System.Text.RegularExpressions.Regex.Split(consumo, pattern);
                    int u = 3;
                    foreach (var INFOMWH in mwhInfo)
                    {

                        //Specify cell address FECHAS
                        sheet.Range[CasillaA + u].Text = superdate.ToString("d");
                        //Specify Cell address MWh Consumido
                        sheet.Range[CasillaB + u].Number = Convert.ToDouble(INFOMWH);
                        superdate = superdate.AddDays(+1);
                        u++;
                    }
                    int s = 3;
                    string[] mwhContract = System.Text.RegularExpressions.Regex.Split(contratado, pattern);
                    foreach (var ContractMWH in mwhContract)
                    {

                        //Specify Cell address MWh Contratado
                        sheet.Range[CasillaC + s].Number = Convert.ToDouble(ContractMWH);

                        s++;
                    }


                    workbook.SaveAs(path);
                }
                CreateGraphic(pathopcion);
            }
            
        }

        
        static void CopyDataResourcesToWork()
        {
            string path = @"c:\PruebasPDF\Resources\";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string rutaactual = HttpContext.Current.Server.MapPath(".");
            string pathSRC = @rutaactual + "/Resources/";
            foreach (var file in Directory.GetFiles(pathSRC))
            {
                File.Copy(file, System.IO.Path.Combine(path, System.IO.Path.GetFileName(file)), true);
            }

        }

        static void CreateQRImage(string InfoForQR, string Etc)
        {
            QRCodeEncoder generarCodigoQR = new QRCodeEncoder();
            generarCodigoQR.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;

            generarCodigoQR.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;

            generarCodigoQR.QRCodeVersion = 0;

            generarCodigoQR.QRCodeBackgroundColor = System.Drawing.Color.FromArgb(-1);
            generarCodigoQR.QRCodeForegroundColor = System.Drawing.Color.FromArgb(-16777216);
            string id = InfoForQR.Substring(0, InfoForQR.IndexOf(","));
            string temp1 = InfoForQR.Substring(InfoForQR.IndexOf(",") + 1);
            string re = temp1.Substring(0, temp1.IndexOf(","));
            string rr = temp1.Substring(temp1.IndexOf(",") + 1);



            string ULTIMOS8 = Etc;
            string xresult = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id=" + id + "&re=" + re + "&rr=" + rr + "&tt=0&fe=" + ULTIMOS8;
            //string tbase64 = GetStringFromImage(generarCodigoQR.Encode(xresult, System.Text.Encoding.UTF8));
            String imaqrcode = @"c:\PruebasPDF\Resources\qr.png";

            generarCodigoQR.Encode(xresult, System.Text.Encoding.UTF8).Save(imaqrcode);
            generarCodigoQR.Encode(xresult, System.Text.Encoding.UTF8).Dispose();


        }
    }
}